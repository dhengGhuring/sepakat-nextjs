import { createSlice } from '@reduxjs/toolkit';

const initialState = [];

export const propertySlice = createSlice({
  name: 'property',
  initialState,
  reducers: {
    propertyPush: (state, action) => {
      const element = action.payload;
      if (!state.includes(element)) {
        state.push(action.payload);
      }
    },
  },
});

export const { propertyPush } = propertySlice.actions;

export default propertySlice.reducer;

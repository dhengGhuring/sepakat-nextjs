import { createSlice } from "@reduxjs/toolkit";

const initialState = [];

export const articleSlice = createSlice({
    name: "article",
    initialState,
    reducers: {
        articlePush: (state, action) => {
            const element = action.payload;
            const checkDuplicate =
                state?.filter((el) => {
                    return el.id === element.id;
                }) ?? [];
            // console.log(checkDuplicate.length);
            if (checkDuplicate.length <= 0) {
                state.push(element);
            }
        },
        isEnded: (state, action) => {
            const condition = action.payload;
            _isFinished = condition;
        },
    },
});

export const { articlePush, isEnded } = articleSlice.actions;
let _isFinished = false;

/// ambil data media artikel
export const fetchArticles = async (created_at, uid) => (dispatch) => {
    const fetchCall =
        created_at && uid
            ? `https://api.sepakat.id/firebase/content-article?page=1&created_at=${created_at}&uid=${uid}`
            : `https://api.sepakat.id/firebase/content-article?page=1`;
    fetch(fetchCall)
        .then((res) => {
            return res.json();
        })
        .then((res) => {
            if (res.data.length <= 0) {
                // console.log(_isFinished);
                _isFinished = true;
                dispatch(isEnded(true));
                return;
            }
            for (let i = 0; i < res.data.length; i++) {
                dispatch(articlePush(res.data[i]));
            }
        });
};

export const articleIsEnded = (state) =>
    (state?.article?.length > 0 ?? false) && _isFinished;

export const articlesStore = (state) => state.article;

export const articlesIsEmpty = (state) => state.article.length === 0;

export default articleSlice.reducer;

import { createSlice } from "@reduxjs/toolkit";

const initialState = [];

export const videoSlice = createSlice({
    name: "video",
    initialState,
    reducers: {
        videoPush: (state, action) => {
            const element = action.payload;
            const checkDuplicate =
                state?.filter((el) => {
                    return el.id === element.id;
                }) ?? [];
            // console.log(checkDuplicate.length);
            if (checkDuplicate.length <= 0) {
                state.push(element);
            }
        },
        isEnded: (state, action) => {
            const condition = action.payload;
            _isFinished = condition;
        },
    },
});

export const { videoPush, isEnded } = videoSlice.actions;
let _isFinished = false;
// ambil data media video
export const fetchVideos =
    async (thumbnail_name, created_at, uid) => (dispatch) => {
        const fetchCall =
            thumbnail_name && created_at && uid
                ? `https://api.sepakat.id/firebase/content-video?page=1&created_at=${created_at}&uid=${uid}&thumbnail_name=${thumbnail_name}`
                : `https://api.sepakat.id/firebase/content-video?page=1`;
        fetch(fetchCall)
            .then((res) => {
                return res.json();
            })
            .then((res) => {
                //console.log(thumbnail_name && created_at && uid);
                if (res.data.length <= 0) {
                    // console.log(_isFinished);
                    _isFinished = true;
                    dispatch(isEnded(true));
                    return;
                }
                for (let i = 0; i < res.data.length; i++) {
                    dispatch(videoPush(res.data[i]));
                }
            });
    };

export const videoIsEnded = (state) =>
    (state?.video?.length > 0 ?? false) && _isFinished;

export const videosStore = (state) => state.video;

export const videosIsEmpty = (state) => state.video.length === 0;

export default videoSlice.reducer;

import { createSlice } from "@reduxjs/toolkit";

const initialState = [];

export const recommendationPropertySlice = createSlice({
    name: "recommendationProperty",
    initialState,
    reducers: {
        recommendationPropertyPush: (state, action) => {
            const element = action.payload;
            if (!state.includes(element)) {
                state.push(action.payload);
            }
        },
    },
});

export const { recommendationPropertyPush } =
    recommendationPropertySlice.actions;

/// ambil data rekomendasi properti
export const fetchRecommendationProperty = async () => (dispatch) => {
    fetch(`https://api.sepakat.id/firebase/properties?page=1&city=jember`)
        .then((res) => {
            return res.json();
        })
        .then((res) => {
            for (let i = 0; i < res.data.length; i++) {
                dispatch(recommendationPropertyPush(res.data[i]));
            }
        });
};

export const recommendationPropertiesStore = (state) =>
    state.recommendationProperty;

export const recommendationPropertyIsEmpty = (state) =>
    state.recommendationProperty.length === 0;

export default recommendationPropertySlice.reducer;

import { createSlice } from "@reduxjs/toolkit";
import Swal from "sweetalert2";

const initialState = [];

export const propertySearchSlice = createSlice({
    name: "propertySearch",
    initialState,
    reducers: {
        propertySearchPush: (state, action) => {
            const element = action.payload;
            if (!state.includes(element)) {
                state.push(action.payload);
            }
        },
        setPropertySearch: (state, action) => {
            if (Array.isArray(action.payload)) {
                while (state.length > 0) {
                    state.pop();
                }
                action.payload.forEach((element) => {
                    state.push(element);
                });
            }
        },
    },
});

export const { propertySearchPush, setPropertySearch } =
    propertySearchSlice.actions;

export const fetchProperties =
    (
        selectedCity = "jember",
        selectedInvestmens,
        selectedCategories,
        uid,
        updated_at
    ) =>
    async (dispatch) => {
        let categories = Array.isArray(selectedCategories)
            ? selectedCategories
                  .map((category) => category.name)
                  .join(",")
                  .toLowerCase()
            : selectedCategories;

        const fetchCall =
            uid && updated_at
                ? `https://api.sepakat.id/firebase//properties?city=${selectedCity}&transaction=${selectedInvestmens.toLowerCase()}&categories=${categories}&uid=${uid}&updated_at=${updated_at}`
                : `https://api.sepakat.id/firebase//properties?city=${selectedCity}&transaction=${selectedInvestmens.toLowerCase()}&categories=${categories}`;

        await fetch(fetchCall)
            .then(async (res) => {
                return await res.json();
            })
            .then((res) => {
                const element = [];
                for (let i = 0; i < res.data.length; i++) {
                    element.push(res.data[i]);
                    if (uid && updated_at)
                        dispatch(propertySearchPush(res.data[i]));
                }
                // console.log(res.data);
                if (res.data.length <= 0 && !(uid && updated_at)) {
                    return Swal.fire({
                        title: "Info!",
                        text: "Tidak ada Properti yang ditampilkan. Silahkan isi form dengan sesuai!",
                        icon: "info",
                    })(
                        setTimeout(() => {
                            window.location.href = "/pencarian";
                        }, 4000)
                    );
                }
                if (!(uid && updated_at)) dispatch(setPropertySearch(element));
            })
            .catch((err) => {
                console.log(err);
                Swal.fire({
                    title: "Info!",
                    text: "Silahkan Pilih Properti dan Jenis Properti dengan valid!",
                    icon: "info",
                    confirmButtonText: "Ok",
                });
            });
    };

export const propertySearchStore = (state) => state.propertySearch;

export const propertySearchIsEmpty = (state) =>
    state.propertySearch.length === 0;

export default propertySearchSlice.reducer;

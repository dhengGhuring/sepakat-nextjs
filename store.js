import { configureStore } from "@reduxjs/toolkit";
import articleSlice from "./reducers/articleSlice";
import propertySearchSlice from "./reducers/propertySearchSlice";
import propertySlice from "./reducers/propertySlice";
import recommendationPropertySlice from "./reducers/recommendationPropertySlice";
import videoSlice from "./reducers/videoSlice";

export const store = configureStore({
    reducer: {
        video: videoSlice,
        article: articleSlice,
        property: propertySlice,
        recommendationProperty: recommendationPropertySlice,
        propertySearch: propertySearchSlice,
    },
    devTools: process.env.NODE_ENV !== "production",
});

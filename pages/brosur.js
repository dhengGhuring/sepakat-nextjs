import React from "react";
import { useState } from "react";
import { useCallback } from "react";
import { useEffect } from "react";
// import { Document, Page, pdfjs } from "react-pdf/dist/esm/entry.webpack";
import { Document, Page, pdfjs } from "react-pdf";
import { Button } from "@mantine/core";
import "react-pdf/dist/esm/Page/AnnotationLayer.css";
import "react-pdf/dist/esm/Page/TextLayer.css";
import style from "../styles/brosurPage.module.css";

const BrosurPage = (props) => {
  const [property, setProperty] = useState(null);
  const [fetching, setFetching] = useState(false);
  const [brochure, setBrochure] = useState(null);

  async function fetchDetailProperty(id) {
    await fetch(`https://api.sepakat.id/firebase/property/${id}`)
      .then(async (res) => {
        return await res.json();
      })
      .then((res) => {
        const element = res.data;
        console.log(element);
        setProperty(element);
      });
  }

  const fetchAPI = useCallback(
    async (id) => {
      if (!property && id) {
        await fetchDetailProperty(id);
      }
    },
    [property]
  );

  useEffect(() => {
    setBrochure(localStorage.getItem("brochure"));
    console.log(localStorage.getItem("brochure"));
    pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
  }, [brochure, fetchAPI, fetching]);

  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);

  const onDocumentLoadSuccess = ({ numPages }) => {
    setNumPages(numPages);
  };

  const goToPrevPage = () =>
    setPageNumber(pageNumber - 1 <= 1 ? 1 : pageNumber - 1);

  const goToNextPage = () =>
    setPageNumber(pageNumber + 1 >= numPages ? numPages : pageNumber + 1);

  // return brochure && <Viewer fileUrl={brochure} />;

  return (
    brochure && (
      <div
        className="d-flex flex-column"
        style={{
          marginTop: 70,
        }}
      >
        <nav className={style.nav}>
          <button
            onClick={goToPrevPage}
            className={`${style.button} ${style.previous}`}
          >
            Prev
          </button>
          <button
            onClick={goToNextPage}
            className={`${style.button} ${style.next}`}
          >
            Next
          </button>
          <p className={style.p}>
            Page {pageNumber} of {numPages}
          </p>
          <a href={brochure} rel="noreferrer" target="_blank">
            <Button color="orange" radius="md" size="xl" ml={20}>
              Download
            </Button>
          </a>
        </nav>

        <Document
          file={brochure}
          onLoadSuccess={onDocumentLoadSuccess}
          error={"loading"}
        >
          <Page pageNumber={pageNumber} className={style.page} />
        </Document>
      </div>
    )
  );
};

export default BrosurPage;
// import React from "react";

// export default function brosur() {
//   return <div>brosur</div>;
// }

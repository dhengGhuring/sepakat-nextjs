import { motion } from 'framer-motion';
import style from '../styles/HousingDetailPageStyle.module.css';
import styleHousing from '../styles/HousingPageStyle.module.css';
import Link from 'next/link';
import { useRouter } from 'next/router';
import lokasiPerumStyle from '../Components/lokasiPerumahan/lokasiPerumCompoStyle.module.css';
// import "../../index.css";
import { Image, Title, Container, createStyles } from '@mantine/core';
// import { useSearchParams } from "react-router-dom";
import { Carousel } from '@mantine/carousel';
import NameHouseCompo from '../Components/detailHouse/namehouse-component';
import ButtonCompo from '../Components/button/button-component';
import ListFasilitasUnit from '../Components/fasilitasUnit/listfasilitasunit-component';
import CardDekstopCompo from '../Components/cardDekstop/carddekstop-component';
import InfoCardCompo from '../Components/infoCard/infocard-component';
import JumboTron2 from '../Components/jumbotron-2/jumbotron2-component';
import CardMobileCompo from '../Components/cardMobile/cardmobile-component';
import LokasiPerumCompo from '../Components/lokasiPerumahan/lokasiperumahan-component';
import { useState, useEffect } from 'react';
import { FooterCompo } from '../Components';
import { BsXCircle } from 'react-icons/bs';
import DescHouseDetailCompo from '../Components/descriptionHouse/descriptionhousedetail-component copy';
import capitalizeWord from '../utility/capitalizeWord';
import SitePlan from '../Components/siteplan/SitePlan';

const useStyle = createStyles((theme) => ({
  image: {
    marginTop: 30,
    height: 500,
    backgroundSize: 'cover',
    borderRadius: 30,
    '@media (max-width: 600px)': {
      height: 500,
    },
  },
  CardDekstopContainer: {
    '@media (max-width: 600px)': {
      display: 'none',
    },
    '@media (min-width: 600px)': {
      display: 'none',
    },
    '@media (min-width: 922px)': {
      display: 'block',
    },
    sectionPromotion: {
      height: '100vh',
    },
    root: {
      '@media (max-width: 600px)': {
        paddingLeft: 30,
        gap: 10,
      },
    },
    control: {
      backgroundColor: '#F2EEC8',
      color: '#F56600',
      borderColor: '#F2EEC8',
      opacity: 1,
    },
    sectionPromotion: {
      height: '100vh',
    },
  },
}));
export default function HousingDetailPage(props) {
  //   const [searchParams, setSearchParams] = useSearchParams();
  const router = useRouter();
  // const [fetching, setFetching] = useState(false);
  const [typehouse, setTypehouse] = useState(null);
  const [property, setProperty] = useState(null);
  const [otherType, setOtherType] = useState(null);
  // const [propertyId, setPropertyId] = useState(null);
  const [id, setId] = useState(null);
  const [open, setOpen] = useState(false);
  const { classes } = useStyle();

  // async function fetchDetailProperty(id) {
  //   return await fetch(
  //     `https://api.sepakat.id/firebase/property/${router.query.id}`
  //   )
  //     .then(async (res) => {
  //       return await res.json();
  //     })
  //     .then((res) => {
  //       const element = res.data;
  //       setOtherType(element);
  //       return element;
  //     });
  // }

  // async function fetchTypeProperty(property_id, uid) {
  //   fetch(
  //     `https://api.sepakat.id/firebase/property/${router.query.property_id}/${uid}`
  //   )
  //     .then((res) => {
  //       return res.json();
  //     })
  //     .then((res) => {
  //       const element = res.data;
  //       setProperty(element);
  //       setTypehouse(element.type_house);
  //     });
  // }

  // useEffect(() => {
  //   const id = router.query.id;
  //   const propertyId = router.query.property_id;
  //   setId(id);
  //   setPropertyId(propertyId);

  //   if (!fetching) {
  //     fetchTypeProperty(propertyId, id).then((value) => setFetching(true));
  //     fetchDetailProperty(propertyId).then((value) => setFetching(true));
  //   }
  // }, [fetching, router.query.id, propertyId, id, router]);

  useEffect(() => {
    const id = router.query.id;
    setId(id);
    const propertyId = router.query.property_id;

    setTimeout(() => {
      async function fetchDetailProperty(id) {
        return await fetch(`https://api.sepakat.id/firebase/property/${id}`)
          .then(async (res) => {
            return await res.json();
          })
          .then((res) => {
            const element = res.data;
            setOtherType(element);
            return element;
          });
      }

      async function fetchTypeProperty(propertyId, uid) {
        fetch(`https://api.sepakat.id/firebase/property/${propertyId}/${uid}`)
          .then((res) => {
            return res.json();
          })
          .then((res) => {
            const element = res.data;
            setProperty(element);
            setTypehouse(element.type_house);
          });
      }
      if (id && propertyId) {
        fetchTypeProperty(propertyId, id);
        fetchDetailProperty(propertyId);
      }
    }, 1000);
  }, [router, router.query.id, router.query.property_id]);

  return (
    property &&
    typehouse && (
      <motion.div
        className={`${style.main}`}
        initial={{ width: 0 }}
        animate={{ width: '100%' }}
        exit={{ x: window.innerWidth, transition: { duration: 1 } }}
      >
        {open ? (
          <div
            className={classes.containerGallery}
            style={{
              width: '100%',
              height: '100vh',
              backgroundColor: 'rgba(0, 0, 0, 0.8)',
              position: 'fixed',
              zIndex: 3,
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              overflow: 'hidden',
              transition: '0.5s',
            }}
          >
            <button
              style={{
                position: 'absolute',
                top: '50px',
                right: '30px',
                border: 'none',
                background: 'none',
              }}
              onClick={() => {
                setOpen(false);
              }}
            >
              <BsXCircle color="white" size={30} />
            </button>
            <Image
              src={typehouse?.images[0]}
              alt=""
              style={{
                marginTop: '-100px',
                width: 'auto',
                maxWidth: '90%',
                maxHeight: '60%',
                borderRadius: '10px',
              }}
            />
          </div>
        ) : null}
        <div className={`container ${style.containerImage}`}>
          <Image
            src={typehouse?.images[0]}
            className={classes.image}
            height={500}
            onClick={() => {
              setOpen(true);
            }}
            mt={100}
            style={{
              cursor: 'pointer',
            }}
            alt=""
          />
        </div>
        <div className="container-fluid">
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-12 col-lg-6 p-0 me-3">
                <NameHouseCompo
                  key={typehouse.id}
                  type={typehouse.name}
                  address={property.name}
                  price={typehouse.price}
                  icon1={typehouse.icon1}
                  unitFacilities={typehouse.unit_facilities}
                />
                <ButtonCompo uriBrochure={property.brochure} />
                <DescHouseDetailCompo
                  key={typehouse.idDesc}
                  description1={typehouse.description}
                />
                <Container>
                  <Title color="#F56600" size={20} mt={40} mb={10}>
                    Fasilitas Unit
                  </Title>
                </Container>
                <div className="row">
                  {typehouse.unit_facilities &&
                    Object.keys(typehouse.unit_facilities).map((key, idx) => {
                      const element = typehouse.unit_facilities[key];
                      if (
                        typeof element === 'string' &&
                        element.length > 0 &&
                        element !== '-' &&
                        element !== '0'
                      ) {
                        return (
                          <div className="col-5 col-md-6 col-lg-4" key={idx}>
                            <ListFasilitasUnit
                              icon={key}
                              title={capitalizeWord(element)}
                            />
                          </div>
                        );
                      } else if (typeof element === 'boolean' && element) {
                        return (
                          <div className="col-5 col-md-6 col-lg-4" key={idx}>
                            <ListFasilitasUnit
                              icon={key}
                              title={capitalizeWord(element ? 'Ada' : '-')}
                            />
                          </div>
                        );
                      } else {
                        return <></>;
                      }
                    })}
                </div>
                {otherType ? (
                  otherType.type_houses?.length > 0 && (
                    <div
                      className={`${styleHousing['rincian-4']} ${styleHousing.item3} ${styleHousing.tpRumah}`}
                    >
                      <Title color="#F56600" size={20} mt={40} mb={20}>
                        Tipe Lainnya
                      </Title>
                      {/* <!-- For Dekstop--> */}
                      <div
                        className={
                          (styleHousing.carouselContainerDekstop,
                          style.carouselContainerDekstop)
                        }
                      >
                        <Carousel
                          controlsOffset="10%"
                          controlSize={40}
                          align="start"
                          height={300}
                          slideSize="10%"
                          dragFree
                          classNames={{
                            control: classes.control,
                          }}
                          styles={{
                            control: {
                              '&[data-inactive]': {
                                opacity: 0.5,
                                cursor: 'default',
                              },
                            },
                          }}
                        >
                          {otherType?.type_houses &&
                            otherType?.type_houses?.map((item, key) => {
                              if (item.id === id) {
                                return;
                              }
                              return (
                                <Link
                                  href={`/detail-tipe?id=${item.id}&property_id=${otherType.id}`}
                                  key={key}
                                >
                                  <CardDekstopCompo
                                    image={item.images[0]}
                                    title={item.name}
                                    address={item.property_type}
                                  />
                                </Link>
                              );
                            })}
                        </Carousel>
                      </div>
                      {/* <!-- End Dekstop --> */}

                      {/* <!-- For Mobile --> */}
                      <div
                        className={
                          (styleHousing.carouselContainerMobile,
                          style.carouselContainerMobile)
                        }
                      >
                        <div
                          className={`row row-cols-2 row-cols-md-2 g-4 ${
                            (styleHousing.rowCardMobileCarousel,
                            style.rowCardMobileCarousel)
                          }`}
                        >
                          {otherType?.type_houses &&
                            otherType?.type_houses?.map((item, key) => {
                              if (item.id === id) {
                                return;
                              }
                              return (
                                <CardMobileCompo
                                  key={key}
                                  to={`/detail-tipe?id=${item.id}&property_id=${otherType.id}`}
                                  image={item.images[0]}
                                  title={item.name}
                                  address={item.property_type}
                                />
                              );
                            })}
                        </div>
                      </div>
                      {/* <!-- End For Mobile --> */}
                    </div>
                  )
                ) : (
                  <Title color="#F56600" size={20} mt={40} mb={10}>
                    Tidak Terdapat Data
                  </Title>
                )}
                <InfoCardCompo />
              </div>
              <div
                className={`col-12 col-md-6 col-lg-5 p-0 ${styleHousing.item4} ${styleHousing.sitePlan} ${styleHousing.pembungkus2}`}
              >
                <Title color="#F56600" size={20} my={20}>
                  Site Plan
                </Title>
                <SitePlan
                  sitePlan={property.siteplan?.image}
                  background={property.siteplan?.background}
                />
                <Title color="#F56600" size={20} my={20}>
                  Lokasi Perumahan
                </Title>
                <div
                  className={`${lokasiPerumStyle.lokasiPerumahan} p-3 rounded-4 shadow-lg mb-5 bg-body rounded mt-4`}
                >
                  {property.near_facilities?.map((item, key) => {
                    return (
                      <LokasiPerumCompo
                        index={key}
                        icon={require('./assets/images/Card-Frame 502.png')}
                        title={item.name}
                        time={item.mileage + ' Menit'}
                        key={key}
                      />
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div></div>
        <JumboTron2 />
        <FooterCompo />
      </motion.div>
    )
  );
}

import React, { useState, useEffect, use } from 'react';
import {
  Box,
  Center,
  Text,
  Heading,
  Switch,
  Flex,
  UnorderedList,
  ListItem,
} from '@chakra-ui/react';
import { FooterCompo2 } from '../Components';
import style from '../styles/SyaratKetentuanPageStyle.module.css';

export default function KebijakanPrivasi() {
  const [lang, setLang] = useState('id');
  useEffect(() => {
    setLang('eg');
  }, []);
  return (
    <Box backgroundColor={'white'} minH={'100vh'}>
      <Box
        minH={{ base: '50vh', md: '70vh' }}
        my={'auto'}
        className={style.container}
        position={'relative'}
      >
        <Center>
          <Heading
            fontSize={{ base: '3xl', md: '4xl', lg: '6xl' }}
            w={'100%'}
            textAlign={'center'}
            fontWeight={'bold'}
            fontFamily={'Poppins, sans-serif'}
            color={'white'}
            position={'absolute'}
            top={{ base: '60%', md: '50%' }}
            left={'50%'}
            transform={{
              base: 'translate(-50%, -40%)',
              md: 'translate(-50%, -50%)',
            }}
          >
            Privacy Policy
          </Heading>
        </Center>
      </Box>
      <Box
        maxW={{ base: '85%', md: '70%' }}
        mx={'auto'}
        my={20}
        textAlign={'justify'}
        display={'flex'}
        flexDirection={'column'}
        gap={7}
      >
        <Flex gap={2}>
          <Text>EN</Text>
          <Switch
            size={'md'}
            value={true}
            onChange={() => {
              if (lang === 'id') {
                setLang('en');
              } else {
                setLang('id');
              }
            }}
          />
          <Text>ID</Text>
        </Flex>
        {lang === 'id' ? (
          <>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Kebijakan dan privasi
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                Andalan Solusi Cemerlang membangun aplikasi Sepakat sebagai
                aplikasi Komersial. LAYANAN ini disediakan oleh Andalan Solusi
                Cemerlang dan dimaksudkan untuk digunakan apa adanya. Halaman
                ini digunakan untuk memberi tahu pengunjung mengenai kebijakan
                kami dengan pengumpulan, penggunaan, dan pengungkapan Informasi
                Pribadi jika ada yang memutuskan untuk menggunakan Layanan kami.
                Jika Anda memilih untuk menggunakan Layanan kami, maka Anda
                menyetujui pengumpulan dan penggunaan informasi sehubungan
                dengan kebijakan ini. Informasi Pribadi yang kami kumpulkan
                digunakan untuk menyediakan dan meningkatkan Layanan. Kami tidak
                akan menggunakan atau membagikan informasi Anda dengan siapa pun
                kecuali sebagaimana dijelaskan dalam Kebijakan Privasi ini.
                Istilah yang digunakan dalam Kebijakan Privasi ini memiliki arti
                yang sama seperti dalam Syarat dan Ketentuan kami, yang dapat
                diakses di Sepakat kecuali ditentukan lain dalam Kebijakan
                Privasi ini.
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Pengumpulan dan Penggunaan Informasi
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                Untuk pengalaman yang lebih baik, saat menggunakan Layanan kami,
                kami mungkin meminta Anda untuk memberikan kami informasi
                pengenal pribadi tertentu, termasuk namun tidak terbatas pada
                Lokasi. Informasi yang kami minta akan disimpan oleh kami dan
                digunakan seperti yang dijelaskan dalam kebijakan privasi ini.
                Aplikasi ini menggunakan layanan pihak ketiga yang dapat
                mengumpulkan informasi yang digunakan untuk mengidentifikasi
                Anda. Tautan ke kebijakan privasi penyedia layanan pihak ketiga
                yang digunakan oleh aplikasi Layanan Google Play
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Log Data
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                Kami ingin memberi tahu Anda bahwa setiap kali Anda menggunakan
                Layanan kami, jika terjadi kesalahan dalam aplikasi, kami
                mengumpulkan data dan informasi (melalui produk pihak ketiga) di
                ponsel Anda yang disebut Data Log. Data Log ini dapat mencakup
                informasi seperti alamat Protokol Internet (“IP”) perangkat
                Anda, nama perangkat, versi sistem operasi, konfigurasi aplikasi
                saat menggunakan Layanan kami, waktu dan tanggal penggunaan
                Layanan oleh Anda, dan statistik lainnya
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Cookies
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                Cookie adalah file dengan sejumlah kecil data yang biasanya
                digunakan sebagai pengidentifikasi unik anonim. Ini dikirim ke
                browser Anda dari situs web yang Anda kunjungi dan disimpan di
                memori internal perangkat Anda. Layanan ini tidak menggunakan
                “cookies” ini secara eksplisit. Namun, aplikasi dapat
                menggunakan kode dan perpustakaan pihak ketiga yang menggunakan
                {'cookies'} untuk mengumpulkan informasi dan meningkatkan
                layanan mereka. Anda memiliki pilihan untuk menerima atau
                menolak cookie ini dan mengetahui kapan cookie dikirim ke
                perangkat Anda. Jika Anda memilih untuk menolak cookie kami,
                Anda mungkin tidak dapat menggunakan beberapa bagian dari
                Layanan ini.
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Penyedia Jasa
              </Heading>
              <Box fontSize={{ base: 'sm', md: 'md' }}>
                <Text>
                  Kami dapat memperkerjakan perusahaan dan individu pihak ketiga
                  karena alasan berikut:
                </Text>
                <UnorderedList ml={10}>
                  <ListItem>Untuk memfasilitasi Layanan kami;</ListItem>
                  <ListItem>Untuk menyediakan Layanan atas nama kami;</ListItem>
                  <ListItem>Untuk melakukan layanan terkait Layanan;</ListItem>
                  <ListItem>
                    atau Untuk membantu kami dalam menganalisis bagaimana
                    Layanan kami digunakan.
                  </ListItem>
                </UnorderedList>
                <Text>
                  Kami ingin memberi tahu pengguna Layanan ini bahwa pihak
                  ketiga ini memiliki akses ke informasi Pribadi mereka.
                  Alasannya adalah untuk melakukan tugas yang diberikan kepada
                  mereka atas nama kita. Namun, mereka berkewajiban untuk tidak
                  mengungkapkan atau menggunakan informasi tersebut untuk tujuan
                  yang lain.
                </Text>
              </Box>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Keamanan
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                Kami menghargai kepercayaan Anda dalam memberikan Informasi
                Pribadi Anda kepada kami, oleh karena itu kami berusaha untuk
                menggunakan cara yang dapat diterima secara komersial untuk
                melindunginya. Tetapi ingat bahwa tidak ada metode transmisi
                melalui internet, atau metode penyimpanan elektronik yang 100%
                aman dan andal, dan kami tidak dapat menjamin keamanan
                mutlaknya.
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Tautan ke Situs lain
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                Layanan ini mungkin berisi tautan ke situs lain. Jika Anda
                mengklik tautan pihak ketiga, Anda akan diarahkan ke situs itu.
                Perhatikan bahwa situs eksternal ini tidak dioperasikan oleh
                kami. Oleh karena itu, kami sangat menyarankan Anda untuk
                meninjau Kebijakan Privasi situs web ini. Kami tidak memiliki
                kendali atas dan tidak bertanggung jawab atas konten, kebijakan
                privasi, atau praktik situs atau layanan pihak ketiga mana pun.
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Privasi Anak-Anak
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                Layanan ini tidak ditujukan kepada siapa pun yang berusia di
                bawah 13 tahun. Kami tidak dengan sengaja mengumpulkan informasi
                pengenal pribadi dari anak-anak di bawah 13 tahun. Jika kami
                menemukan bahwa seorang anak di bawah 13 tahun telah memberi
                kami informasi pribadi, kami segera menghapusnya dari server
                kami. Jika Anda adalah orang tua atau wali dan Anda mengetahui
                bahwa anak Anda telah memberikan informasi pribadi kepada kami,
                silakan hubungi kami sehingga kami dapat melakukan tindakan yang
                diperlukan.
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Perubahan pada Kebijakan Privasi Ini
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                Kami dapat memperbarui Kebijakan Privasi kami dari waktu ke
                waktu. Oleh karena itu, Anda disarankan untuk meninjau halaman
                ini secara berkala untuk setiap perubahan. Kami akan memberi
                tahu Anda tentang perubahan apa pun dengan memposting Kebijakan
                Privasi baru di halaman ini. Kebijakan ini berlaku mulai 6
                Agustus 2022
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Kontak Kami
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                Jika Anda memiliki pertanyaan atau saran tentang Kebijakan
                Privasi kami, jangan ragu untuk menghubungi kami di
                Andalansolusicemerlang@gmail.com.
              </Text>
            </Box>
          </>
        ) : (
          <>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Privacy Policy
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                Andalan Solusi Cemerlang built the Sepakat app as a Commercial
                app. This SERVICE is provided by Andalan Solusi Cemerlang and is
                intended for use as is. This page is used to inform visitors
                regarding our policies with the collection, use, and disclosure
                of Personal Information if anyone decided to use our Service. If
                you choose to use our Service, then you agree to the collection
                and use of information in relation to this policy. The Personal
                Information that we collect is used for providing and improving
                the Service. We will not use or share your information with
                anyone except as described in this Privacy Policy. The terms
                used in this Privacy Policy have the same meanings as in our
                Terms and Conditions, which are accessible at Sepakat unless
                otherwise defined in this Privacy Policy
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Information Collection and Use
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                For a better experience, while using our Service, we may require
                you to provide us with certain personally identifiable
                information, including but not limited to Location. The
                information that we request will be retained by us and used as
                described in this privacy policy. The app does use third-party
                services that may collect information used to identify you. Link
                to the privacy policy of third-party service providers used by
                the app Google Play Services
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Log Data
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                We want to inform you that whenever you use our Service, in a
                case of an error in the app we collect data and information
                (through third-party products) on your phone called Log Data.
                This Log Data may include information such as your device
                Internet Protocol (“IP”) address, device name, operating system
                version, the configuration of the app when utilizing our
                Service, the time and date of your use of the Service, and other
                statistics.
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Cookies
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                Cookies are files with a small amount of data that are commonly
                used as anonymous unique identifiers. These are sent to your
                browser from the websites that you visit and are stored on your
                {"device's"} internal memory. This Service does not use these
                “cookies” explicitly. However, the app may use third-party code
                and libraries that use “cookies” to collect information and
                improve their services. You have the option to either accept or
                refuse these cookies and know when a cookie is being sent to
                your device. If you choose to refuse our cookies, you may not be
                able to use some portions of this Service.
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Service Providers
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                We may employ third-party companies and individuals due to the
                following reasons:
                <UnorderedList ml={10}>
                  <ListItem>To facilitate our Service;</ListItem>
                  <ListItem>To provide the Service on our behalf;</ListItem>
                  <ListItem>To perform Service-related services;</ListItem>
                  <ListItem>
                    or To assist us in analyzing how our Services is used.
                  </ListItem>
                </UnorderedList>
                We want to inform users of this Services that these third
                parties have access to their Personal Information. The reason is
                to perform the tasks assigned to them on our behalf. However,
                they are obligated not to disclose or use the information for
                any other purpose.
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Security
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                We value your trust in providing us your Personal Information,
                thus we are striving to use commercially acceptable means of
                protecting it. But remember that no method of transmission over
                the internet, or method of electronic storage is 100% secure and
                reliable, and we cannot guarantee its absolute security.
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Links to Other Sites
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                This Service may contain links to other sites. If you click on a
                third-party link, you will be directed to that site. Note that
                these external sites are not operated by us. Therefore, we
                strongly advise you to review the Privacy Policy of these
                websites. We have no control over and assume no responsibility
                for the content, privacy policies, or practices of any
                third-party sites or services.
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                {"Childern's"} Privacy
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                These Services do not address anyone under the age of 13. We do
                not knowingly collect personally identifiable information from
                children under 13 years of age. In the case we discover that a
                child under 13 has provided us with personal information, we
                immediately delete this from our servers. If you are a parent or
                guardian and you are aware that your child has provided us with
                personal information, please contact us so that we will be able
                to do the necessary actions.
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Changes to This Privacy Policy
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                We may update our Privacy Policy from time to time. Thus, you
                are advised to review this page periodically for any changes. We
                will notify you of any changes by posting the new Privacy Policy
                on this page. This policy is effective as of 2022-06-08
              </Text>
            </Box>
            <Box>
              <Heading
                fontSize={{ base: 'lg', md: '2xl' }}
                fontFamily={'Poppins, sans-serif'}
              >
                Contact Us
              </Heading>
              <Text fontSize={{ base: 'sm', md: 'md' }}>
                If you have any questions or suggestions about our Privacy
                Policy, do not hesitate to contact us at
                Andalansolusicemerlang@gmail.com.
              </Text>
            </Box>
          </>
        )}
      </Box>
      <FooterCompo2 />
    </Box>
  );
}

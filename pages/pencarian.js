import style from "../styles/SearchPageStyle.module.css";
import { motion } from "framer-motion";
import JumboTron2 from "../Components/jumbotron-2/jumbotron2-component";
import ToolTip from "../Components/tooltip/toolTip-component";

import CarouselCompo2 from "../Components/carousel2/carousel2-component";
import { Carousel } from "@mantine/carousel";
import { useEffect, useState } from "react";
import { createStyles } from "@mantine/core";
import { FooterCompo } from "../Components";
import searchProperty from "../data/Search_Property.json";
import SearchBoxV2Component from "../Components/searchbox/searchboxv2-component";
import SuggestCityComponent from "../Components/suggestCity/suggestcity-component";
import { useDispatch, useSelector } from "react-redux";
import {
    fetchRecommendationProperty,
    recommendationPropertiesStore,
    recommendationPropertyIsEmpty,
} from "../reducers/recommendationPropertySlice";
import Link from "next/link";
import { useRouter } from "next/router";
import { fetchProperties } from "../reducers/propertySearchSlice";
import Swal from "sweetalert2";
import capitalizeWord from "../utility/capitalizeWord";

const useStyle = createStyles((theme) => ({
    container: {
        "@media (max-width: 600px)": {
            paddingLeft: "0!important ",
        },
        searchBoxWrapper: {
            maxWidth: 300,
        },
    },
    sectionPromotion: {
        height: "100vh",
    },
}));

const SearchPage = () => {
    const categories = searchProperty.masterPropertyCategories;
    const invesments = searchProperty.masterInvesments;

    const [selectedCategories, setselectedCategories] = useState(null);
    const [selectedInvestmens, setselectedInvestmens] = useState(null);
    const [selectedCity, setSelectedCity] = useState(null);
    const dispatch = useDispatch();
    const router = useRouter();

    const { classes } = useStyle();

    const properties = useSelector(recommendationPropertiesStore);

    const recommendationPropertyEmpty = useSelector(
        recommendationPropertyIsEmpty
    );

    function handleLimitCategory(category) {
        if (selectedCategories?.includes(category)) {
            const list = selectedCategories ?? [];
            const idx = list.indexOf(category);
            list.splice(idx, 1);
            setselectedCategories(list);
        } else {
            const list = selectedCategories
                ? [...selectedCategories, category]
                : [category];
            if (list.length > 2) {
                list.shift();
            }
            setselectedCategories(list);
        }
    }

    function handleLimitJenis(invest) {
        setselectedInvestmens(invest);
    }

    useEffect(() => {
        if (recommendationPropertyEmpty) {
            (async function fetch() {
                dispatch(await fetchRecommendationProperty());
            })();
        }
    }, [properties, dispatch, recommendationPropertyEmpty]);

    async function searchProperties() {
        try {
            if (
                selectedInvestmens?.length > 0 &&
                selectedCategories?.length > 0
            ) {
                dispatch(
                    await fetchProperties(
                        selectedCity ?? "jember",
                        selectedInvestmens,
                        selectedCategories
                    )
                );
                router.push(
                    `/hasil-pencarian?city=${
                        selectedCity ?? "jember"
                    }&transaction=${selectedInvestmens.toLowerCase()}&categories=${selectedCategories
                        .map((category) => category.name)
                        .join(",")
                        .toLowerCase()}`
                );
            } else {
                Swal.fire({
                    title: "Info!",
                    text: "Silahkan Pilih Properti dan Jenis Properti",
                    icon: "info",
                    confirmButtonText: "Ok",
                });
            }
        } catch (error) {
            console.log(error).catch((error) => {
                return Swal.fire({
                    title: "Info!",
                    text: "Silahkan Pilih Properti dan Jenis Properti dengan valid!",
                    icon: "info",
                    confirmButtonText: "Ok",
                });
            });
        }
    }

    // start change code - ijlalWindhi
    const [cheked, setChecked] = useState([]);

    const isDisebled = (category) => {
        return cheked.length > 1 && cheked.indexOf(category) === -1;
    };

    const onChange = (category) => {
        if (cheked.indexOf(category) === -1) {
            setChecked([...cheked, category]);
        } else {
            setChecked(cheked.filter((item) => item !== category));
        }
    };
    // end change code - ijlalWindhi

    return (
        <motion.div
            initial={{ width: 0 }}
            animate={{ width: "100%" }}
            className="mt-5"
        >
            {/* Jumbotron Section */}
            <div className={`${style.jumbotron} jumbotron-fluid`}>
                <div className={`${style["input-container"]} input-container`}>
                    <div className={`container ${classes.searchBoxWrapper}`}>
                        {/* <SearchBoxCompo /> */}
                        <SearchBoxV2Component
                            setSelectedCity={setSelectedCity}
                        />
                        <SuggestCityComponent suggestion={[]} />
                    </div>
                </div>
            </div>
            {/* End Jumbotron */}
            {/* Main Section */}
            <main className={`${style.main}`}>
                <div className={`${style["container-fluid"]} mb-5`}>
                    <div className="row">
                        {/* Pilih Properti Section */}
                        <div
                            className={`${style.rincianProperti} col-12 col-md-0 col-lg-11 p-0  mb-3 mt-4`}
                        >
                            <div
                                className={`d-flex flex-row align-items-center mb-2`}
                            >
                                <h1>Pilih Properti</h1>
                                <ToolTip
                                    message={"Maksimal hanya bisa memilih 2"}
                                />
                                {cheked.length > 1 && (
                                    <h1 className="text-danger ms-2">
                                        Maksimal hanya bisa memilih 2
                                    </h1>
                                )}
                            </div>
                            {/* Checkbox Button */}
                            <div className="button-check"></div>
                            {/* End Checkbox Button */}
                            <div className={style.tombolPilih}>
                                <div
                                    className={`${style.pilihProperti} ${style.selectpicker}`}
                                >
                                    <div
                                        className={`${style["form-check"]} me-2`}
                                    >
                                        {categories &&
                                            categories.map((category, key) => (
                                                <div key={key}>
                                                    <label
                                                        className="form-check-label"
                                                        htmlFor={category.name}
                                                    >
                                                        <input
                                                            className="form-check-input"
                                                            type="checkbox"
                                                            value={
                                                                category.name
                                                            }
                                                            disabled={isDisebled(
                                                                category.name
                                                            )}
                                                            onChange={(e) =>
                                                                onChange(
                                                                    category.name
                                                                )
                                                            }
                                                            id={category.name}
                                                            name="chk"
                                                            onClick={() => {
                                                                handleLimitCategory(
                                                                    category
                                                                );
                                                            }}
                                                        />
                                                        <span>
                                                            {capitalizeWord(
                                                                category.name
                                                            )}
                                                        </span>
                                                    </label>
                                                </div>
                                            ))}
                                    </div>
                                </div>
                                <div
                                    className={`${style.pProperti} d-flex flex-row align-items-center mt-4 mb-2`}
                                >
                                    <h1>Jenis Properti</h1>
                                    <ToolTip
                                        message={
                                            "Minimal memilih 1 Jenis Properti"
                                        }
                                    />
                                </div>
                                <div
                                    className={`${style.pilihProperti} ${style.jenisProperti} mb-3`}
                                >
                                    <div className={style["form-check"]}>
                                        {invesments &&
                                            invesments.map((invest, key) => (
                                                <div key={key}>
                                                    <label
                                                        className="form-check-label"
                                                        htmlFor={invest}
                                                        name="chk"
                                                    >
                                                        <input
                                                            className="form-check-input"
                                                            type="checkbox"
                                                            value={invest}
                                                            id={invest}
                                                            {...(selectedInvestmens &&
                                                            selectedInvestmens?.includes(
                                                                invest
                                                            )
                                                                ? "checked"
                                                                : "")}
                                                            onClick={() =>
                                                                handleLimitJenis(
                                                                    invest
                                                                )
                                                            }
                                                        />
                                                        <span>
                                                            {invest[0].toUpperCase() +
                                                                invest.substring(
                                                                    1
                                                                )}
                                                        </span>
                                                    </label>
                                                </div>
                                            ))}
                                    </div>
                                </div>
                            </div>
                            <button
                                type="submit"
                                className={`btn ${style["btn-submit"]} btn-success mt-4 rounded-pill`}
                                onClick={async () => await searchProperties()}
                            >
                                <p className="m-0">Cari Properti</p>
                            </button>
                        </div>
                        {/* End Pilihan Properti */}

                        <div
                            className={`${style.perumahanLn} ${style["rincian-6"]} col-12 col-md-0 col-lg-12 p-0  mt-3`}
                        >
                            <h1>Rekomendasi Properti</h1>
                            <Carousel
                                controlsOffset="10%"
                                controlSize={40}
                                slideSize={350}
                                slideGap={30}
                                align="start"
                                pl={250}
                                className={classes.container}
                                styles={{
                                    control: {
                                        backgroundColor: "#F2EEC8",
                                        color: "#F56600",
                                        borderColor: "#F2EEC8",
                                        "&[data-inactive]": {
                                            opacity: 0.5,
                                            cursor: "default",
                                        },
                                    },
                                }}
                            >
                                {properties?.map((item, key) => {
                                    return (
                                        <Link
                                            href={{
                                                pathname: "/properti",
                                                query: { id: item.id },
                                            }}
                                            key={key}
                                        >
                                            <CarouselCompo2
                                                image={item.images[0]}
                                                title={item.name}
                                                address={`${
                                                    item.address.street
                                                }, ${
                                                    item.address.lower_city[0].toUpperCase() +
                                                    item.address.lower_city.substring(
                                                        1
                                                    )
                                                }`}
                                            />
                                        </Link>
                                    );
                                })}
                            </Carousel>
                        </div>
                    </div>
                </div>
                <JumboTron2 />
            </main>
            {/* End Main */}
            <FooterCompo />
        </motion.div>
    );
};

export default SearchPage;

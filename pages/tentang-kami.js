import { motion } from "framer-motion";
import { createStyles } from "@mantine/core";
import { useRef } from "react";
// Import AOS Animation
import Aos from "aos";
import "aos/dist/aos.css";

import FeatureCompo from "../Components/featureSepakat/feature-component";
import JumboTron2 from "../Components/jumbotron-2/jumbotron2-component";
import JumboTron3Compo from "../Components/jumbotron-3/jumbotron3-component";
import PartnersCompo from "../Components/partners/partners-component";
import { useEffect } from "react";
import { FooterCompo } from "../Components";
import GallerySepakat from "../Components/gallerySepakat/GallerySepakat";

const useStyle = createStyles((theme) => ({
    app: {
        overflow: "hidden",
    },
    title: {
        fontFamily: "Poppins, sans-serif",
        color: "#F56600",
        fontSize: 48,
        fontWeight: 700,
        marginTop: 50,
        marginBottom: 30,
        "@media (max-width: 600px)": {
            fontSize: 41,
        },
    },
    jumbotron2: {
        marginTop: 50,
        height: "100vh",
    },
}));
const AboutUsPage = () => {
    const { classes } = useStyle();
    useEffect(() => {
        Aos.init({
            duration: 200,
            easing: "ease-in-sine",
            offset: 200,
            delay: 100,
        });
    }, []);
    const ref = useRef(null);
    return (
        <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }}>
            <div className={`${classes.app} bg-white`} data-aos="fade-up">
                <JumboTron3Compo data-aos="fade-up" />
                <h1
                    className={`${classes.title} text-center`}
                    data-aos="fade-up"
                >
                    Layanan Kami
                </h1>
                <FeatureCompo />

                {/* Aktivitas Section */}
                <div className="container">
                    <h1
                        className={`${classes.title} text-center`}
                        data-aos="fade-up"
                    >
                        Aktivitas Kami
                    </h1>
                    <GallerySepakat />
                </div>
                {/* End Aktivitas Section */}

                <div className={classes.kerjasamaSection}>
                    <h1
                        className={`${classes.title} text-center`}
                        data-aos="zoom-in"
                        data-aos-duration="300"
                    >
                        Kerjasama dan Kemitraan
                    </h1>
                    <PartnersCompo />
                </div>
                <JumboTron2 />
                <FooterCompo />
            </div>
        </motion.div>
    );
};

export default AboutUsPage;

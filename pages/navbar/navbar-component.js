import React from 'react';
import { useState } from 'react';
import { Tooltip } from '@mantine/core';
import style from './navbarCompoStyle.module.css';
import playstore from '../../pages/assets/images/Button-Download-Playstore-Large.webp';
import appstore from '../../pages/assets/images/Button-Download-Appstore-Large.webp';
import sepakatLogo from '../../pages/assets/images/icon-logo.webp';
import Chevron from '../../pages/assets/icons/icon-chevron.svg';
import Propertiku from '../../pages/assets/images/Logo Propertiku.webp';
import Alert from '../../Components/alert/Alert';
import 'bootstrap/dist/css/bootstrap.css';
import Image from 'next/image';
import Link from 'next/link';

import { createStyles } from '@mantine/core';

const variants = {
  open: { opacity: 1, x: 0 },
  closed: { opacity: 0, x: '-100%' },
};

const useStyle = createStyles((theme) => ({
  iconChevron: {
    transform: 'rotate(180deg)',
    transition: '0.3s ease-in-out',
  },
}));

const NavbarCompo = () => {
  const [isOpen, setIsOpen] = useState('none');
  const [opened, setOpened] = useState(false);
  const [isOpenChevron, setIsOpenChevron] = useState(false);
  const { classes } = useStyle();
  return (
    <>
      <Alert
        opened={opened}
        setOpened={setOpened}
        image={Propertiku}
        heading={'Propertiku'}
        description={'Segera Hadir '}
      />
      <div
        className={`${style.navbar2} container-fluid sticky-top bg-light position-fixed`}
        style={{ zIndex: 100 }}
      >
        <div className="container-fluid navbar2">
          <div className="container">
            <nav className={`navbar navbar-expand-lg p-0 ${style.nav}`}>
              <div className="container-fluid">
                <Image
                  src={sepakatLogo}
                  alt=""
                  className={style.logoSepakat}
                  width={50}
                />
                <Link className={`navbar-brand ${style.brandName}`} href="/">
                  Sepakat
                </Link>
                <button
                  className="navbar-toggler"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#navbarNavDropdown"
                  aria-controls="navbarNavDropdown"
                  aria-expanded="false"
                  aria-label="Toggle navigation"
                  onClick={() => {
                    if (isOpen == 'none') {
                      setIsOpen('block');
                    } else {
                      setIsOpen('none');
                    }
                  }}
                >
                  <span className="navbar-toggler-icon"></span>
                </button>
                <div
                  className="collapse navbar-collapse"
                  id="navbarNavDropdown"
                  style={{ display: isOpen }}
                >
                  <ul className={`navbar-nav ${style.navUl}`}>
                    <li className={`nav-item ${style.navItems}`}>
                      <Link
                        className={`nav-link active ${style.item} me-4`}
                        aria-current="page"
                        href="/"
                      >
                        Beranda
                      </Link>
                    </li>
                    <li
                      className="nav-item dropdown"
                      style={{
                        display: 'none',
                      }}
                    >
                      <Link
                        className={`nav-link active dropdown-toggle me-4 ${style.item}`}
                        href="/#"
                        id="navbarDropdownMenuLink"
                        role="button"
                        data-bs-toggle="dropdown"
                        aria-expanded="false"
                      >
                        Gabung Sepakat
                      </Link>
                      <ul
                        className="dropdown-menu"
                        aria-labelledby="navbarDropdownMenuLink"
                      >
                        <li>
                          <Link
                            className={`dropdown-item ${style.item}`}
                            href="/#"
                          >
                            Jadi Agen
                          </Link>
                        </li>
                        <li>
                          <div className={`dropdown-item ${style.item}`}>
                            Karir
                          </div>
                        </li>
                      </ul>
                    </li>
                    <li
                      className={`nav-item dropdown ${style.konten}`}
                      onMouseEnter={() => {
                        setIsOpenChevron('show');
                      }}
                      onMouseLeave={() => {
                        setIsOpenChevron('none');
                      }}
                      onClick={() => {
                        setIsOpenChevron(
                          isOpenChevron === 'show' ? 'none' : 'show'
                        );
                      }}
                    >
                      <div
                        className={`d-flex flex-row me-4 justify-content-center`}
                        style={{ cursor: 'pointer' }}
                      >
                        <a
                          className={`nav-link active ${style.item1}`}
                          // id="navbarDropdownMenuLink"
                          // role="button"
                          // data-bs-toggle="dropdown"
                          // aria-expanded="false"
                        >
                          Konten
                        </a>
                        <Image
                          src={Chevron}
                          alt=""
                          className={`${style.iconChevron} ${
                            isOpenChevron === 'show' && classes.iconChevron
                          }`}
                        />
                      </div>
                      <ul
                        className={`dropdown-menu ${isOpenChevron} ${style.dropDownMenu}`}
                        aria-labelledby="navbarDropdownMenuLink"
                      >
                        <li>
                          <Link
                            className={`dropdown-item ${style.item}`}
                            href="/konten-video"
                          >
                            Video
                          </Link>
                        </li>
                        <li>
                          <Link
                            className={`dropdown-item ${style.item}`}
                            href="/konten-artikel"
                          >
                            Artikel
                          </Link>
                        </li>
                      </ul>
                    </li>
                    <li className="nav-item">
                      <Link
                        className={`nav-link active item1 me-4 ${style.item}`}
                        aria-current="page"
                        href="/tentang-kami"
                      >
                        Tentang Kami
                      </Link>
                    </li>
                    <li className="nav-item">
                      <div
                        onClick={() => {
                          setOpened(true);
                        }}
                        className={`nav-link active item1 me-4 ${style.item}`}
                        aria-current="page"
                        style={{ cursor: 'pointer' }}
                      >
                        Ajukan KPR
                      </div>
                    </li>
                    <li className="nav-item">
                      <Link
                        className={`nav-link active item1 me-4 ${style.item}`}
                        aria-current="page"
                        href="/undian-sepakat"
                      >
                        Gebyar Sepakat
                      </Link>
                    </li>
                  </ul>
                </div>
                <div className="d-flex gap-3 justify-content-end">
                  <div className={`${style.download} me-3`}>
                    <Link
                      href="https://play.google.com/store/apps/details?id=id.asc.sepakat"
                      target="_blank"
                    >
                      <Image
                        src={playstore}
                        alt=""
                        className="img-fluid"
                        priority
                      />
                    </Link>
                  </div>
                  <div className={style.download}>
                    <Tooltip label="Segera Hadir" color="green.9">
                      <div>
                        <a>
                          <Image
                            src={appstore}
                            alt=""
                            className="d-block img-fluid"
                          />
                        </a>
                      </div>
                    </Tooltip>
                  </div>
                </div>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </>
  );
};

export default NavbarCompo;

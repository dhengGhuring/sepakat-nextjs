import { createStyles, Center, Button } from "@mantine/core";
import { motion } from "framer-motion";
import { useRef } from "react";
import Aos from "aos";
import "aos/dist/aos.css";
import CardArticleCompo from "../Components/content/cardarticle-component-konten";
import JumboTron2 from "../Components/jumbotron-2/jumbotron2-component";
import { useEffect } from "react";
import { FooterCompo } from "../Components";
import {
    articleIsEnded,
    articlesIsEmpty,
    articlesStore,
    fetchArticles,
} from "../reducers/articleSlice";
import { useDispatch, useSelector } from "react-redux";
import { Progress } from "@chakra-ui/react";

const useStyle = createStyles((theme) => ({
    gridContainer: {},
    cardPageWrapper: {
        overflow: "hidden",
        marginTop: 100,
        "@media (max-width: 600px)": {
            overflow: "hidden",
        },
    },
    title: {
        marginTop: 24,
        marginBottom: 40,
        fontSize: 24,
        fontWeight: 700,
        color: "#4D4D4D",
    },
    button: {
        marginTop: 20,
        marginBottom: 20,
        padding: "20px 30px",
        fontSize: 16,
        fontWeight: 700,
        height: "auto",
    },
    sectionPromotion: {
        height: "100vh",
    },
}));

const ArticleContentPage = () => {
    const { classes } = useStyle();

    const articles = useSelector(articlesStore);
    const empty = useSelector(articlesIsEmpty);
    const isEnded = useSelector(articleIsEnded);
    const dispatch = useDispatch();

    useEffect(() => {
        Aos.init({
            duration: 300,
            easing: "ease-in-sine",
            offset: 200,
            delay: 100,
        });

        if (empty) {
            (async function fetch() {
                dispatch(await fetchArticles());
            })();
        }
    }, [empty, dispatch]);
    const ref = useRef(null);

    if (empty) {
        return (
            <Progress
                size="xs"
                isIndeterminate
                colorScheme="teal"
                position="fixed"
                top="0"
                left="0"
                right="0"
            />
        );
    } else {
        return (
            <motion.div
                className={classes.cardPageWrapper}
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ x: window.innerWidth, transition: { duration: 0.3 } }}
            >
                <div className="container">
                    <h1 className={classes.title}>Artikel</h1>
                </div>
                <div className={`container mb-5 `}>
                    <div className="row">
                        <div className="col col-lg-12">
                            <div className={`row justify-content-center`}>
                                {articles &&
                                    articles.map((item, key) => {
                                        return (
                                            <motion.div
                                                className="col-sm-12 col-md-6 col-lg-4 justify-content-center"
                                                key={key}
                                                initial={{ y: 50 }}
                                                whileInView={{ y: 0 }}
                                                whileHover={{ y: -20 }}
                                                viewport={{ root: ref }}
                                            >
                                                <CardArticleCompo
                                                    id={item.id}
                                                    image={item.cover_url}
                                                    title={item.title}
                                                    article={item.caption}
                                                    likes={item.likes}
                                                />
                                            </motion.div>
                                        );
                                    })}
                            </div>
                        </div>
                    </div>
                    {!isEnded && (
                        <Center>
                            <Button
                                color="teal.9"
                                radius="xl"
                                className={classes.button}
                                onClick={async () =>
                                    dispatch(
                                        await fetchArticles(
                                            articles[articles.length - 1]
                                                .created_at.seconds,
                                            articles[articles.length - 1].id
                                        )
                                    )
                                }
                            >
                                Lainnya
                            </Button>
                        </Center>
                    )}
                </div>
                <JumboTron2 />
                <FooterCompo />
            </motion.div>
        );
    }
};
export default ArticleContentPage;

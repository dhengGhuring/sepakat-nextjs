import bathroom from "./icons/sepakat icon/Tag-Facility-icon-bathroom.svg";
import carport from "./icons/sepakat icon/Tag-Facility-icon-carport.svg";
import bedroom from "./icons/sepakat icon/Tag-Facility-icon-bedroom.svg";
import building_area from "./icons/sepakat icon/Tag-Facility-icon-building_area.svg";
import certificate from "./icons/sepakat icon/Tag-Facility-icon-certificate.svg";
import family_room from "./icons/sepakat icon/Tag-Facility-icon-family_room.svg";
import electricity from "./icons/sepakat icon/Tag-Facility-icon-electricity.svg";
import land_area from "./icons/sepakat icon/Tag-Facility-icon-land_area.svg";
import garage from "./icons/sepakat icon/Tag-Facility-icon-garage.svg";
import garden from "./icons/sepakat icon/Tag-Facility-icon-garden.svg";
import kitchen from "./icons/sepakat icon/Tag-Facility-icon-kitchen.svg";
import living_room from "./icons/sepakat icon/Tag-Facility-icon-living_room.svg";
import dining_room from "./icons/sepakat icon/Tag-Facility-icon-dining_room.svg";
import water_sources from "./icons/sepakat icon/Tag-Facility-icon-water_sources.svg";

export {
  bathroom,
  carport,
  bedroom,
  building_area,
  certificate,
  family_room,
  electricity,
  land_area,
  garage,
  garden,
  kitchen,
  living_room,
  water_sources,
  dining_room,
};

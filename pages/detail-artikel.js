import { motion } from 'framer-motion';
import ReactPlayer from 'react-player';
import InfoCardCompo from '../Components/infoCard/infocard-component';
import { Image, createStyles, Button, Container, Center } from '@mantine/core';
import { Carousel } from '@mantine/carousel';
import JumboTron2 from '../Components/jumbotron-2/jumbotron2-component';
import ArticleMobileCompo from '../Components/content/articlemobile-component';
import { useEffect, useState } from 'react';
import VideoCompo from '../Components/content/video-component';
import CardArticleCompo from '../Components/content/cardarticle-component';
import { FooterCompo } from '../Components';
import TitleComponent from '../Components/admin/title-component';
import DescriptionAdminComponent from '../Components/admin/descriptionadmin-component';
import { useSelector } from 'react-redux';
import { videosStore } from '../reducers/videoSlice';
import { articlesStore } from '../reducers/articleSlice';
import { useRouter } from 'next/router';

const useStyle = createStyles((theme) => ({
  container: {
    marginTop: 100,
  },
  image: {
    '@media (max-width: 600px)': {
      marginTop: 30,
      width: 300,
    },
  },
  title: {
    fontFamily: 'Nunito Sans, sans-serif',
    fontSize: 48,
    color: '#000000',
    fontWeight: 700,
    marginTop: 40,
    marginBottom: 10,
    '@media (max-width: 600px)': {
      fontSize: 20,
    },
  },
  textDetail: {
    fontSize: 24,
    fontWeight: 700,
    color: '#1C7849',
    '@media (max-width: 600px)': {
      fontSize: 16,
    },
  },
  textDetail2: {
    marginLeft: 40,
    fontSize: 24,
    fontWeight: 400,
    '@media (max-width: 600px)': {
      fontSize: 16,
    },
  },
  textDetail3: {
    fontFamily: 'Nunito Sans, sans-serif',
    fontWeight: 700,
    fontSize: 24,
    color: '#4D4D4D',
    marginBottom: 40,
    '@media (max-width: 600px)': {
      display: 'none',
    },
  },
  textDetail4: {
    fontFamily: 'Nunito Sans, sans-serif',
    fontWeight: 700,
    fontSize: 24,
    color: '#4D4D4D',
    marginTop: 40,
    marginBottom: 40,
    '@media (max-width: 600px)': {
      fontFamily: 'Poppins, sans-serif',
      fontSize: 20,
      fontWeight: 700,
      color: '#F56600',
      marginBottom: 10,
    },
  },
  text: {
    '@media (max-width: 600px)': {
      fontSize: 12,
    },
  },
  tombol: {
    fontFamily: 'Nunito Sans, sans-serif',
    marginTop: 40,
    marginBottom: 48,
    borderRadius: 15,
    backgroundColor: '#F56600',
    padding: '12px 24px',
    width: '100%',
    maxWidth: 300,

    '&:hover': {
      transition: '0.5s',
      backgroundColor: '#F56600',
      boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px',
      color: 'white',
    },

    '@media (max-width: 600px)': {
      padding: '12px 24px 12px 24px',
    },
  },
  pembungkusCarouselVideo: {
    '@media (max-width: 600px)': {
      display: 'none',
    },
  },
  articleDekstop: {
    '@media (max-width: 600px)': {
      display: 'none',
    },
    '@media (min-width: 600px)': {
      display: 'none',
    },
    '@media (min-width: 768px)': {
      display: 'block',
    },
  },
  articleMobile: {
    '@media (min-width: 768px)': {
      display: 'none',
    },
  },
  sectionPromotion: {
    height: '100vh',
  },
  articleCarouselSlide: {
    width: 100,
  },
}));

const ArticleDetail = (props) => {
  const [detailarticle, setDetailArticle] = useState();
  const videos = useSelector(videosStore);
  const articles = useSelector(articlesStore);
  const router = useRouter();

  const [fetching, setFetching] = useState(false);
  const { classes } = useStyle();

  useEffect(() => {
    const id = router.query.id;

    setTimeout(() => {
      async function fetchDetailMedia(id) {
        if (!id || id === '') {
          return console.log('id is empty');
        }
        fetch(`https://api.sepakat.id/firebase/content/${id}`)
          .then((res) => {
            return res.json();
          })
          .then((res) => {
            const element = res.data;
            setDetailArticle(element);
          });
      }

      if (id) {
        (async function fetch() {
          await fetchDetailMedia(id);
          window.scrollTo(0, 0);
        })().then((value) => setFetching(true));
      }
    }, 1000);
  }, [router.query.id, fetching, router]);

  return (
    detailarticle && (
      <motion.div
        initial={{ width: 0 }}
        animate={{ width: '100%' }}
        exit={{ x: window.innerWidth, transition: { duration: 0.3 } }}
        key={detailarticle.id}
      >
        <div className={`container mb-5 ${classes.container}`}>
          <Image
            src={
              detailarticle.name?.includes('.mp4')
                ? detailarticle.thumbnail_url
                : detailarticle.cover_url
            }
            mt={30}
            height={627}
            radius={30}
            className={classes.image}
            alt=""
          />
          <TitleComponent
            key={detailarticle.id}
            titleArticle={detailarticle.title}
            adminWritter={`Admin ${detailarticle.creator?.name}`}
            date={Date(detailarticle.created_at).substring(3, 15)}
          />
          <div className="row mt-5">
            <div className="col-lg-9">
              <Container>
                {detailarticle.description.map((item, key) => {
                  if (item['type'] === 'text') {
                    return (
                      <DescriptionAdminComponent
                        key={key}
                        articles={item['content']}
                      />
                    );
                  } else if (item['type'] === 'image') {
                    return <Image src={item['content']} alt="" key={key} />;
                  } else if (item['type'] === 'video') {
                    return <ReactPlayer url={item['content']} key={key} />;
                  } else if (item['type'] === 'link') {
                    return (
                      <Center key={key}>
                        <Button
                          className={classes.tombol}
                          component="a"
                          href={item['content']}
                        >
                          {item.name}
                        </Button>
                      </Center>
                    );
                  } else {
                    return <></>;
                  }
                })}
              </Container>
            </div>
            <div className="col">
              <InfoCardCompo />
            </div>

            {videos?.length > 0 && (
              <>
                <h1 className={classes.textDetail3}>Video Lainnya</h1>
                <div className={classes.pembungkusCarouselVideo}>
                  <Carousel
                    withControls={true}
                    controlSize={70}
                    controlsOffset={5}
                    slideSize={350}
                    slideGap={30}
                    align="start"
                    styles={{
                      control: {
                        opacity: 0.5,
                        backgroundColor: '#F2EEC8',
                        color: '#F56600',
                        borderColor: '#F2EEC8',
                        '&[data-inactive]': {
                          opacity: 0,
                          cursor: 'default',
                        },
                      },
                    }}
                  >
                    {videos.map((item, key) => {
                      return (
                        <Carousel.Slide key={key}>
                          <div>
                            <VideoCompo
                              key={key}
                              myId={item.id}
                              video={item.cover_url}
                              title={item.title}
                              desc={item.caption}
                              likes={item.likes}
                              setFetching={setFetching}
                            />
                          </div>
                        </Carousel.Slide>
                      );
                    })}
                  </Carousel>
                </div>
              </>
            )}
            {articles?.length > 0 && (
              <>
                <h1 className={classes.textDetail4}>Artikel Lainnya</h1>
                <div className={classes.articleDekstop}>
                  <Carousel
                    withControls={true}
                    controlSize={40}
                    slideSize={350}
                    slideGap={30}
                    align="start"
                    styles={{
                      control: {
                        backgroundColor: '#F2EEC8',
                        color: '#F56600',
                        borderColor: '#F2EEC8',
                        opacity: 0.5,
                        '&[data-inactive]': {
                          opacity: 0,
                          cursor: 'default',
                        },
                      },
                    }}
                  >
                    {articles?.map((item, key) => {
                      return (
                        <Carousel.Slide
                          className={classes.articleCarouselSlide}
                          key={key}
                        >
                          <div>
                            <CardArticleCompo
                              key={key}
                              id={item.id}
                              image={item.cover_url}
                              title={item.title}
                              article={item.caption}
                              likes={item.likes}
                              setFetching={setFetching}
                            />
                          </div>
                        </Carousel.Slide>
                      );
                    })}
                  </Carousel>
                </div>
              </>
            )}
            <div className={classes.articleMobile}>
              {articles?.map((item, key) => {
                return (
                  <div
                    onClick={async (e) => {
                      setFetching(false);
                      await setTimeout(() => {}, 100);
                    }}
                    key={key}
                  >
                    <ArticleMobileCompo
                      myId={item.id}
                      key={key}
                      image={item.cover_url}
                      title={item.title}
                      article={item.caption}
                    />
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <JumboTron2 />
        <FooterCompo />
      </motion.div>
    )
  );
};
export default ArticleDetail;

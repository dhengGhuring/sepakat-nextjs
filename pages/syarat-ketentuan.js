import React from 'react';
import { Box, Center, Text, Heading } from '@chakra-ui/react';
import { FooterCompo2 } from '../Components';
import style from '../styles/SyaratKetentuanPageStyle.module.css';

export default function SyaratKetentuan() {
  return (
    <Box backgroundColor={'white'} minH={'100vh'}>
      <Box
        minH={{ base: '50vh', md: '70vh' }}
        my={'auto'}
        className={style.container}
        position={'relative'}
      >
        <Center>
          <Heading
            fontSize={{ base: '3xl', md: '4xl', lg: '6xl' }}
            w={'100%'}
            textAlign={'center'}
            fontWeight={'bold'}
            fontFamily={'Poppins, sans-serif'}
            color={'white'}
            position={'absolute'}
            top={{ base: '60%', md: '50%' }}
            left={'50%'}
            transform={{
              base: 'translate(-50%, -40%)',
              md: 'translate(-50%, -50%)',
            }}
          >
            Syarat & Ketentuan
          </Heading>
        </Center>
      </Box>
      <Box
        maxW={{ base: '85%', md: '70%' }}
        mx={'auto'}
        my={20}
        textAlign={'justify'}
        display={'flex'}
        flexDirection={'column'}
        gap={7}
      >
        <Text fontSize={{ base: 'sm', md: 'md' }}>
          Selamat datang di website Sepakat. Dengan mengakses dan menggunakan
          situs web ini, Anda setuju untuk mematuhi dan terikat oleh ketentuan
          penggunaan dan kebijakan privasi yang berlaku terkait dengan PT
          Sepakat Menjala Berkat sehubungan dengan situs web ini.
        </Text>
        <Box>
          <Heading
            fontSize={{ base: 'lg', md: '2xl' }}
            fontFamily={'Poppins, sans-serif'}
          >
            1. PENGGUNAAN WEBSITE
          </Heading>
          <Text fontSize={{ base: 'sm', md: 'md' }}>
            Dengan mengakses dan menggunakan website www.sepakat.co.id atau
            aplikasi SEPAKAT PROPERTI sebagai pengunjung dan pengguna terdaftar,
            Anda menyetujui Syarat dan Ketentuan Umum yang ditetapkan untuk
            layanan website ini. Ketentuan Umum ini dapat diubah dan/atau
            diperbarui dan/atau ditambah sewaktu-waktu tanpa pemberitahuan. Jika
            pengunjung, pengguna terdaftar, agen properti atau penjual/penyewa
            tidak menyetujui Syarat dan Ketentuan ini, www.sepakat.co.id akan
            menghentikan akses dan penggunaan situs web www.sepakat.co.id ini.
            www.sepakat.co.id adalah situs Properti Indonesia yang menghubungkan
            agen dan/atau penjual SEPAKAT PROPERTI dan pembeli atau penyewa
            dengan penyewa.
          </Text>
        </Box>
        <Box>
          <Heading
            fontSize={{ base: 'lg', md: '2xl' }}
            fontFamily={'Poppins, sans-serif'}
          >
            2. LAYANAN WEBSITE DAN APLIKASI SEPAKAT
          </Heading>
          <Text fontSize={{ base: 'sm', md: 'md' }}>
            Pengguna dan/atau Pengunjung Terdaftar menerima dan menyetujui bahwa
            www.sepakat.co.id memiliki hak eksklusif untuk mengubah dan/atau
            menghentikan penyediaan Situs Web dan Aplikasi beserta layanannya
            atau menghapus sementara atau permanen data Pengguna Terdaftar tanpa
            pemberitahuan dan tanpa persetujuan sebelumnya. Pengguna terdaftar
            sebelumnya di www.sepakat.co.id dan/ Aplikasi SEPAKAT PROPERTI tidak
            memiliki kewajiban kepada pengguna terdaftar untuk penanganan hak
            eksklusif tersebut. www.sepakat.co.id tidak bertanggung jawab atas
            penghapusan data, ketidakakuratan data dan/atau kesalahan dalam
            penyediaan data dan/atau informasi yang disampaikan kepada
            www.sepakat.co.id
          </Text>
        </Box>
        <Box>
          <Heading
            fontSize={{ base: 'lg', md: '2xl' }}
            fontFamily={'Poppins, sans-serif'}
          >
            3. REGISTRASI / PENDAFTARAN PENGGUNA
          </Heading>
          <Text fontSize={{ base: 'sm', md: 'md' }}>
            Untuk dapat menggunakan fungsionalitas penuh dari website
            www.sepakat.co.id, pengguna harus registrasi atau mendaftar sebagai
            pengguna terdaftar sebagai agen SEPAKAT PROPERTI dan wajib
            memberikan data dan/atau informasi dimana pengguna, pada saat
            pendaftaran, dianggap telah menyetujui dan menerima ketentuan umum
            yang berlaku pada situs www.sepakat.co.id
          </Text>
        </Box>
        <Box>
          <Heading
            fontSize={{ base: 'lg', md: '2xl' }}
            fontFamily={'Poppins, sans-serif'}
          >
            4. PENGISIAN DAN PUBLIKASI
          </Heading>
          <Text fontSize={{ base: 'sm', md: 'md' }}>
            Pengguna terdaftar yang telah memasukkan dan mengunggah data atau
            informasi terkait penjualan dan/atau persewaan properti di website
            www.sepakat.co.id dan/ Aplikasi SEPAKAT PROPERTI setuju bahwa
            www.sepakat.co.id berhak menampilkan dan menggunakan serta
            menyetujuinya, terus-menerus atau sementara mereproduksi,
            memodifikasi, mengubah, mengadaptasi, menerbitkan dan
            mendistribusikan semua atau sebagian dari data atau informasi yang
            berkaitan dengan properti tanpa kewajiban untuk membayar royalti hak
            cipta
          </Text>
        </Box>
        <Box>
          <Heading
            fontSize={{ base: 'lg', md: '2xl' }}
            fontFamily={'Poppins, sans-serif'}
          >
            5. KEWAJIBAN PENGGUNA TERDAFTAR
          </Heading>
          <Text fontSize={{ base: 'sm', md: 'md' }}>
            Meskipun, kami mengetahui dan memiliki akses ke Informasi Pribadi
            yang anda kirimkan dengan sukarela ke Situs kami, seperti, namun
            tidak terbatas pada, kata sandi, alamat email, dan nomor telepon,
            kami berharap anda bertanggung jawab secara pribadi untuk menjaga
            kerahasiaan informasi ini, dan untuk bertanggung jawab atas semua
            informasi yang dipublikasikan, dan aktivitas yang dilakukan dengan
            menggunakan akun anda dengan kata sandi anda. Kami tidak dapat
            dimintakan pertanggungjawaban atas konten yang dikirimkan oleh anda
            atau orang lain yang bertindak atas nama anda ke Situs kami. Namun,
            kami berhak untuk memantau dan memeriksa informasi yang anda
            kirimkan ke Situs. Kami memiliki hak untuk menyensor, menghapus,
            atau melarang pengiriman informasi yang dianggap perlu untuk tujuan
            melindungi properti dan hak-hak Pinhome. Anda akan secara pribadi
            bertanggung jawab atas gugatan yang terkait dengan pencemaran nama
            baik, pelanggaran kekayaan intelektual, privasi, atau
            gugatan-gugatan lain yang timbul dari konten anda. Atas hal-hal
            tersebut, SEPAKAT PROPERTI berhak, atas kebijakan tunggalnya, untuk
            memberikan upaya-upaya perbaikan yang dianggap perlu. Pilihan
            upaya-upaya perbaikan tersebut termasuk penolakan Layanan,
            pengakhiran keanggotaan, penghapusan atau pengubahan konten,
            pembatalan pemesanan, atau menolak akses ke Situs dan ke semua
            Layanan kami, di dalam atau di luar Situs.
          </Text>
        </Box>
        <Box>
          <Heading
            fontSize={{ base: 'lg', md: '2xl' }}
            fontFamily={'Poppins, sans-serif'}
          >
            6. PENGHAPUSAN TAUTAN DARI WEBSITE KAMI
          </Heading>
          <Text fontSize={{ base: 'sm', md: 'md' }}>
            Jika Anda menemukan tautan apa pun di Situs Web kami yang
            menyinggung karena alasan apa pun, Anda bebas untuk menghubungi dan
            memberi tahu kami kapan saja. Kami akan mempertimbangkan permintaan
            untuk menghapus tautan tetapi kami tidak berkewajiban untuk atau
            lebih atau untuk menanggapi Anda secara langsung.
          </Text>
        </Box>
      </Box>
      <FooterCompo2 />
    </Box>
  );
}

import style from '../styles/App.module.css';
import { Section } from 'react-fully-scrolled';
import { useState, Suspense } from 'react';
import React, { useEffect } from 'react';
import Aos from 'aos';
import 'aos/dist/aos.css';
import { motion } from 'framer-motion';
import { useSelector, useDispatch } from 'react-redux';
import Image from 'next/image';
import Link from 'next/link';
import {
  fetchVideos,
  videosIsEmpty,
  videosStore,
} from '../reducers/videoSlice';
import {
  fetchRecommendationProperty,
  recommendationPropertiesStore,
  recommendationPropertyIsEmpty,
} from '../reducers/recommendationPropertySlice';
import {
  articlesIsEmpty,
  articlesStore,
  fetchArticles,
} from '../reducers/articleSlice';

//** Import Mantine
import {
  Container,
  createStyles,
  Center,
  Button,
  Title,
  Loader,
} from '@mantine/core';
import { Carousel } from '@mantine/carousel';

// Import Components
import JumboTron2 from '../Components/jumbotron-2/jumbotron2-component';
import CarouselCompo2 from '../Components/carousel2/carousel2-component';
import VideoCompo from '../Components/content/video-component';
import { FooterCompo } from '../Components';
import PartnersCompo from '../Components/partners/partners-component';
import AgenFeature from '../Components/carouselAgen/agenfeature-component';
import HomePageJumbotron from '../Components/homepageJumbotron/homepage-jumbotron.js';
import PromotionCompo from '../Components/promotion/promotion-component.js';

const useStyle = createStyles((theme) => ({
  rowMobile: {
    height: 600,
    '@media (min-width: 992px)': {
      height: 700,
    },
  },
  video: {
    position: 'absolute',
    zIndex: '-1',
  },
  containerCarousel2: {
    paddingLeft: 0,
    paddingRight: 0,
    maxWidth: '100%',
    marginLeft: 0,
    marginRight: 0,
    '@media (max-width: 600px)': {
      paddingLeft: 0,
    },
  },
  pembatas: {
    width: '100%',
    height: 400,
    backgroundColor: 'transparent',
    '@media (max-width: 600px)': {
      height: 0,
    },
    '@media (min-width: 600px)': {
      height: 20,
    },
    '@media (min-width: 768px)': {
      height: 140,
    },
    '@media (min-width: 992px)': {
      height: 300,
    },
    '@media (min-width: 1200px)': {
      height: 150,
    },
    '@media (min-width: 1400px)': {
      height: 0,
    },
    '@media (min-width: 1600px)': {
      height: 0,
    },
  },
  container: {
    width: '100%',
    '@media (max-width: 600px)': {
      marginLeft: 30,
      overflow: 'visible',
    },
    '@media (min-width: 600px)': {
      marginLeft: 50,
    },
    '@media (min-width: 768px)': {
      marginLeft: 100,
    },
    '@media (min-width: 1200px)': {
      marginLeft: '260px!important',
    },
    '@media (min-width: 992px)': {
      marginLeft: 150,
    },
  },
  control: {
    backgroundColor: '#F2EEC8',
    color: '#F56600',
    borderColor: '#F2EEC8',
    opacity: 0.5,
  },
  containerVideo: {
    '@media (max-width: 600px)': {
      display: 'none',
    },
    '@media (min-width: 600px)': {
      display: 'none',
    },
    '@media (min-width: 768px)': {
      display: 'none',
    },
    '@media (min-width: 992px)': {
      display: 'block',
    },
  },
  rowVideo: {
    gap: '4rem',
    '@media (max-height: 500px)': {
      gap: '1rem',
    },
    '@media (min-height: 500px)': {
      gap: '1rem',
    },
  },
  buttonSelengkapnyaVideo: {
    '@media (min-width: 1200px) and (max-height: 500px)': {},
    '@media (min-width: 1200px) and (min-height: 500px)': {
      marginTop: -25,
    },
    '@media (min-width: 1200px) and (min-height: 600px)': {
      marginTop: -25,
    },
    '@media (min-width: 1200px) and (min-height: 700px)': {
      marginTop: -10,
    },
    '@media (min-width: 1400px) and (max-height: 500px)': {},
    '@media (min-width: 1600px) and (max-height: 500px)': {},
  },
  slide: {},
  containerSlide: {
    '@media (max-width: 600px)': {
      width: '240px!important',
      paddingLeft: '350px!important',
      gap: '30px',
    },
  },
  head1: {
    '@media (min-width: 1200px)': {
      paddingTop: '10%',
    },
    '@media (max-height: 500px)': {
      paddingTop: '2%!important',
    },
    '@media (min-height: 500px) and (max-height: 600px)': {
      paddingTop: '5%',
      transition: '0.5s',
    },
    '@media (min-height: 600px) and (max-height: 800px)': {
      paddingTop: '5%!important',
    },
  },
  head1H1: {
    fontFamily: 'Poppins, sans-serif',
    color: '#1c7849',
    fontSize: 80,
    fontWeight: 700,
    '@media (max-height: 500px)': {
      fontSize: '50px!important',
    },
    '@media (min-height: 500px) and (max-height: 600px)': {
      fontSize: '50px!important',
    },
    '@media (min-height: 600px) and (max-height: 750px)': {
      fontSize: '60px!important',
    },
    '@media (min-height: 750px) and (max-height: 800px)': {
      fontSize: '80px!important',
    },
  },
  titlePartner: {
    fontFamily: 'Poppins, sans-serif',
    color: '#F56600',
    fontSize: 48,
    fontWeight: 700,
    marginTop: '5%',
    marginBottom: 10,
    textAlign: 'center',
    '@media (max-width: 600px)': {
      fontSize: 30,
      marginTop: 32,
    },
    '@media (max-width: 600px) and (max-height: 500px)': {
      fontSize: 35,
    },
    '@media (min-width: 600px)': {
      fontSize: 40,
    },
    '@media (min-height: 800px)': {
      marginTop: 15,
    },
    '@media (min-width: 1200px) and (max-height: 500px)': {
      marginTop: '10%',
    },
    '@media (min-width: 1200px) and (min-height: 500px)': {
      marginTop: 0,
      paddingTop: 65,
      marginBottom: 30,
    },
    '@media (min-width: 1200px) and (min-height: 700px)': {
      marginTop: 0,
      paddingTop: 100,
    },
    '@media (min-width: 1600px) and (min-height: 900px)': {
      paddingTop: 65,
    },
    '@media (min-width: 1800px) and (min-height: 900px)': {
      paddingTop: 115,
      marginTop: 0,
    },
  },
  wrapperFooter: {},
  containerFooter: {
    marginTop: 0,
  },
  containerButton: {},
  articleButton: {
    padding: 20,
    fontFamily: 'Nunito, sans-serif !important',
    fontSize: '15px !important',
    fontWeight: '700 !important',
    maxWidth: '200px',
    maxHeight: '70px',
    boxShadow:
      'rgba(50, 50, 93, 0.25) 0px 13px 27px -5px, rgba(0, 0, 0, 0.3) 0px 8px 16px -8px;',
    '@media (max-width: 600px) and (max-height: 500px)': {
      padding: '10px !important',
    },
    '@media (max-width: 600px) and (min-height: 500px)': {
      padding: '12px !important',
    },
    '@media (min-width: 600px) and (max-height: 500px)': {
      padding: 12,
      fontSize: 15,
    },
    '@media (min-width: 600px) and (min-height: 500px)': {
      padding: 12,
      fontSize: 15,
    },
    '@media (min-width: 600px) and (min-height: 600px)': {
      padding: 12,
      fontSize: 15,
    },
    '@media (min-width: 600px) and (min-height: 700px)': {
      padding: 12,
      fontSize: 15,
    },
  },
  containerArticle: {
    '@media (max-width: 600px)': {
      height: 510,
    },
  },
}));

const HomePage = () => {
  const { classes } = useStyle();
  const [fetching, setFetching] = useState(false);
  const [videoMobile, setVideoMobile] = useState([]);

  const videos = useSelector(videosStore);
  const articles = useSelector(articlesStore);
  const recommendationProperties = useSelector(recommendationPropertiesStore);

  const recommendationPropertyEmpty = useSelector(
    recommendationPropertyIsEmpty
  );
  const VideosEmpty = useSelector(videosIsEmpty);
  const articlesEmpty = useSelector(articlesIsEmpty);

  const dispatch = useDispatch();

  function detectMob() {
    return window.innerWidth <= 800;
  }

  useEffect(() => {
    Aos.init({
      duration: 300,
      easing: 'ease-in-sine',
      offset: 200,
      delay: 100,
    });

    if (!fetching) {
      (async function fetch() {
        if (recommendationPropertyEmpty)
          dispatch(await fetchRecommendationProperty());
        if (articlesEmpty) dispatch(await fetchArticles());
        if (VideosEmpty) dispatch(await fetchVideos());
      })().then((value) => setFetching(true));
    }
  }, [
    fetching,
    dispatch,
    recommendationPropertyEmpty,
    VideosEmpty,
    articlesEmpty,
  ]);

  useEffect(() => {
    fetch(`https://api.sepakat.id/firebase/content-video?page=1`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((res) => {
      res.json().then((data) => {
        setVideoMobile(data.data);
      });
    });
  }, []);

  const scrollRef = 'oke';

  return (
    <>
      {/* /---------------------------------------------/

      DEKSTOP VIEW FOCUS

    /---------------------------------------------/ */}
      <div className={`${style.dekstopView} bg-white`}>
        <Suspense fallback={<div>Loading...</div>}>
          <HomePageJumbotron />
          <div className={classes.pembatas}></div>
        </Suspense>
        <div className="container-fluid" style={{ height: '100vh' }}>
          <div className={`row ${classes.rowDekstop}`}>
            <div className={`col-12 ${style.pertama}`}>
              <div className={`head-1 ${classes.head1}`}>
                <h1
                  className={`${classes.head1H1} ${classes.container}`}
                  data-aos="fade-right"
                  data-aos-duration="400"
                >
                  Rekomendasi Rumah
                  <br />
                  Idaman Buat Kamu
                </h1>
              </div>
            </div>
            <div className={`col-12 ${style.rekomendasiRumah} mb-5`}>
              <Container className={classes.containerCarousel2}>
                <Carousel
                  controlsOffset="10%"
                  controlSize={45}
                  slideSize={350}
                  slideGap={'xl'}
                  dragFree
                  align="start"
                  classNames={{
                    container: classes.container,
                    control: classes.control,
                  }}
                  styles={{
                    control: {
                      '&[data-inactive]': {
                        opacity: 0.5,
                        cursor: 'default',
                      },
                    },
                  }}
                >
                  {recommendationProperties?.map((item, key) => {
                    return (
                      <Link href={`/properti?id=${item.id}`} key={key}>
                        <CarouselCompo2
                          image={item.images[0]}
                          title={item.name}
                          address={`${item.address.street}, ${
                            item.address.lower_city[0].toUpperCase() +
                            item.address.lower_city.substring(1)
                          }`}
                        />
                      </Link>
                    );
                  })}
                </Carousel>
              </Container>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-12">
            <div className={style.promoCarousel}>
              <Suspense fallback={<Loader />}>
                <PromotionCompo />
              </Suspense>
            </div>
          </div>
        </div>
        <div className="videoSection" style={{ height: '100vh' }}>
          <div className={style.head3}>
            <h1
              className={`text-center ${style.tajuk}`}
              data-aos="fade-right"
              data-aos-duration="400"
            >
              Informasi Seputar
              <br />
              Properti Buat Kamu
            </h1>
          </div>
          <div className={style.head3Vidio}>
            <div className={`container ${classes.containerVideo}`}>
              <div className="row">
                <div className="col col-lg-12">
                  <Suspense fallback={<div>Loading...</div>}>
                    <div
                      className={`row justify-content-center ${classes.rowVideo}`}
                    >
                      {videos?.slice(0, 4).map((item, key) => {
                        return (
                          !detectMob() &&
                          item && (
                            <VideoCompo
                              key={key}
                              myId={item.id}
                              video={item.cover_url}
                              title={item.title}
                              desc={item.caption}
                              likes={item.likes}
                              item={item}
                            />
                          )
                        );
                      })}
                    </div>
                  </Suspense>
                </div>
              </div>
              <Center className={classes.buttonSelengkapnyaVideo}>
                <Link href="/konten-video">
                  <Button
                    size="md"
                    radius={50}
                    color="teal.9"
                    className="text-center"
                    mt={40}
                    mb={40}
                    data-aos="fade-up"
                    data-aos-duration="200"
                  >
                    Tampilkan lebih banyak
                  </Button>
                </Link>
              </Center>
            </div>
          </div>
        </div>
        <div>
          <div className="col-12">
            <div className={style.head2}>
              <h5
                className={style.title}
                data-aos="fade-up"
                data-aos-duration="300"
              >
                Jadikan dirimu sebagai agen
                <br />
                profesional bersama kami
              </h5>
              <AgenFeature />
            </div>
          </div>
        </div>
        <JumboTron2 />
        <Title
          className={classes.titlePartner}
          data-aos="zoom-in"
          data-aos-duration="200"
        >
          Kerjasama dan Kemitraan
        </Title>
        <div className={style.dekstopPartners}>
          <PartnersCompo />
        </div>
        <div className={style.footerContainer}>
          <FooterCompo />
        </div>
      </div>
      {/* 
      /---------------------------------------------/

        MOBILE VIEW FOCUS
      
      /---------------------------------------------/
       */}
      <div className={`${style.mobileView} bg-white`}>
        <Section className={style.pagesscroll1}>
          <HomePageJumbotron />

          <div className={classes.pembatas}></div>
        </Section>

        <Section className={style.pagesscroll2}>
          <div className="container-fluid">
            <div className={`row ${classes.rowMobile}`}>
              <div className={`col-12 ${style.pertama}`}>
                <div className={style.head1}>
                  <motion.h1
                    initial={{ x: -100 }}
                    whileInView={{ x: 0 }}
                    viewport={{ root: scrollRef }}
                    transition={{ duration: '0' }}
                  >
                    Rekomendasi Rumah
                    <br />
                    Idaman Buat Kamu
                  </motion.h1>
                </div>
              </div>
              <div className={`col-12 ${style.rekomendasiRumah} mb-5`}>
                <Container className={classes.containerCarousel2}>
                  <Carousel
                    controlsOffset="10%"
                    controlSize={30}
                    slideSize={350}
                    slideGap={30}
                    dragFree
                    align="start"
                    classNames={{
                      container: classes.container,
                      control: classes.control,
                    }}
                    styles={{
                      control: {
                        opacity: 0.1,
                        '&[data-inactive]': {
                          opacity: 0,
                          cursor: 'default',
                        },
                      },
                    }}
                  >
                    {recommendationProperties?.map((item, key) => {
                      return (
                        <Link href={`/properti?id=${item.id}`} key={key}>
                          <CarouselCompo2
                            image={item.images[0]}
                            title={item.name}
                            address={`${item.address.street}, ${
                              item.address.lower_city[0].toUpperCase() +
                              item.address.lower_city.substring(1)
                            }`}
                          />
                        </Link>
                      );
                    })}
                  </Carousel>
                </Container>
              </div>
            </div>
          </div>
        </Section>
        <Section className={style.pagesscroll3}>
          <div className="row">
            <div className="col-12">
              <div className={style.promoCarousel}>
                <PromotionCompo />
              </div>
            </div>
          </div>
        </Section>
        <Section className={style.pagesscroll4}>
          <div className={style.head3}>
            <h1
              className={`text-center ${style.tajuk}`}
              data-aos="fade-right"
              data-aos-duration="400"
            >
              Informasi Seputar <br />
              Properti Buat Kamu
            </h1>
          </div>
          <div className={style.head3Article}>
            <div className={`container-fluid ${classes.containerArticle}`}>
              <Carousel
                withControls={true}
                controlSize={40}
                slideSize={350}
                slideGap={'xl'}
                align="center"
                classNames={{
                  root: classes.root,
                  slide: classes.slide,
                  container: classes.containerSlide,
                  control: classes.control,
                }}
                sx={{
                  '@media (max-width: 600px)': {
                    overflow: 'visible!important',
                  },
                  '@media (min-width: 600px)': {
                    paddingLeft: 0,
                    transform: 'translate3d(0px, 0px, 0px)!important',
                  },
                }}
                styles={{
                  control: {
                    opacity: '0.2',
                    '&[data-inactive]': {
                      opacity: 0,
                      cursor: 'default',
                    },
                  },
                }}
              >
                {videos?.slice(0, 4).map((item, key) => {
                  return (
                    <Carousel.Slide
                      key={key}
                      sx={{
                        '@media (max-width: 600px)': {
                          paddingRight: 0,
                        },
                        '@media (min-width: 600px)': {
                          marginRight: 30,
                        },
                      }}
                    >
                      <VideoCompo
                        key={key}
                        myId={item.id}
                        video={item.cover_url}
                        title={item.title}
                        desc={item.caption}
                        likes={item.likes}
                        item={item}
                      />
                    </Carousel.Slide>
                  );
                })}
              </Carousel>
              <Link href="/konten-video">
                <button
                  className={`btn btn-success rounded-5 mt-3 ms-2 ${classes.articleButton}`}
                >
                  Video Lainnya
                </button>
              </Link>
            </div>
          </div>
        </Section>
        <Section className={style.pagesscroll5}>
          <div>
            <div className="col-12">
              <div className={style.head2}>
                <h5 className={style.title}>
                  Jadikan dirimu sebagai agen
                  <br />
                  profesional bersama kami
                </h5>

                <AgenFeature />
              </div>
            </div>
          </div>
        </Section>
        <Section className={style.pagesscroll6}>
          <JumboTron2 />
        </Section>
        <Section className={style.pagesscroll7}>
          <Title
            className={classes.titlePartner}
            data-aos="zoom-in"
            data-aos-duration="200"
          >
            Kerjasama dan Kemitraan
          </Title>
          <div className={style.dekstopPartners}>
            <PartnersCompo />
          </div>
          <div className={style.footerDekstop}>
            <div className={style.footerWrapper}>
              <div className={style.footerContainer}>
                <FooterCompo />
              </div>
            </div>
          </div>
        </Section>
        <div className={style.footerMobile}>
          <FooterCompo />
        </div>
      </div>
    </>
  );
};

export default HomePage;

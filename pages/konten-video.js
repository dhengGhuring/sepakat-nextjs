import { motion } from 'framer-motion';
import Aos from 'aos';
import 'aos/dist/aos.css';
import JumboTron2 from '../Components/jumbotron-2/jumbotron2-component';
import { createStyles, Button, Center } from '@mantine/core';
import VideoCompo from '../Components/content/video-component-konten';
import { useEffect } from 'react';
import { FooterCompo } from '../Components';
import { Progress } from '@chakra-ui/react';
// import LoadingAnimation from "../Components/loading animation/loadingAnimation";

import { useDispatch, useSelector } from 'react-redux';
import {
  fetchVideos,
  videoIsEnded,
  videosIsEmpty,
  videosStore,
} from '../reducers/videoSlice';
const useStyle = createStyles((theme) => ({
  root: {
    overflow: 'hidden',
    marginTop: 100,
  },
  judul: {
    marginTop: 20,
    marginBottom: 40,
    fontSize: 30,
    fontWeight: 700,
    color: '#4D4D4D',
    fontFamily: 'Nunito-sans, sans-serif',
  },
  containerVideo: {
    overflow: 'hidden',
  },
  rowVideo: {
    gap: '4rem',
  },
  button: {
    marginTop: 20,
    marginBottom: 20,
    padding: '20px 30px',
    fontSize: 16,
    fontWeight: 700,
    height: 'auto',
  },
  sectionPromotion: {
    height: '100vh',
  },
}));
const VideoContentPage = () => {
  const { classes } = useStyle();

  const videos = useSelector(videosStore);
  const empty = useSelector(videosIsEmpty);
  const isEnded = useSelector(videoIsEnded);
  const dispatch = useDispatch();
  useEffect(() => {
    Aos.init({
      duration: 300,
      easing: 'ease-in-sine',
      offset: 200,
      delay: 100,
    });

    if (empty) {
      (async function fetch() {
        dispatch(await fetchVideos());
      })();
    }
  }, [empty, dispatch]);

  if (empty) {
    return (
      // <LoadingAnimation />
      <Progress size="xs" isIndeterminate />
    );
  } else {
    return (
      <motion.div
        className={classes.root}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ x: window.innerWidth, transition: { duration: 0.3 } }}
      >
        <div className="container">
          <h1 className={classes.judul}>Video</h1>
        </div>
        <div className={`container mb-5`}>
          <div className="row">
            <div className="col col-lg-12">
              <div className={`row justify-content-center ${classes.rowVideo}`}>
                {videos.map((item, key) => {
                  return (
                    <VideoCompo
                      key={key}
                      myId={item.id}
                      video={item.cover_url}
                      title={item.title}
                      desc={item.caption}
                      likes={item.likes}
                      poster={item.thumbnail_url}
                    />
                  );
                })}
              </div>
            </div>
          </div>
          {!isEnded && (
            <Center>
              <Button
                color="teal.9"
                radius="xl"
                className={classes.button}
                onClick={async () =>
                  dispatch(
                    await fetchVideos(
                      videos[videos.length - 1].thumbnail_name,
                      videos[videos.length - 1].created_at.seconds,
                      videos[videos.length - 1].id
                    )
                  )
                }
              >
                Lainnya
              </Button>
            </Center>
          )}
        </div>
        <JumboTron2 />
        <FooterCompo />
      </motion.div>
    );
  }
};
export default VideoContentPage;

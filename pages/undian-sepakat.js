import React, { useState } from 'react';
import { FooterCompo } from '../Components/';
import { createStyles } from '@mantine/core';
import ImageBrosur from './assets/images/imgae-brosur.png';
import ModalFailed from '../Components/modalCupon/ModalFailed';
import ModalSuccess from '../Components/modalCupon/ModalSuccess';
import Swal from 'sweetalert2';
import { useForm } from 'react-hook-form';
import Image from 'next/image';
import style from '../styles/App.module.css';

export default function Cupon() {
  const useStyle = createStyles((theme) => ({
    containerCupon: {
      marginTop: '4.5rem',
      minHeight: '95vh',
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      alignItems: 'center',
      gap: '2rem',
      padding: '0 2rem',
      '@media (max-width: 768px)': {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      },
    },
    cupon: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      color: 'white',
      gap: '2rem',
      padding: '2rem 0',
      '& h1': {
        textAlign: 'center',
        fontSize: '3rem',
        fontWeight: 'bold',
        marginBottom: '1rem',
      },
      '& h2': {
        fontSize: '1.2rem',
        fontWeight: 'bold',
        marginBottom: '1rem',
      },
      '@media (max-width: 768px)': {
        '& h1': {
          fontSize: '2rem',
          marginBottom: '0rem',
        },
        '& h2': {
          fontSize: '1rem',
          marginBottom: '1rem',
        },
      },
    },
    inputContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: '1rem',
      '@media (max-width: 768px)': {
        marginBottom: '0',
      },
    },
    buttonSend: {
      width: '50%',
      height: '3rem',
      borderRadius: '30px',
      border: 'none',
      padding: '0 1rem',
      fontSize: '1rem',
      backgroundColor: '#1C7849',
      color: 'white',
      fontWeight: 'bold',
      cursor: 'pointer',
      '@media (max-width: 1200px)': {
        width: '40%',
      },
      '@media (max-width: 991px)': {
        width: '50%',
      },
      '@media (max-width: 768px)': {
        width: '70%',
      },
      '@media (max-width: 480px)': {
        width: '50%',
      },
    },
    buttonSendDisible: {
      width: '50%',
      height: '3rem',
      borderRadius: '30px',
      border: 'none',
      padding: '0 1rem',
      fontSize: '1rem',
      backgroundColor: '#eaeaea',
      color: 'gray',
      fontWeight: 'bold',
      cursor: 'not-allowed',
      '@media (max-width: 1200px)': {
        width: '40%',
      },
      '@media (max-width: 991px)': {
        width: '50%',
      },
      '@media (max-width: 768px)': {
        width: '70%',
      },
      '@media (max-width: 480px)': {
        width: '50%',
      },
    },
    inputField: {
      width: '100%',
      height: '3rem',
      borderRadius: '5px',
      border: 'none',
      padding: '0 1rem',
      fontSize: '1rem',
      color: 'black',
      '@media (max-width: 1200px)': {
        width: '90%',
      },
      '@media (max-width: 991px)': {
        width: '100%',
      },
      '@media (max-width: 480px)': {
        width: '80%',
        height: '2rem',
      },
    },
    brosur: {
      width: '80%',
      alignSelf: 'center',

      '@media (max-width: 768px)': {
        width: '80%',
      },
      '@media (max-width: 480px)': {
        width: '100%',
      },
    },
  }));
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const { classes } = useStyle();
  const [respons, setRespons] = useState({
    name: '',
    pembelian: '',
    number: '',
    phone: '',
  });
  const [modalSuccess, setModalSuccess] = useState(false);
  const [modalFailed, setModalFailed] = useState(false);
  const [isDisible, setIsDisible] = useState(false);
  const submitHandler = async (value) => {
    setIsDisible(true);
    if (
      value.phone[0] === '+' &&
      value.phone[1] === '6' &&
      value.phone[2] === '2'
    ) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Nomor Hp tidak boleh menggunakan kode negara, gunakan 08xxxx',
      });
    } else {
      fetch(`https://api.sepakat.id/coupon/verify`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: new URLSearchParams({
          number: value.number,
          phone: value.phone,
        }),
      })
        .then((response) => response.json())
        .then((data) => {
          if (data.status === 200) {
            setModalSuccess(true);
            setRespons({
              name: data.data.nama,
              pembelian: data.data.pembelian,
              number: value.number,
              phone: value.phone,
            });
            setIsDisible(false);
          } else {
            setModalFailed(true);
            setIsDisible(false);
          }
        })
        .catch((error) => {});
    }
  };

  return (
    <>
      <ModalSuccess
        open={modalSuccess}
        setModalSuccess={setModalSuccess}
        number={respons.number}
        name={respons.name}
        pembelian={respons.pembelian}
        phone={respons.phone}
      />
      <ModalFailed open={modalFailed} setModalFailed={setModalFailed} />
      <div className={`${classes.containerCupon} ${style.containerCupon}`}>
        <div className={classes.cupon}>
          <Image src={ImageBrosur} className={classes.brosur} alt="brosur" />
        </div>
        <div className={classes.cupon}>
          <h1>Kupon Undian Sepakat</h1>
          <div className={classes.inputContainer}>
            <h2>Masukkan Kode Kupon Anda</h2>
            <input
              type="text"
              className={classes.inputField}
              {...register('number', {
                required: true,
              })}
              placeholder="xxxx"
            />
            {errors.number?.type === 'required' && (
              <p style={{ color: 'red', textAlign: 'left' }}>
                Kode Kupon tidak boleh kosong
              </p>
            )}
          </div>
          <div className={classes.inputContainer}>
            <h2>Masukkan No Hp Anda</h2>
            <input
              type="text"
              className={classes.inputField}
              {...register('phone', {
                required: true,
                pattern: /^[0-9]*$/,
              })}
              placeholder="08xxxxxxxxxx"
            />
            {errors.phone?.type === 'required' && (
              <p style={{ color: 'red', textAlign: 'left' }}>
                Nomor Hp tidak boleh kosong
              </p>
            )}
            {errors.phone?.type === 'pattern' && (
              <p style={{ color: 'red', textAlign: 'left' }}>
                Nomor Hp tidak valid, megandung karakter selain angka
              </p>
            )}
          </div>
          <div className={classes.inputContainer}>
            <button
              className={
                isDisible ? classes.buttonSendDisible : classes.buttonSend
              }
              onClick={handleSubmit(async (value) => {
                await submitHandler(value);
              })}
              disabled={isDisible}
            >
              Cek Kupon
            </button>
          </div>
        </div>
      </div>
      <FooterCompo />
    </>
  );
}

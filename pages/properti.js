import { motion } from 'framer-motion';
import { useState, useEffect, useCallback } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Carousel } from '@mantine/carousel';
import CardDekstopCompo from '../Components/cardDekstop/carddekstop-component';
import CardMobileCompo from '../Components/cardMobile/cardmobile-component';
import CarouselCompo from '../Components/carousel/carousel-component';
import CarouselCompo2 from '../Components/carousel2/carousel2-component';
import InfoCardCompo from '../Components/infoCard/infocard-component';
import JumboTron2 from '../Components/jumbotron-2/jumbotron2-component';
import style from '../styles/HousingPageStyle.module.css';
import lokasiPerumStyle from '../Components/lokasiPerumahan/lokasiPerumCompoStyle.module.css';
import LokasiPerumCompo from '../Components/lokasiPerumahan/lokasiperumahan-component';
import { createStyles } from '@mantine/core';
import { FooterCompo } from '../Components';
import DescriptionHousingComponent from '../Components/descriptionHouse/descriptionhousing-component';
import DescHouseCompo from '../Components/descriptionHouse/descriptionhouse-component';
import capitalizeWord from '../utility/capitalizeWord';
import ButtonCompo from '../Components/button/button-component';
import '../styles/siteplan.module.css';
import SitePlan from '../Components/siteplan/SitePlan';
import Image from 'next/image';

const useStyle = createStyles((theme) => ({
  root: {
    '@media (max-width: 600px)': {
      paddingLeft: 30,
      gap: 10,
    },
  },
  control: {
    backgroundColor: '#F2EEC8',
    color: '#F56600',
    borderColor: '#F2EEC8',
    opacity: 1,
  },
  sectionPromotion: {
    height: '100vh',
  },
}));

const HousingPage = (props) => {
  const router = useRouter();
  const [property, setProperty] = useState(null);
  const [otherproperties, setOtherproperties] = useState();
  // const [fetching, setFetching] = useState(false);
  // const [propertyId, setPropertyId] = useState(null);

  const { classes } = useStyle();

  // async function fetchDetailProperty(id) {
  //   return await fetch(`https://api.sepakat.id/firebase/property/${id}`)
  //     .then(async (res) => {
  //       return await res.json();
  //     })
  //     .then((res) => {
  //       const element = res.data;
  //       setProperty(element);
  //       localStorage.setItem("brochure", element.brochure);
  //       return element;
  //     });
  // }
  // async function fetchotherProperties(city, transaction, categories, name) {
  //   await fetch(
  //     `https://api.sepakat.id/firebase/properties?city=${city}&transaction=${transaction}&categories=${categories}&name=${name}`
  //   )
  //     .then(async (res) => {
  //       return await res.json();
  //     })
  //     .then((res) => {
  //       const element = [];
  //       for (let i = 0; i < res.data.length; i++) {
  //         element.push(res.data[i]);
  //       }
  //       setOtherproperties(element);
  //     });
  // }

  // const fetchAPI = useCallback(
  //   async (propertyId) => {
  //     if (property == null && propertyId) {
  //       try {
  //         await fetchDetailProperty(propertyId).then(async (value) => {
  //           await fetchotherProperties(
  //             value?.address.lower_city,
  //             value?.transaction,
  //             value?.categories.map((e) => e.name),
  //             value?.name
  //           );
  //         });
  //       } catch (error) {
  //         console.log("error: " + error);
  //       }
  //     }
  //   },
  //   [property, fetchotherProperties, fetchDetailProperty]
  // );

  // useEffect(() => {
  //   const id = router.query.id;
  //   setPropertyId(id);

  //   if (!fetching) {
  //     fetchAPI(id).then((value) => setFetching(true));
  //   }
  // }, [fetchAPI, fetching, propertyId, router.query.id]);

  useEffect(() => {
    const id = router.query.id;

    setTimeout(() => {
      async function fetchDetailProperty(id) {
        return await fetch(`https://api.sepakat.id/firebase/property/${id}`)
          .then(async (res) => {
            return await res.json();
          })
          .then((res) => {
            const element = res.data;
            setProperty(element);
            localStorage.setItem('brochure', element.brochure);
            return element;
          });
      }

      async function fetchotherProperties(city, transaction, categories, name) {
        await fetch(
          `https://api.sepakat.id/firebase/properties?city=${city}&transaction=${transaction}&categories=${categories}&name=${name}`
        )
          .then(async (res) => {
            return await res.json();
          })
          .then((res) => {
            const element = [];
            for (let i = 0; i < res.data.length; i++) {
              element.push(res.data[i]);
            }
            setOtherproperties(element);
          });
      }

      if (id) {
        fetchDetailProperty(id).then(async (value) => {
          await fetchotherProperties(
            value?.address.lower_city,
            value?.transaction,
            value?.categories.map((e) => e.name),
            value?.name
          );
        });
      }
    }, 1000);
    // }, [router.query.id, fetching, router]);
  }, [router.query.id, router]);

  return (
    <motion.div
      initial={{ width: 0 }}
      animate={{ width: '100%' }}
      className={'mt-5'}
      //   exit={{ x: window.innerWidth, transition: { duration: 1 } }}
    >
      {/* {fetching && property?.id && <CarouselCompo property={property} />}
      {fetching && property?.id && ( */}
      {property?.id && <CarouselCompo property={property} />}
      {property?.id && (
        <>
          <main className={`${style.main}`}>
            <div className={`${style['container-fluid']} mb-0`}>
              <div className={`row ${style.rows}`}>
                <div
                  className={`col-12 col-md-6 col-lg-5 p-0 ${style.pembungkus1}`}
                >
                  <div className={style.row}>
                    <div className="col d-flex flex-column">
                      <div className={style.header}>
                        <div className={`p-0 m-0 mb-3 ${style['rincian-1']}`}>
                          {property?.address && (
                            <DescriptionHousingComponent
                              key={property.id}
                              title={property.name}
                              address={`${property.address.street}, ${
                                property.address.lower_city[0].toUpperCase() +
                                property.address.lower_city.substring(1)
                              }`}
                              categories={property.categories.map(
                                (e) => e.name
                              )}
                              transaction={property.transaction}
                            />
                          )}
                        </div>
                        <div className={style['rincian-2']}>
                          <h1>Detail</h1>
                          <div className={`${style.rowRincian2} row`}>
                            <div className="col p-0">
                              <h3 className="mb-2">Pengembang</h3>
                              <p>{property.developer}</p>
                              <h3 className="mb-2 mt-3">Pembayaran</h3>
                              {property.payments?.map((payment, key) => (
                                <p key={key} className="mb-2">
                                  {payment}
                                </p>
                              ))}
                            </div>
                            <div className="col p-0">
                              <h3 className="mb-2">Tipe Properti</h3>
                              <p>{capitalizeWord(property.property_type)}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <ButtonCompo uriBrochure={property.brochure} />

                      <div
                        className={`${style['rincian-3']} ${style.item2} ${style.deskripsi}`}
                      >
                        {property.description && (
                          <DescHouseCompo
                            key={property.id}
                            description1={property.description}
                          />
                        )}
                      </div>
                      {property.type_houses?.length > 0 && (
                        <div
                          className={`${style['rincian-4']} ${style.item3} ${style.tpRumah}`}
                        >
                          <h1>Tipe Rumah</h1>
                          {/* <!-- For Dekstop--> */}
                          <div className={style.carouselContainerDekstop}>
                            <Carousel
                              controlsOffset="10%"
                              controlSize={40}
                              align="start"
                              height={300}
                              slideSize="10%"
                              dragFree
                              classNames={{
                                control: classes.control,
                              }}
                              styles={{
                                control: {
                                  '&[data-inactive]': {
                                    opacity: 0.5,
                                    cursor: 'default',
                                  },
                                },
                              }}
                            >
                              {property?.type_houses &&
                                property?.type_houses?.map((item, key) => {
                                  return (
                                    <Link
                                      href={{
                                        pathname: '/detail-tipe',
                                        query: {
                                          id: item.id,
                                          property_id: property.id,
                                        },
                                      }}
                                      key={key}
                                    >
                                      <CardDekstopCompo
                                        image={item.images[0]}
                                        title={item.name}
                                        address={item.property_type}
                                      />
                                    </Link>
                                  );
                                })}
                            </Carousel>
                          </div>
                          {/* <!-- End Dekstop --> */}

                          {/* <!-- For Mobile --> */}
                          <div className={style.carouselContainerMobile}>
                            <div
                              className={`row row-cols-2 row-cols-md-2 g-4 ${style.rowCardMobileCarousel}`}
                            >
                              {property?.type_houses &&
                                property?.type_houses?.map((item, key) => {
                                  return (
                                    <CardMobileCompo
                                      key={key}
                                      to={`/detail-tipe?id=${item.id}&property_id=${property.id}`}
                                      image={item.images[0]}
                                      title={item.name}
                                      address={item.property_type}
                                    />
                                  );
                                })}
                            </div>
                          </div>
                          {/* <!-- End For Mobile --> */}
                        </div>
                      )}
                      <InfoCardCompo />
                    </div>
                  </div>
                </div>
                <div
                  className={`col-12 col-md-6 col-lg-4 p-0 mt-5 ${style.item4} ${style.sitePlan} ${style.pembungkus2}`}
                >
                  <h1>SitePlan</h1>
                  <SitePlan
                    sitePlan={property.siteplan?.image}
                    background={property.siteplan.background}
                  />
                  {property.sketch && (
                    <>
                      <h1>Sketch</h1>
                      <div className={style.btnWrapper}>
                        {property.sketch && (
                          <Image
                            src={property.sketch}
                            alt=""
                            className={`mb-3 ${style.sitePlanImg}`}
                          />
                        )}
                        <button
                          type="button"
                          className={`btn btn-success rounded-pill ${style.tombolSitePlan}`}
                        >
                          <p className="m-0">Buka Siteplan</p>
                        </button>
                      </div>
                    </>
                  )}
                  <h1 className="mt-3">Lokasi Perumahan</h1>
                  <div
                    className={`${lokasiPerumStyle.lokasiPerumahan} p-3 rounded-4 shadow-lg mb-5 bg-body rounded mt-4`}
                  >
                    {property.near_facilities?.map((item, key) => {
                      return (
                        <LokasiPerumCompo
                          key={key}
                          icon={require('./assets/images/Card-Frame 502.png')}
                          title={item.name}
                          time={item.mileage + ' Menit'}
                        />
                      );
                    })}
                  </div>
                </div>

                <div
                  className={`col-12 col-md-0 col-lg-6 p-0 ${style['rincian-6']} mt-2 ${style.perumahanLn}`}
                >
                  <h1>Perumahan Lainnya</h1>
                </div>
              </div>
            </div>
            <Carousel
              controlsOffset="20%"
              controlSize={40}
              slideSize={350}
              slideGap={30}
              align="start"
              pl={280}
              mb={'xl'}
              classNames={{
                control: classes.control,
                root: classes.root,
              }}
            >
              {otherproperties?.map((item, key) => {
                return (
                  <Link
                    onClick={async (e) => {
                      await setTimeout(() => window.location.reload(), 100);
                    }}
                    href={`/properti?id=${item.id}`}
                    key={key}
                  >
                    <CarouselCompo2
                      image={item.images[0]}
                      title={item.name}
                      address={`${item.address.street}, ${capitalizeWord(
                        item.address.lower_city
                      )}`}
                    />
                  </Link>
                );
              })}
            </Carousel>
            <JumboTron2 />
          </main>
          <FooterCompo />
        </>
      )}
    </motion.div>
  );
};

export default HousingPage;

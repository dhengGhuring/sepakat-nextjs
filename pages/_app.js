import "../styles/globals.css";
import { Provider } from "react-redux";
import { store } from "../store";
import NavbarCompo from "./navbar/navbar-component";
import { ChakraProvider } from "@chakra-ui/react";
import Script from "next/script";
import Head from "next/head";

export default function App({ Component, pageProps, ...rest }) {
  return (
    <>
      <Head>
        <title>Sepakat Menjala Berkat</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#000000" />
        <meta
          name="facebook-domain-verification"
          content="rf11ik7ron1gr3x0tb5fk6q63ehvik"
        />
        <meta
          name="google-site-verification"
          content="8kPygb7akvkZQvTxZOVX0tSpipDn-ipBmrFdHUTWjqo"
        />
        <meta
          name="description"
          content="Platform agensi properti digital modern yang memudahkan kamu dalam proses pencarian properti impian dengan memilih agen yang kamu inginkan"
        />
        <meta
          httpEquiv="Content-Security-Policy"
          content="upgrade-insecure-requests"
        />
        <meta
          name="keywords"
          content="Agensi properti, cari properti, properti"
        />
        <meta name="author" content="PT Sepakat Menjala Berkat" />
        <meta name="robots" content="index, follow" />
      </Head>
      <Script
        id="bootstrap-cdn"
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
      />
      <Provider store={store}>
        <NavbarCompo />
        <ChakraProvider>
          <Component {...pageProps} />
        </ChakraProvider>
      </Provider>
    </>
  );
}

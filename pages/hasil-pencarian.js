import { motion } from 'framer-motion';
import { createStyles, Button } from '@mantine/core';
import { useEffect, useState } from 'react';
import JumboTron1 from '../Components/jumbotron-1/jumbotron1-component';
import JumboTron2 from '../Components/jumbotron-2/jumbotron2-component';
import ToolTip2 from '../Components/tooltip/toolTip2-component';
import CardTypeHouseCompo from '../Components/typeHouse/cardtypehouse-component';
import style from '../styles/HousingTypePageStyle.module.css';
import { useDispatch, useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { FooterCompo } from '../Components';
import {
  fetchProperties,
  propertySearchIsEmpty,
  propertySearchStore,
} from '../reducers/propertySearchSlice';
import Swal from 'sweetalert2';

const useStyle = createStyles((theme) => ({
  button: {
    width: 'unset!important',
  },
  row: {
    '@media (max-width: 600px)': {
      margin: 10,
    },
  },
}));

const ResultPage = () => {
  const { classes } = useStyle();
  const router = useRouter();
  const [selectedCity, setSelectedCity] = useState(router.query.city);
  const propertySearch = useSelector(propertySearchStore);
  const propertyCheck = useSelector(propertySearchIsEmpty);
  const dispatch = useDispatch();

  const transaction = router.query.transaction,
    categories = router.query.categories;

  async function searchProperties(uid, updated_at) {
    try {
      if (transaction?.length > 0 && categories?.length > 0) {
        dispatch(
          await fetchProperties(
            selectedCity ?? 'jember',
            transaction,
            categories,
            uid,
            updated_at
          )
        );
      } else {
        Swal.fire({
          title: 'Error!',
          text: 'Silahkan Pilih Properti dan Jenis Properti',
          icon: 'error',
          confirmButtonText: 'Ok',
        });
      }
    } catch (error) {
      console.log(error).catch((err) => {
        return Swal.fire({
          title: 'Error!',
          text: 'Silahkan Pilih Properti dan Jenis Properti dengan valid!',
          icon: 'error',
          confirmButtonText: 'Ok',
        });
      });
    }
  }

  return (
    <motion.div
      className="main"
      style={{ overflowX: 'hidden' }}
      initial={{ width: 0 }}
      animate={{ width: '100%' }}
      // exit={{ x: window.innerWidth, transition: { duration: 0.3 } }}
    >
      {selectedCity && <JumboTron1 city={selectedCity} />}
      <div className="container">
        {/* ToolTip Compo*/}
        <div
          className={`${style.toolTipWrapper} d-flex flex-row mt-5 align-items-center`}
        >
          <p>Hasil Pencarian</p>
          <ToolTip2 />
        </div>
        {/* End ToolTp Compo*/}
        {/* Type House Compo */}

        <div className="row justify-content-center mt-5">
          {propertySearch?.map((item, key) => {
            return (
              <div className="col-sm-12 col-md-10 col-lg-4" key={key}>
                <CardTypeHouseCompo
                  id={item.id}
                  image={item.images[0]}
                  title={item.name}
                  address={`${item.address.street}, ${
                    item.address.lower_city[0].toUpperCase() +
                    item.address.lower_city.substring(1)
                  }`}
                />
              </div>
            );
          })}
          {propertySearch.length % 3 === 0 && (
            <Button
              mt={30}
              mb={30}
              color="teal.9"
              radius="xl"
              className={classes.button}
              onClick={async () =>
                await searchProperties(
                  propertySearch[propertySearch.length - 1].id,
                  propertySearch[propertySearch.length - 1].updated_at.seconds
                )
              }
            >
              Lainnya
            </Button>
          )}
        </div>

        {/* End Type House */}
      </div>
      <JumboTron2 />
      <FooterCompo />
    </motion.div>
  );
};

export default ResultPage;

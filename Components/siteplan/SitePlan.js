import React from "react";
import Image from "next/image";
import redirectPlayStore from "../../utility/redirectPlayStore";
import { ReactSVG } from "react-svg";
import style from "../../styles/HousingDetailPageStyle.module.css";
import styleSiteplan from "../../styles/siteplan.module.css";

export default function SitePlan({ sitePlan, background }) {
  return (
    <>
      <div className={style.btnWrapper}>
        <div className={style.containerStackImage}>
          <Image
            src={background}
            alt="background siteplan"
            className={styleSiteplan.imageBackground}
            width={1000}
            height={1000}
          />
        </div>
        <div className={style.stackImage}>
          <div className={styleSiteplan.containerSiteplan}>
            <ReactSVG
              aria-label="Description of the overall image"
              role="img"
              crossOrigin="anonymous"
              evalScripts="always"
              httpRequestWithCredentials={false}
              loading={() => <span>Loading</span>}
              renumerateIRIElements={false}
              src={sitePlan}
              useRequestCache={false}
              wrapper="span"
              className={styleSiteplan.imageSiteplan}
              style={{
                position: "absolute",
                top: "0",
              }}
            />
          </div>
        </div>
        <button
          type="button"
          onClick={redirectPlayStore}
          className={`btn btn-success rounded-pill ${style.tombolSitePlan}`}
        >
          <p className="m-0">Buka Siteplan</p>
        </button>
      </div>
    </>
  );
}

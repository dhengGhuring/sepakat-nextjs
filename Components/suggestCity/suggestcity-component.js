import React from 'react';
import { ScrollArea, Stack } from '@mantine/core';
// import { suggestion } from '../../utility/constants';

const selectedSuggestion = 'Jember';

const SuggestCityComponent = (props) => {
  const {suggestion, selected } = props;
  return (
    <>
      <ScrollArea>
        <Stack mt={20} sx={{ flexDirection: 'row' }}>
          {suggestion.map((suggest) => (
            <button className="suggest-btn" key={suggest.name}>
              <span>{suggest.name}</span>
            </button>
          ))}
        </Stack>
      </ScrollArea>
    </>
  );
};

export default SuggestCityComponent;

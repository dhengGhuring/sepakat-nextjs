import React from 'react';
import { Title, Text, Group, createStyles } from '@mantine/core';
const useStyle = createStyles((theme) => ({
  title: {
    fontFamily: 'Nunito Sans, sans-serif',
    fontSize: 48,
    color: '#000000',
    fontWeight: 700,
    marginTop: 40,
    marginBottom: 10,
    '@media (max-width: 600px)': {
      fontSize: 20,
    },
  },
  textDetail: {
    fontSize: 24,
    fontWeight: 700,
    color: '#1C7849',
    '@media (max-width: 600px)': {
      fontSize: 16,
    },
  },
  textDetail2: {
    marginLeft: 40,
    fontSize: 24,
    fontWeight: 400,
    '@media (max-width: 600px)': {
      fontSize: 16,
    },
  },
}));
export default function TitleComponent(props) {
  const { classes } = useStyle();
  return (
    <>
      <Title className={classes.title}>{props.titleArticle}</Title>
      <Group>
        <Text className={classes.textDetail}>{props.adminWritter}</Text>
        <Text className={classes.textDetail2}>{props.date}</Text>
      </Group>
    </>
  );
}

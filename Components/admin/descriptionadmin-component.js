import React from 'react';
import { createStyles, Text } from '@mantine/core';
const useStyle = createStyles((theme) => ({
  text: {
    '@media (max-width: 600px)': {
      fontSize: 12,
    },
  },
}));
export default function DescriptionAdminComponent(props) {
  const { classes } = useStyle();
  return (
    <>
      <Text align="justify" className={classes.text}>
        {props.articles}
      </Text>
    </>
  );
}

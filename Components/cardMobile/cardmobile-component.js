import Link from "next/link";
import style from "./cardMobileStyle.module.css";

const CardMobileCompo = (props) => {
  return (
    <div className={style.mobileCard}>
      <div className="col">
        <div className={`card ${style.cards}`}>
          <Link href={props.to}>
            <img
              src={props.image}
              className={`card-img-top ${style["card-img-top"]}`}
              alt="Tipe Rumah"
            />
            <div className={`card-img-overlay ${style.card1}`}>
              <h5 className="card-title">{props.title}</h5>
              <p className="card-text">{props.address}</p>
            </div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default CardMobileCompo;

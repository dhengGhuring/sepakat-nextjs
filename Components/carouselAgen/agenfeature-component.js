import { createStyles, Container, Paper, Text, Grid } from '@mantine/core';
import briefcase from '../../pages/assets/icons/Man.png';
import chat from '../../pages/assets/icons/Hand.png';
import clipboard from '../../pages/assets/icons/Book.png';
import Image from 'next/image';
import Link from 'next/link';
const useStyle = createStyles((theme) => ({
  containerGrid: {
    textAlign: 'center',
  },
  grid: {
    '@media (max-width: 600px) and (max-height: 500px)': {
      marginTop: 40,
      textAlign: '-webkit-center',
    },
    '@media (max-width: 600px) and (min-height: 500px)': {
      marginTop: 60,
      textAlign: '-webkit-center',
    },
    '@media (max-width: 600px) and (min-height: 600px)': {
      marginTop: 20,
      textAlign: '-webkit-center',
    },
    '@media (max-width: 600px) and (min-height: 700px)': {
      marginTop: 40,
    },
    '@media (min-width: 600px)': {
      textAlign: '-webkit-center',
    },
    '@media (min-width: 600px) and (max-height: 500px)': {
      marginTop: 60,
    },
    '@media (min-width: 600px) and (min-height: 500px)': {
      marginTop: 70,
    },
    '@media (min-width: 600px) and (min-height: 600px)': {
      marginTop: 60,
    },
    '@media (min-width: 992px)': {
      marginTop: 80,
    },
    // '@media (min-width: 992px) and (max-height: 500px)': {
    //   marginTop: 140,
    // },
    // '@media (min-width: 992px) and (min-height: 700px)': {
    //   marginTop: 112,
    // },
    '@media (min-height: 800px) and (min-width: 1600px)': {
      marginTop: 400,
      left: '20%!important',
    },
    '@media (min-width: 1200px) and (max-height: 500px)': {
      marginTop: 20,
    },
    '@media (min-width: 1200px) and (min-height: 500px)': {
      marginTop: 60,
    },
    '@media (min-width: 1200px) and (min-height: 600px)': {
      marginTop: 20,
    },
    '@media (min-width: 1200px) and (min-height: 700px)': {
      marginTop: 0,
      marginBottom: 20,
    },
    '@media  (min-width: 1200px) (min-height: 800px) and': {
      marginTop: 425,
      left: '9%',
    },
    '@media (min-width: 1400px) and (max-height: 500px)': {
      marginTop: 55,
      padding: '0 80px',
    },
    '@media (min-width: 1400px) and (min-height: 500px)': {
      marginTop: 60,
      padding: '0 80px',
    },
    '@media (min-width: 1400px) and (min-height: 600px)': {
      marginTop: 50,
    },
    '@media (min-width: 1400px) and (min-height: 700px)': {
      marginTop: 50,
      padding: '0 100px',
    },
    '@media (min-width: 1400px) and (min-height: 800px)': {
      marginTop: 0,
      padding: '0 100px',
    },
    '@media (min-width: 1600px) and (max-height: 500px)': {
      marginTop: 45,
      padding: 0,
    },
    '@media (min-width: 1600px) and (min-height: 500px)': {
      padding: 0,
    },
    '@media (min-width: 1600px) and (min-height: 600px)': {
      padding: '0 50px',
    },
    '@media (min-width: 1800px) and (max-height: 500px)': {
      padding: '0 30px',
    },
    '@media (min-width: 1800px) and (min-height: 500px)': {
      padding: '0 30px',
    },
    '@media (min-width: 1800px) and (min-height: 900px)': {
      padding: '0 30px',
    },
  },
  container: {
    width: 150,
    height: 150,
    padding: 12,
    borderRadius: 16,
    position: 'absolute',
    bottom: 160,
    right: 50,
    '@media (max-width: 600px)': {
      width: '100px!important',
      height: '100px!important',
      bottom: 0,
      top: '-60px!important',
      right: '66px!important',
    },
    '@media (min-width: 1200px)': {
      right: 0,
    },
    '@media (max-height: 500px)': {
      bottom: 90,
    },
    '@media (min-height: 500px)': {
      bottom: 120,
    },
    '@media (min-height: 600px)': {
      bottom: 141,
    },
  },
  imageAgen: {
    width: 150,
    height: 150,
    objectFit: 'cover',
    objectPosition: 'center',
    '@media (max-width: 600px)': {
      width: '100px!important',
      height: '100px!important',
    },
  },
  paper: {
    position: 'relative',
    width: 416,
    height: 220,
    display: 'flex',
    alignItems: 'end',
    background: 'rgba(245, 245, 245, 0.4)',
    backdropFilter: 'blur(10px)',
    border: '2px solid rgba(245, 245, 245, 0.2)',
    '@media (max-width: 600px)': {
      width: '240px!important',
      height: '150px!important',
      marginBottom: '35px!important',
    },
    '@media (min-width: 600px)': {
      marginBottom: 80,
    },
    '@media (min-width: 992px)': {
      marginBottom: 30,
    },
    '@media (min-width: 992px) and (min-height: 400px)': {
      width: 293,
    },
    '@media (min-width: 1200px)': {
      width: 350,
      marginBottom: 0,
    },
    '@media (max-height: 500px)': {
      height: 150,
    },
    '@media (min-height: 500px)': {
      height: 180,
    },
    '@media (min-height: 600px)': {
      height: 200,
    },
  },
  text: {
    fontFamily: 'Poppins, sans-serif',
    fontWeight: 700,
    fontSize: 24,
    color: '#F2EEC8',
    '@media (max-width: 600px)': {
      fontSize: '16px!important',
    },
    '@media (max-height: 500px)': {
      fontSize: 17,
    },
    '@media (min-height: 500px)': {
      fontSize: 19,
    },
    '@media (min-height: 600px)': {
      fontSize: 21,
    },
  },
  btn: {
    fontFamily: 'Nunito, sans-serif !important',
    fontSize: '15px !important',
    fontWeight: '700 !important',
    padding: '12px !important',
    marginBottom: 30,
  },
}));
function AgenFeature() {
  const { classes } = useStyle();
  return (
    <>
      <div className={`container ${classes.containerGrid}`}>
        <Grid className={classes.grid}>
          <Grid.Col md={4} lg={4}>
            <Paper
              shadow="xs"
              radius="lg"
              p="xl"
              className={classes.paper}
              data-aos="fade-up"
              data-aos-duration="200"
            >
              <Container className={classes.container}>
                <Image
                  src={briefcase}
                  className={classes.imageAgen}
                  alt="brifcase"
                />
              </Container>
              <Text className={classes.text}>
                Agensi berlisensi dan berbadan hukum
              </Text>
            </Paper>
          </Grid.Col>

          <Grid.Col md={4} lg={4}>
            <Paper
              shadow="xs"
              radius="lg"
              p="xl"
              className={classes.paper}
              data-aos="fade-up"
              data-aos-duration="250"
            >
              <Container className={classes.container}>
                <Image src={chat} className={classes.imageAgen} alt="chat" />
              </Container>
              <Text className={classes.text}>
                Berinteraksi dengan klien secara digital dan terintegrasi
              </Text>
            </Paper>
          </Grid.Col>
          <Grid.Col md={4} lg={4}>
            <Paper
              shadow="xs"
              radius="lg"
              p="xl"
              className={classes.paper}
              data-aos="fade-up"
              data-aos-duration="300"
            >
              <Container className={classes.container}>
                <Image
                  src={clipboard}
                  className={classes.imageAgen}
                  alt="clipboard"
                />
              </Container>
              <Text className={classes.text}>
                Mendapat pelatihan berstandar BNSP Properti Indonesia
              </Text>
            </Paper>
          </Grid.Col>
        </Grid>
        <Link
          className={`btnAgenContainer ${classes.btnAgenContainer}`}
          href="https://api.whatsapp.com/send/?phone=6281335258080&text=Saya+ingin+mendaftar+menjadi+Agen%2C+bagaimana+cara+untuk+menjadi+agen+di+Sepakat+Properti&type=phone_number&app_absent=0"
          target="_blank"
        >
          <button
            className={`btn btn-success rounded-pill my-lg-5 ${classes.btn}`}
            data-aos="fade-up"
            data-aos-duration="400"
          >
            Gabung Sepakat
          </button>
        </Link>
      </div>
    </>
  );
}

export default AgenFeature;

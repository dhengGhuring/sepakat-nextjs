import { Carousel } from "@mantine/carousel";
import { BackgroundImage, Button, createStyles } from "@mantine/core";
import Image from "next/image";
import promo1 from "../../pages/assets/images/promo/PromoLandscape/PromoLandscape1.webp";
import promoPotrait1 from "../../pages/assets/images/promo/PromoPotrait/PromoPotrait1.webp";
import promo2 from "../../pages/assets/images/promo/PromoLandscape/PromoLandscape2.webp";
import promoPotrait2 from "../../pages/assets/images/promo/PromoPotrait/PromoPotrait2.webp";
import promo3 from "../../pages/assets/images/promo/PromoLandscape/ASA-Landscape.webp";
import promoPotrait3 from "../../pages/assets/images/promo/PromoPotrait/ASA2.webp";
import promo4 from "../../pages/assets/images/promo/PromoLandscape/PRAJEKAN-Landscape.webp";
import promoPotrait4 from "../../pages/assets/images/promo/PromoPotrait/PRAJEKAN2.webp";
import promo5 from "../../pages/assets/images/promo/PromoLandscape/Promo-Gebyar-Diamond-Landscape.webp";
import promoPotrait5 from "../../pages/assets/images/promo/PromoPotrait/Promo-Gebyar-Diamond-Potrait.webp";
const useStyle = createStyles((theme) => ({
    carousel: {
        height: "100vh",
        "@media (min-width: 1200px)": {
            height: "90vh",
            transition: "0.5s",
        },
    },
    carouselSlide: {
        "@media (max-height: 600px)": {
            marginBottom: "100px!important",
        },
    },
    backgroundImageMobile: {
        height: "100%",
        "@media (min-width: 600px)": {
            display: "block",
        },
        // "@media (max-width: 991px and min-width: 701px)": {
        //     display: "none",
        // },
        "@media (min-width: 992px)": {
            display: "none",
        },
        "@media (min-width: 1200px)": {
            display: "none",
        },
    },
    backgroundImage: {
        height: "100%",
        backgroundSize: "cover!Important",
        backgroundRepeat: "no-repeat",
        // "@media (max-width: 700px)": {
        //     backgroundSize: "cover!Important",
        //     display: "none",
        // },
        "@media (max-width: 600px)": {
            backgroundSize: "cover!Important",
            display: "none",
        },
        "@media (min-width: 600px)": {
            backgroundSize: "cover!important",
            display: "none",
        },

        // "@media (max-width: 991px) and (min-width: 700px) and (max-height: 500px) and (min-height: 400px)":
        //     {
        //         backgroundSize: "cover!important",
        //         display: "block",
        //         height: "100%",
        //     },
        "@media (max-height: 500px) and (min-height: 300px)": {
            backgroundSize: "cover!important",
            display: "block",
        },
        "@media (min-width: 992px)": {
            backgroundSize: "cover!important",
            display: "block",
        },
        "@media (min-width: 1200px)": {
            backgroundSize: "cover!important",
            height: "100%",
            transition: "0.5s",
            display: "block",
        },
        "@media (max-height: 600px)": {
            backgroundSize: "cover!important",
            height: "100vh",
        },
        "@media (min-height: 700px)": {
            height: "100%",
        },
        "@media (min-height: 800px)": {
            height: "100%",
        },
    },
    text: {
        fontWeight: 700,
        fontFamily: "Poppins, sans-serif",
        "@media (max-width: 600px)": {
            fontSize: 25,
        },
        "@media (min-width: 600px)": {
            fontSize: 26,
        },
        "@media (min-width: 1200px)": {
            fontSize: 30,
        },
    },
    containerButton: {
        position: "absolute",
        bottom: 100,
        right: 100,
        "@media (max-width: 600px)": {
            bottom: 35,
            right: 30,
        },
        "@media (max-width: 600px) and (max-height: 500px)": {
            bottom: 10,
            right: 28,
        },
        "@media (max-width: 600px) and (min-height: 500px)": {
            bottom: 10,
            right: 28,
        },
        "@media (max-width: 600px) and (min-height: 600px)": {
            bottom: 10,
            right: 28,
        },
        "@media (max-width: 600px) and (min-height: 800px)": {
            bottom: 20,
            right: 28,
        },
        "@media (min-width: 600px) and (min-height: 500px)": {
            bottom: 30,
            right: 40,
        },
        "@media (min-width: 600px) and (min-height: 500px)": {
            bottom: 35,
            right: 40,
        },
        "@media (min-width: 600px) and (min-height: 600px)": {
            bottom: 30,
            right: 40,
        },
        "@media (min-width: 768px) and (max-height: 500px)": {
            bottom: -80,
        },
        "@media (min-width: 768px) and (min-height: 500px)": {
            bottom: 30,
        },
        "@media (min-width: 768px) and (min-height: 600px)": {
            bottom: 40,
        },
        "@media (min-width: 1200px) and (max-height: 500px)": {
            bottom: -50,
        },
        "@media (min-width: 1200px) and (min-height: 500px)": {
            bottom: -15,
        },
        "@media (min-width: 1200px) and (min-height: 600px)": {
            bottom: 90,
        },
    },
    button: {
        width: "100%",
        maxWidth: 500,
        maxHeight: 81,
        height: "100%",
        padding: "24px 56px 24px 56px",
        fontSize: 20,
        boxShadow:
            "rgba(0, 0, 0, 0.07) 0px 1px 2px, rgba(0, 0, 0, 0.07) 0px 2px 4px, rgba(0, 0, 0, 0.07) 0px 4px 8px, rgba(0, 0, 0, 0.07) 0px 8px 16px, rgba(0, 0, 0, 0.07) 0px 16px 32px, rgba(0, 0, 0, 0.07) 0px 32px 64px",
        "@media (max-width: 600px)": {
            padding: "15px 0px 15px 0px",
            width: "100%",
            transition: "0.5s",
        },
        "@media (max-width: 600px) and (max-height: 500px)": {
            padding: 10,
            fontSize: "16px",
            maxWidth: "280px",
            maxHeight: "70px",
        },
        "@media (max-width: 600px) and (min-height: 500px)": {
            padding: 10,
            fontSize: "16px",
            maxWidth: "280px",
            maxHeight: "70px",
        },
        "@media (max-width: 600px) and (min-height: 800px)": {
            padding: 15,
            fontSize: "16px",
            maxWidth: "280px",
            maxHeight: "70px",
        },
        "@media (min-width: 600px) and (max-height: 500px)": {
            padding: 15,
            transition: "0.5s",
            width: "100%",
            fontSize: 13,
        },
        "@media (min-width: 600px) and (min-height: 500px)": {
            padding: 15,
            transition: "0.5s",
            width: "100%",
            fontSize: 13,
        },
        "@media (min-width: 992px)": {
            fontSize: 16,
        },

        "@media (min-width: 1200px)": {
            padding: 20,
            fontSize: 35,
            fontWeight: 700,
            width: "100%",
        },
        "@media (min-width: 1200px) and (max-height: 500px)": {
            padding: 15,
            fontSize: 17,
        },
        "@media (min-width: 1200px) and (min-height: 500px)": {
            padding: 15,
            fontSize: 17,
        },
    },
}));

const PromotionCompo = (props) => {
    const { classes } = useStyle();
    return (
        <Carousel
            align="start"
            className={classes.carousel}
            // sx={{ maxWidth: 2000 }}
            mx="auto"
            height="100%"
            controlsOffset={30}
            controlSize={70}
            styles={{
                control: {
                    backgroundColor: "#F2EEC8",
                    color: "#F56600",
                    borderColor: "#F2EEC8",
                    fontSize: 1,
                    opacity: 0.2,
                    "&[data-inactive]": {
                        opacity: 0,
                        cursor: "default",
                    },
                },
            }}
        >
            <Carousel.Slide className={classes.carouselSlide}>
                <BackgroundImage
                    src={promo1}
                    className={classes.backgroundImage}
                    style={{
                        backgroundImage: `url(${promo1.src})`,
                    }}
                >
                    <a
                        className={classes.containerButton}
                        href="https://www.instagram.com/p/Cg3GTsZoBqT/?utm_source=ig_web_copy_link"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <Button
                            radius={100}
                            color="teal.9"
                            className={classes.button}
                        >
                            Selengkapnya
                        </Button>
                    </a>
                </BackgroundImage>
                <BackgroundImage
                    src={promoPotrait1}
                    className={classes.backgroundImageMobile}
                    style={{
                        backgroundImage: `url(${promoPotrait1.src})`,
                    }}
                >
                    <a
                        className={classes.containerButton}
                        href="https://www.instagram.com/p/Cg3GTsZoBqT/?utm_source=ig_web_copy_link"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <Button
                            radius={100}
                            color="teal.9"
                            className={classes.button}
                        >
                            Selengkapnya
                        </Button>
                    </a>
                </BackgroundImage>
            </Carousel.Slide>

            <Carousel.Slide className={classes.carouselSlide}>
                <BackgroundImage
                    src={promo2}
                    className={classes.backgroundImage}
                    style={{
                        backgroundImage: `url(${promo2.src})`,
                    }}
                >
                    <a
                        className={classes.containerButton}
                        href="https://www.instagram.com/p/Cg3GTsZoBqT/?utm_source=ig_web_copy_link"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <Button
                            radius="xl"
                            color="teal.9"
                            className={classes.button}
                        >
                            Selengkapnya
                        </Button>
                    </a>
                </BackgroundImage>
                <BackgroundImage
                    src={promoPotrait2}
                    className={classes.backgroundImageMobile}
                    style={{
                        backgroundImage: `url(${promoPotrait2.src})`,
                    }}
                >
                    <a
                        className={classes.containerButton}
                        href="https://www.instagram.com/p/Cg3GTsZoBqT/?utm_source=ig_web_copy_link"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <Button
                            radius={100}
                            color="teal.9"
                            className={classes.button}
                        >
                            Selengkapnya
                        </Button>
                    </a>
                </BackgroundImage>
            </Carousel.Slide>

            <Carousel.Slide className={classes.carouselSlide}>
                <BackgroundImage
                    src={promo3}
                    className={classes.backgroundImage}
                    style={{
                        backgroundImage: `url(${promo3.src})`,
                    }}
                >
                    <a
                        className={classes.containerButton}
                        href="https://www.instagram.com/p/Cg3GTsZoBqT/?utm_source=ig_web_copy_link"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <Button
                            radius="xl"
                            color="teal.9"
                            className={classes.button}
                        >
                            Selengkapnya
                        </Button>
                    </a>
                </BackgroundImage>
                <BackgroundImage
                    src={promoPotrait3}
                    className={classes.backgroundImageMobile}
                    style={{
                        backgroundImage: `url(${promoPotrait3.src})`,
                    }}
                >
                    <a
                        className={classes.containerButton}
                        href="https://www.instagram.com/p/Cg3GTsZoBqT/?utm_source=ig_web_copy_link"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <Button
                            radius={100}
                            color="teal.9"
                            className={classes.button}
                        >
                            Selengkapnya
                        </Button>
                    </a>
                </BackgroundImage>
            </Carousel.Slide>

            <Carousel.Slide className={classes.carouselSlide}>
                <BackgroundImage
                    src={promo4}
                    className={classes.backgroundImage}
                    style={{
                        backgroundImage: `url(${promo4.src})`,
                    }}
                >
                    <a
                        className={classes.containerButton}
                        href="https://www.instagram.com/p/Cg3GTsZoBqT/?utm_source=ig_web_copy_link"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <Button
                            radius="xl"
                            color="teal.9"
                            className={classes.button}
                        >
                            Selengkapnya
                        </Button>
                    </a>
                </BackgroundImage>
                <BackgroundImage
                    src={promoPotrait4}
                    className={classes.backgroundImageMobile}
                    style={{
                        backgroundImage: `url(${promoPotrait4.src})`,
                    }}
                >
                    <a
                        className={classes.containerButton}
                        href="https://www.instagram.com/p/Cg3GTsZoBqT/?utm_source=ig_web_copy_link"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <Button
                            radius={100}
                            color="teal.9"
                            className={classes.button}
                        >
                            Selengkapnya
                        </Button>
                    </a>
                </BackgroundImage>
            </Carousel.Slide>

            <Carousel.Slide className={classes.carouselSlide}>
                <BackgroundImage
                    src={promo5}
                    className={classes.backgroundImage}
                    style={{
                        backgroundImage: `url(${promo5.src})`,
                    }}
                >
                    <a
                        className={classes.containerButton}
                        href="https://www.instagram.com/p/Cg3GTsZoBqT/?utm_source=ig_web_copy_link"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <Button
                            radius="xl"
                            color="teal.9"
                            className={classes.button}
                        >
                            Selengkapnya
                        </Button>
                    </a>
                </BackgroundImage>
                <BackgroundImage
                    src={promoPotrait5}
                    className={classes.backgroundImageMobile}
                    style={{
                        backgroundImage: `url(${promoPotrait5.src})`,
                    }}
                >
                    <a
                        className={classes.containerButton}
                        href="https://www.instagram.com/p/Cg3GTsZoBqT/?utm_source=ig_web_copy_link"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <Button
                            radius={100}
                            color="teal.9"
                            className={classes.button}
                        >
                            Selengkapnya
                        </Button>
                    </a>
                </BackgroundImage>
            </Carousel.Slide>
        </Carousel>
    );
};

export default PromotionCompo;

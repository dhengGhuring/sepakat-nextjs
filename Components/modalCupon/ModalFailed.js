import React from "react";
import { Modal, Text, Box, createStyles } from "@mantine/core";
import ImageFailed from "../../pages/assets/images/image-modalField.png";
import Image from "next/image";

export default function ModalField({ open, setModalFailed }) {
  const useStyle = createStyles((theme) => ({
    imageFailed: {
      padding: "3rem 0",
      justifyContent: "center",
      justifyItems: "center",
      justifySelf: "center",
      alignSelf: "center",
      margin: "auto",
      display: "flex",
      maxWidth: "50%",
      "@media (max-width: 768px)": {
        padding: "1rem 0",
      },
    },
  }));
  const { classes } = useStyle();
  return (
    <>
      <Modal
        opened={open}
        onClose={() => setModalFailed(false)}
        centered
        withCloseButton={false}
        padding="0"
      >
        <Box
          sx={(theme) => ({
            width: "100%",
            height: "100%",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            gap: "2rem",
            backgroundColor: "red",
            "& h1": {
              fontSize: "3rem",
              fontWeight: "bold",
              marginBottom: "1rem",
            },
          })}
        >
          <Text
            color={"white"}
            weight={"bold"}
            size={"xl"}
            sx={(theme) => ({
              padding: "0.5rem 0",
              "@media (max-width: 768px)": {
                fontSize: "1rem",
              },
            })}
          >
            Tidak Ditemukan
          </Text>
        </Box>
        <Image
          src={ImageFailed}
          alt="ImageFailed"
          className={classes.imageFailed}
        />
        <Text
          align={"center"}
          weight={"bold"}
          size={"xl"}
          sx={(theme) => ({
            padding: "1rem 4rem",
            "@media (max-width: 768px)": {
              padding: "1rem 1rem",
              fontSize: "1rem",
            },
          })}
        >
          Nomor undian atau Nomor HP ada yang salah
        </Text>
      </Modal>
    </>
  );
}

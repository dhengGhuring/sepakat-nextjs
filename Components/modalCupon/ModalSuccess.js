import React, { useEffect, useState } from "react";
import { Modal, Text, Box, createStyles } from "@mantine/core";
import Image from "next/image";
import ImageKuponPotrait from "../../pages/assets/images/kupon-potrait.jpg";
import ImageKuponLandscape from "../../pages/assets/images/kupon-landscape.png";

export default function ModalSuccess(props) {
  const [width, setWidth] = useState(0);
  const useStyle = createStyles((theme) => ({
    cuponContainer: {
      position: "relative",
      userSelect: "none",
    },
    img: {
      width: "50%",
    },
    cuponImage: {
      padding: "2rem",
      position: "relative",
      justifyContent: "center",
      justifyItems: "center",
      justifySelf: "center",
      alignSelf: "center",
      margin: "auto",
      display: "flex",
      width: "100%",
      "@media (max-width: 768px)": {
        padding: "0.5rem",
      },
    },
    name: {
      fontSize: "10px",
      fontWeight: "bold",
      color: "#F56600",
      position: "absolute",
      top: "34.5%",
      left: "8%",
      "@media (max-width: 768px)": {
        top: "61.5%",
        left: "50%",
        margin: "0 0 0 -40px",
      },
    },
    handphone: {
      fontSize: "10px",
      fontWeight: "bold",
      color: "#F56600",
      position: "absolute",
      top: "45.4%",
      left: "8%",
      "@media (max-width: 768px)": {
        top: "70%",
        left: "50%",
        margin: "0 0 0 -35px",
      },
    },
    pembelian: {
      fontSize: "10px",
      fontWeight: "bold",
      color: "#F56600",
      position: "absolute",
      top: "57%",
      left: "8%",
      "@media (max-width: 768px)": {
        top: "77.5%",
        left: "50%",
        margin: "0 0 0 -35px",
      },
    },
    number: {
      fontSize: "20px",
      fontWeight: "bold",
      color: "#F56600",
      position: "absolute",
      top: "70%",
      left: "18%",
      "@media (max-width: 768px)": {
        fontSize: "14px",
        top: "84%",
        left: "45%",
        margin: "0 0 0 -8px",
      },
    },
  }));
  const { classes } = useStyle();

  useEffect(() => {
    setWidth(window.innerWidth);
  }, []);
  return (
    <>
      <Modal
        opened={props.open}
        onClose={() => props.setModalSuccess(false)}
        centered
        withCloseButton={false}
        padding="0"
        size={"xl"}
      >
        <Box
          sx={(theme) => ({
            width: "100%",
            height: "100%",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            gap: "2rem",
            backgroundColor: "green",
            "& h1": {
              fontSize: "3rem",
              fontWeight: "bold",
              marginBottom: "1rem",
            },
          })}
        >
          <Text
            color={"white"}
            weight={"bold"}
            size={"xl"}
            sx={(theme) => ({
              padding: "0.5rem 0",
              "@media (max-width: 768px)": {
                fontSize: "1rem",
              },
            })}
          >
            Berhasil
          </Text>
        </Box>
        <Box className={classes.cuponContainer}>
          <Image
            src={width > 768 ? ImageKuponLandscape : ImageKuponPotrait}
            height={width > 768 ? "auto" : 400}
            alt="Image Kupon"
            className={classes.cuponImage}
          />
          <Text className={classes.name}>{props.name}</Text>
          <Text className={classes.handphone}>{props.phone}</Text>
          <Text className={classes.pembelian}>{props.pembelian}</Text>
          <Text className={classes.number}>{props.number}</Text>
        </Box>
        <Text
          weight={"bold"}
          size={"lg"}
          sx={(theme) => ({
            padding: "1rem 2rem",
            "& span": {
              color: "#F56600",
            },
            "@media (max-width: 768px)": {
              padding: "1rem 3rem",
              fontSize: "1.2rem",
            },
            "@media (max-width: 480px)": {
              padding: "1rem 2rem",
              fontSize: "1rem",
            },
          })}
        >
          No Kupon Undian <span>{props.number}</span> atas Nama
          <span> {props.name}</span> dengan pembelian{" "}
          <span>{props.pembelian}</span> berstatus valid hingga 31 Juli 2023.
        </Text>
        <Text
          weight={"lighter"}
          size={"md"}
          sx={(theme) => ({
            padding: "1rem 2rem",
            "@media (max-width: 768px)": {
              padding: "1rem 3rem",
              fontSize: "1rem",
            },
            "@media (max-width: 480px)": {
              padding: "1rem 2rem",
              fontSize: "0.8rem",
            },
          })}
        >
          Nantikan pengundian hadiahnya pada tanggal <br /> 18 Agustus 2023.
          <br />
          Semoga beruntung!
        </Text>
      </Modal>
    </>
  );
}

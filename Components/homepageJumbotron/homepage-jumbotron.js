import { Carousel } from '@mantine/carousel';
import { createStyles, Image } from '@mantine/core';
import SearchBoxCompo from '../searchbox/searchbox-component';
const useStyle = createStyles((theme) => ({
  // Init commit
  jumbotron: {
    marginTop: 60,
    height: '100%',
    '@media (max-width: 600px)': {
      marginTop: 55,
    },
  },
  searchBoxWrapper: {
    maxWidth: 600,
  },
  titleWrapper: {
    position: 'absolute',
    zIndex: 1,
    transition: '0.5s',
    paddingLeft: '5%',
    width: '100%',
    '@media (max-width: 600px)': {
      bottom: -50,
    },
    '@media (min-width: 600px) and (max-height: 500px)': {
      bottom: 35,
    },
    '@media (min-width: 600px) and (min-height: 500px)': {
      bottom: 10,
      left: 0,
      transition: '0.5s',
    },
    '@media (min-width: 600px) and (min-height: 600px)': {
      bottom: 20,
      left: 0,
      transition: '0.5s',
    },
    '@media (min-width: 768px) and (max-height: 500px)': {
      bottom: 50,
      left: 0,
      transition: '0.5s',
    },
    '@media (min-width: 768px) and (min-height: 500px)': {
      bottom: 20,
      left: 0,
      transition: '0.5s',
    },
    '@media (min-width: 992px) and (max-height:500px)': {
      bottom: 100,
      left: 0,
      transition: '0.5s',
    },
    '@media (min-width: 992px) and (min-height:500px)': {
      bottom: 30,
      left: 0,
      transition: '0.5s',
    },
    '@media (min-width: 1200px)': {
      bottom: 100,
    },
    '@media (min-width:1200px) and (min-height: 700px)': {
      bottom: 100,
    },
    '@media (min-width:1200px) and (min-height: 800px)': {
      bottom: 20,
    },
    '@media (min-width:1200px) and (min-height: 900px)': {
      bottom: 300,
    },
    '@media (min-width:1400px) and (min-height: 700px)': {
      bottom: 100,
    },
  },
  h1: {
    fontFamily: 'Poppins, sans-serif',
    fontSize: 65,
    fontWeight: 700,
    color: '#f56600',
    marginBottom: 5,
    transition: '0.5s',
    '@media (max-width: 600px)': {
      fontSize: '50px!important',
    },
    '@media (min-width: 768px)': {
      fontSize: 80,
      transition: '0.5s',
    },
    '@media (max-height: 500px)': {
      fontSize: 50,
    },
    '@media (min-height: 600px)': {
      fontSize: 65,
    },
    '@media (min-width: 1600px) and (max-height: 500px)': {
      fontSize: 70,
    },
    '@media (min-width: 1600px) and (min-height: 500px)': {
      fontSize: 65,
    },
  },
  p: {
    fontFamily: 'Poppins, sans-serif',
    fontSize: 20,
    fontWeight: 700,
    color: '#fff',
    transition: '0.5s',
    '@media (max-width: 600px)': {
      fontSize: '25px!important',
      marginBottom: 16,
    },
    '@media (min-width: 768px)': {
      fontSize: 35,
      transition: '0.5s',
    },
    '@media (max-height: 500px)': {
      fontSize: 30,
    },
    '@media (min-height: 500px)': {
      fontSize: 30,
    },
    '@media (min-height: 600px)': {
      fontSize: 35,
    },
  },
  slide: {
    background: '#0000!important',
    '@media (max-width: 600px)': {
      height: 200,
    },
  },
  carousel: {
    height: '100%',
  },
  video: {
    '@media (max-width: 600px)': {
      marginTop: 0,
    },
    '@media (min-width: 600px)': {
      marginTop: 0,
    },
  },
}));
const HomePageJumbotron = () => {
  const { classes } = useStyle();
  return (
    <>
      <div
        className={`jumbotron jumbotron-fluid ${classes.jumbotron}`}
        style={{ position: 'relative' }}
      >
        <div className={classes.titleWrapper}>
          <h1 className={classes.h1}>Sepakat</h1>
          <p className={classes.p}>#MudahCariProperti</p>
          <div className={classes.searchBoxWrapper}>
            <SearchBoxCompo />
          </div>
        </div>
        <Carousel
          controlSize={60}
          controlsOffset={30}
          className={classes.carousel}
          styles={{
            control: {
              backgroundColor: '#F2EEC8',
              color: '#F56600',
              borderColor: '#F2EEC8',
              opacity: 0.3,
              '&[data-inactive]': {
                opacity: 0,
                cursor: 'default',
              },
              '@media (max-width: 600px)': {},
              '@media (min-width: 600px)': {},
              '@media (min-width: 992px)': {
                display: 'flex',
              },
            },
          }}
        >
          <Carousel.Slide className={classes.slide}>
            <video
              autoPlay
              muted
              loop
              width="100%"
              className={classes.video}
              src={
                'https://propertiku-s3.s3.ap-southeast-1.amazonaws.com/test1.mp4'
              }
            ></video>
          </Carousel.Slide>
          <Carousel.Slide>
            <video
              autoPlay
              muted
              loop
              width="100%"
              className={classes.video}
              src={
                'https://propertiku-s3.s3.ap-southeast-1.amazonaws.com/test2.mp4'
              }
            ></video>
          </Carousel.Slide>
        </Carousel>
      </div>
    </>
  );
};
export default HomePageJumbotron;

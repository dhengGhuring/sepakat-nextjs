import {
    createStyles,
    BackgroundImage,
    Container,
    Title,
    Text,
} from "@mantine/core";

const useStyles = createStyles((theme) => ({
    backgroundimage: {
        height: "100%",
        marginBottom: 25,
    },
    container: {
        paddingTop: "35%",
        paddingBottom: 24,
        paddingLeft: 24,
        position: "relative",
    },
    title: {
        fontFamily: "Nunito Sans, sans-serif",
        fontWeight: 700,
        fontSize: 20,
        textOverflow: "ellipsis",
        whiteSpace: "nowrap",
        overflow: "hidden",
    },
    text: {
        fontFamily: "Nunito Sans, sans-serif",
        fontSize: 16,
        fontWeight: 400,
        textOverflow: "ellipsis",
    },
}));

const CardTypeHouseCompo = (props) => {
    const { classes } = useStyles();
    return (
        <div>
            <BackgroundImage
                src={`"${props.image} "`}
                className={classes.backgroundimage}
                radius="lg"
                component="a"
                href={`/properti?id=${props.id}`}
            >
                <Container className={classes.container}>
                    <Title order={5} color="white" className={classes.title}>
                        {props.title}
                    </Title>
                    <Text color="white" className={classes.text}>
                        {props.address}
                    </Text>
                </Container>
            </BackgroundImage>
        </div>
    );
};

export default CardTypeHouseCompo;

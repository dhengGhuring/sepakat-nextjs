import style from "./toolTipCompoStyle.module.css";
import Image from "next/image";

const ToolTip2 = () => {
    return (
        <div className={style.toolTip}>
            <span className={style.toolTipText}>
                Klik untuk melihat lebih rinci
            </span>
            <span>
                <Image
                    src={require("../../pages/assets/icons/Information.png")}
                    alt=""
                    className={style.tip}
                />
            </span>
        </div>
    );
};
export default ToolTip2;

import style from "./toolTipCompoStyle.module.css";
import Image from "next/image";

const ToolTip = (props) => {
    const { message } = props;
    return (
        <div className={style.toolTip}>
            <span className={style.toolTipText}>{message}</span>
            <span>
                <Image
                    src={require("../../pages/assets/icons/Information.png")}
                    alt=""
                    className={style.tip}
                />
            </span>
        </div>
    );
};
export default ToolTip;

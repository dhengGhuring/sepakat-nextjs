import Image from 'next/image';
import Link from 'next/link';
import React from 'react';
import style from './topBarCompoStyle.module.css';
class TopBarCompo extends React.Component {
  render() {
    return (
      <nav className={`${style.navbar1} navbar bg-success`}>
        <div className={`${style.topbar} container justify-content-star`}>
          <Link className="navbar-brand ms-auto a1" to="/content-video">
            Edukasi Properti
          </Link>
          <a
            className="navbar-brand a2"
            href="https://play.google.com/store/apps/details?id=id.asc.sepakat"
            target="_blank"
            rel="noreferrer"
          >
            Download Apps
          </a>
          <Image
            src={require('../../pages/assets/images/bi_phone.webp')}
            alt=""
          />
        </div>
      </nav>
      // </div>
    );
  }
}
export default TopBarCompo;

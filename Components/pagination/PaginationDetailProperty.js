import React, { useEffect, useState } from 'react';
import { Carousel } from '@mantine/carousel';
import { createStyles } from '@mantine/core';
import { VscChromeClose } from 'react-icons/vsc';
import Image from 'next/image';

export default function PaginationDetailProperty({
  image,
  actionImage,
  index,
}) {
  const useStyles = createStyles((theme) => ({
    control: {
      backgroundColor: '#F2EEC8',
      borderColor: '#F2EEC8',
      color: '#F56600',
      opacity: 0.5,
    },
    carouselSlide: {
      overflow: 'hidden',
      cursor: 'pointer',
    },
    image: {
      position: 'absolute',
      objectFit: 'cover',
      width: '100%',
      height: '100%',
      top: -9999,
      bottom: -9999,
      left: -9999,
      right: -9999,
      margin: 'auto',
    },
    containerGallery: {
      transitionTimingFunction: 'ease-out',
      transition: '1s!important',
    },
    buttonExit: {
      position: 'fixed',
      top: '80px',
      right: '50px',
      border: 'none',
      background: 'none',
      zIndex: 10,
      backgroundColor: '#F2EEC8',
      borderColor: '#F2EEC8',
      color: '#F56600',
      borderRadius: '100%',
      padding: '10px',
      '@media (max-width: 768px)': {
        top: '100px',
      },
      '@media (max-width: 600px) and (max-height: 500px)': {
        top: '80px',
      },
      '@media (max-width: 600px) and (max-height: 500px)': {
        top: '120px',
        right: '20px',
      },
      '@media (max-width: 400px) and (max-height: 350px)': {
        top: '120px',
        right: '10px',
      },
    },
    imageCarousel: {
      width: 'fit-content',
      maxWidth: '100%',
      maxHeight: '100%',
      borderRadius: '10px',
      margin: 'auto',
    },
  }));
  const { classes } = useStyles();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const images = [];

  useEffect(() => {
    images.push(image[index]);
    for (let i = 0; i < image.length; i++) {
      if (i !== index) {
        images.push(image[i]);
      }
    }
  }, [image, images, index]);
  return image.length > 0 ? (
    <>
      <button
        className={classes.buttonExit}
        onClick={() => {
          actionImage();
        }}
      >
        <VscChromeClose color="orange" size={15} />
      </button>
      <Carousel
        slideSize="100%"
        initialSlide={index}
        height={'100%'}
        slideGap="xs"
        controlsOffset="lg"
        controlSize={42}
        loop
        withIndicators
        draggable={false}
        classNames={{
          control: classes.control,
        }}
        align="center"
        style={{
          width: '100%',
          height: '95%',
          backgroundColor: 'rgba(0, 0, 0, 0.8)',
          position: 'fixed',
          zIndex: 3,
          display: 'flex',
          transition: '0.5s',
          control: {
            '&[data-inactive]': {
              opacity: 0,
              cursor: 'default',
            },
          },
        }}
      >
        {image?.map((item, key) => {
          return (
            <Carousel.Slide key={key}>
              <Image
                src={item}
                alt="image carousel"
                className={classes.imageCarousel}
                style={{
                  display: 'flex',
                  height: '100%',
                  objectFit: 'contain',
                }}
                width={1000}
                height={1000}
              />
            </Carousel.Slide>
          );
        })}
      </Carousel>
    </>
  ) : null;
}

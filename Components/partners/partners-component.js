import Aos from 'aos';
import 'aos/dist/aos.css';
import { useRef, useState } from 'react';
import { Container, createStyles } from '@mantine/core';
import Autoplay from 'embla-carousel-autoplay';
import { useEffect } from 'react';
import styleCss from './partners.module.css';
import Image from 'next/image';

const dataImage = [
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_Asa_Dreamland.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_Baratan_Residence.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_Diamond_City-01.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/Jember_Pesona_Wirolegi.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_Sumber_Jeruk_Regency-01.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_Taman_Gading.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/SITUBONDO_Puncak_Dieng.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/LUMAJANG_Permata Grati_Garden.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/1JEMBER_VBI_&_RHR.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/1LUMAJANG_Cluster_Hasanan_Park.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_Graha_Kusuma.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_GWK-01.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_Puri_Tegal_Gede.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/BANYUWANGI_Banyuwangi Residence.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/BANYUWANGI_Green Garden 1 & 2.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/BONDOWOSO_Green View-01.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/BONDOWOSO_Wijaya Kusuma-01.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_Amany Residence 2.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_Bentuyun Garden.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_Casa Grande.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_Cendrawasih Green Residence.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_GPI-01.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_KelapaGading.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_Green-Pesona-Patrang.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_ITB.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_JETOS.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_Nadzira_Residence.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_Puri_Kartika_Tiga-01.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_River_Side_Garden.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/JEMBER_Sadana_Land.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/LUMAJANG_Grand_Arya_Wiraja.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Ahsana.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Azvina.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Banyuwangi-Residence.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Cakra.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/De-Aesthetic-Cluster.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/de-grand-land.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Ganesha.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Graha-Artha.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Graha-Kutorejo.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Grand-Andara.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Grand-Mutiara.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Griya-Arjowinangun-Permain.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Griya-Madureso.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Griya-Sooko-Asri.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Harmoni.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Puncak-Arjuna.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Puri-Buana-Asri.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Rezanna-Garden.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Royal-Singosari.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Shafa.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/SinghaJaya2.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/Star-Garden-Residence.png'),
  },
  {
    image: require('../../pages/assets/images/logo/Redisign/wilayah2/The-Valley-Cluster.png'),
  },
];

const useStyle = createStyles((theme) => ({
  container1: {
    textAlign: '-webkit-center',
    '@media (min-width: 1200px) and (min-height: 700px)': {
      marginBottom: '0!important',
    },
  },
  container: {
    // width: "auto",
    height: 100,
    width: 100,
    padding: 0,
    '@media (max-width: 600px)': {
      height: 90,
    },
    '@media (min-width: 600px)': {
      height: 89,
    },
    '@media (min-width: 1200px) and (min-height: 500px)': {
      height: 90,
    },
    '@media (min-width: 1200px) and (min-height: 600px)': {
      height: 90,
    },
  },
  row: {
    justifyContent: 'center',
  },
  image: {
    width: '9vw',
    height: '9vh',
    objectFit: 'contain',
    objectPosition: 'center',
    '@media (max-width: 768px)': {
      width: '10vw',
      height: 'auto',
      aspectRatio: '1/1',
    },
    '@media (max-width: 600px)': {
      width: '15vw',
      height: 'auto',
    },
  },
  carouselPartners1: {
    '@media (min-width: 1200px) and (min-height: 500px)': {
      height: 100,
    },
    '@media (min-width: 1200px) and (min-height: 600px)': {
      height: 200,
    },
    '@media (min-width: 1200px) and (min-height: 700px)': {
      marginBottom: 0,
    },
    '@media (max-width: 600px)': {
      height: 100,
    },
  },
}));

const PartnersCompo = (props) => {
  const [image, setImage] = useState(dataImage);
  const { classes } = useStyle();
  useEffect(() => {
    Aos.init({
      duration: 300,
      easing: 'ease-in-sine',
      offset: 200,
      delay: 100,
    });
  }, []);
  return (
    <div className={`container mb-4 ${classes.container1}`}>
      <div className={`row ${classes.row}`}>
        {image.map((item, index) => (
          <div
            className="col-6 col-sm-4 col-md-4 col-lg-3 col-xl-2"
            key={index}
          >
            <Container
              className={`${classes.container} ${styleCss.containerIcon}`}
            >
              <Image
                src={item.image}
                alt=""
                className={`${classes.image} ${styleCss.imagePartner}`}
                style={{
                  filter: 'grayscale(100%)',
                  opacity: 0.7,
                }}
                onMouseEnter={(e) => (e.target.style.filter = 'grayscale(0%)')}
                onMouseLeave={(e) =>
                  (e.target.style.filter = 'grayscale(100%)')
                }
                onTouchStart={(e) => (e.target.style.filter = 'grayscale(0%)')}
                onTouchEnd={(e) => (e.target.style.filter = 'grayscale(100%)')}
              />
            </Container>
          </div>
        ))}
      </div>
    </div>
  );
};

export default PartnersCompo;

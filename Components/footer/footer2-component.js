// import React from "react";
// import style from "./footer2CompoStyle.module.css";
// import youtube from "../../pages/assets/icons/sosial media/youtube-icon.svg";
// import instagram from "../../pages/assets/icons/sosial media/instagram-icon.svg";
// import facebook from "../../pages/assets/icons/sosial media/facebook-icon.svg";
// import tiktok from "../../pages/assets/icons/sosial media/tiktok-icon.svg";
// import playstore from "../../pages/assets/images/Button-Download-Playstore-Large.webp";
// export const Footer2Component = () => {
//     return (
//         <>
//             <footer id="kontak" className={`${style.footer}`}>
//                 <div className={`${style.container} container text-center`}>
//                     <div className={`${style.awal} row awal text-center mb-0`}>
//                         <div className={`${style["icon-logo"]} col icon-logo`}>
//                             <p>&copy;2022 Copyright Sepakat</p>
//                             <a
//                                 href="https://www.youtube.com/channel/UChSicdrwHc5api6jWRCICCQ"
//                                 target="_blank"
//                                 rel="noreferrer"
//                                 className="me-3"
//                             >
//                                 <img src={youtube} alt="" />
//                             </a>
//                             <a
//                                 href="https://www.instagram.com/sepakat.properti"
//                                 target="_blank"
//                                 rel="noreferrer"
//                                 className="me-3"
//                             >
//                                 <img src={instagram} alt="" />
//                             </a>
//                             <a
//                                 href="https://www.tiktok.com/@sepakat.properti"
//                                 target="_blank"
//                                 rel="noreferrer"
//                                 className="me-3"
//                             >
//                                 <img src={tiktok} alt="" />
//                             </a>
//                             <a
//                                 href="https://www.facebook.com/sepakat.properti"
//                                 target="_blank"
//                                 rel="noreferrer"
//                             >
//                                 <img src={facebook} alt="" />
//                             </a>
//                             <a
//                                 href="https://play.google.com/store/apps/details?id=id.asc.sepakat"
//                                 className={`${style["anchor-playstore"]}`}
//                             >
//                                 <img
//                                     src={playstore}
//                                     alt=""
//                                     width={150}
//                                     className={`${style["logo-playstore"]} logo-playstore`}
//                                 />
//                             </a>
//                         </div>
//                     </div>
//                 </div>
//             </footer>
//         </>
//     );
// };

import React from "react";
import { Box, Text } from "@chakra-ui/react";
import Image from "next/image";
import Link from "next/link";
import youtube from "../../pages/assets/icons/sosial media/youtube-icon.svg";
import instagram from "../../pages/assets/icons/sosial media/instagram-icon.svg";
import facebook from "../../pages/assets/icons/sosial media/facebook-icon.svg";
import tiktok from "../../pages/assets/icons/sosial media/tiktok-icon.svg";
import playstore from "../../pages/assets/images/Button-Download-Playstore-Large.svg";

export default function footer2component() {
    return (
        <Box bgColor={"#f56600"} color={"white"}>
            <Box
                display={"flex"}
                flexDir={{ base: "column", lg: "row" }}
                justifyContent={"space-between"}
                alignItems={"center"}
                maxW={{ base: "90%", md: "70%" }}
                mx={"auto"}
                py={10}
                gap={5}
            >
                <Box>
                    <Text fontWeight={"bold"} fontSize={"lg"}>
                        ©2022 Copyright Sepakat
                    </Text>
                </Box>
                <Box
                    display={"flex"}
                    alignItems={"center"}
                    gap={4}
                    flexDir={{ base: "column", sm: "row" }}
                >
                    <Box display={"flex"} alignItems={"center"} gap={4}>
                        <Link
                            href="https://www.youtube.com/channel/UChSicdrwHc5api6jWRCICCQ"
                            target={"_blank"}
                        >
                            <Image
                                src={youtube}
                                alt="youtube"
                                width={40}
                                height={40}
                            />
                        </Link>
                        <Link
                            href="https://www.instagram.com/sepakat.properti"
                            target={"_blank"}
                        >
                            <Image
                                src={instagram}
                                alt="instagram"
                                width={30}
                                height={30}
                            />
                        </Link>
                        <Link
                            href="https://www.tiktok.com/@sepakat.properti"
                            target={"_blank"}
                        >
                            <Image
                                src={tiktok}
                                alt="tiktok"
                                width={30}
                                height={30}
                            />
                        </Link>
                        <Link
                            href="https://www.facebook.com/sepakat.properti"
                            target={"_blank"}
                        >
                            <Image
                                src={facebook}
                                alt="facebook"
                                width={30}
                                height={30}
                            />
                        </Link>
                    </Box>
                    <Link
                        href="https://play.google.com/store/apps/details?id=id.asc.sepakat"
                        target={"_blank"}
                    >
                        <Image src={playstore} alt="playstore" height={50} />
                    </Link>
                </Box>
            </Box>
        </Box>
    );
}

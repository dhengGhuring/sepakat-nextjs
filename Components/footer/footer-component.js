import React, { useState } from 'react';
import Link from 'next/link';
import Alert from '../alert/Alert';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import Image from 'next/image';

// ** Import Style
import style from './footerCompoStyle.module.css';

// ** Import Icons
import emailIcon from '../../pages/assets/icons/sosial media/email-icon.svg';
import whatsappIcon from '../../pages/assets/icons/sosial media/whatsapp-icon.svg';
import youtube from '../../pages/assets/icons/sosial media/youtube-icon.svg';
import instagram from '../../pages/assets/icons/sosial media/instagram-icon.svg';
import facebook from '../../pages/assets/icons/sosial media/facebook-icon.svg';
import tiktok from '../../pages/assets/icons/sosial media/tiktok-icon.svg';
import { MdContentCopy } from 'react-icons/md';
import Sepakat from '../../pages/assets/images/logoSepakat.svg';

const FooterCompo = () => {
  const [opened, setOpened] = useState(false);
  const [openedComingSoon, setOpenedComingSoon] = useState(false);

  return (
    <div>
      <Alert
        opened={opened}
        setOpened={setOpened}
        image={Sepakat}
        heading={'Sepakat'}
        description={'Halaman belum tersedia'}
      />
      <Alert
        opened={openedComingSoon}
        setOpened={setOpenedComingSoon}
        image={Sepakat}
        heading={'Sepakat'}
        description={'Segera Hadir'}
      />
      <footer className={`${style.kontak} ${style.footer}`}>
        <div className={`${style['container-fluid']} container-fluid`}>
          <div className={`${style.awal} row  mb-0 mx-md-5 mx-1`}>
            <div className={`col-lg-3 ${style.column1} `}>
              <div className={`${style['icon-logo']} col`}>
                <div
                  className={`${style['container-icon']} align-items-center`}
                >
                  <a
                    href="https://www.youtube.com/channel/UChSicdrwHc5api6jWRCICCQ"
                    className="me-3"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Image
                      src={youtube}
                      alt=""
                      className={`${style['icon-footer']}`}
                    />
                  </a>
                  <a
                    href="https://www.instagram.com/sepakat.properti/"
                    className="me-3"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Image
                      src={instagram}
                      alt=""
                      className={`${style['icon-footer']}`}
                    />
                  </a>
                  <a
                    href="https://www.tiktok.com/@sepakat.properti"
                    className="me-3"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Image
                      src={tiktok}
                      alt=""
                      className={`${style['icon-footer']}`}
                    />
                  </a>
                  <a
                    href="https://www.facebook.com/sepakat.properti"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Image
                      src={facebook}
                      alt=""
                      className={`${style['icon-footer']}`}
                    />
                  </a>
                </div>
                <div className="row mt-4">
                  <div className={`col d-flex flex-row ${style['icon-logo2']}`}>
                    <Image
                      src={require('../../pages/assets/images/Section-Footer-Logo Putih 1.png')}
                      alt=""
                      className="me-3"
                    />
                    <h1 className={`${style.logoFooter} m-0`}>Sepakat</h1>
                  </div>
                  <div
                    className={`${style.containerAddress} ${style['kolom-2']}  d-flex flex-row`}
                  >
                    <div className={`${style['klm-2']}col d-flex mt-2`}>
                      <p
                        className={`me-3 text-start ${style.address}`}
                        style={{ color: '#fff', fontWeight: 700 }}
                      >
                        Ruko Java Square Kavling 15, Jalan Jawa, Sumbersari,
                        Kabupaten Jember.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              className={`col mx-auto text-start ${style.columnLeft}`}
              style={{ fontWeight: 700 }}
            >
              <Link href="/syarat-ketentuan">
                <p className="mb-3">Syarat & Ketentuan</p>
              </Link>
              <a
                href="https://docs.google.com/forms/d/e/1FAIpQLScGW8JuTWLuq9A3SW6s83GB3OS5tuntuqsEk9NUjk9wM0uLgA/viewform?usp=sf_link"
                target="_blank"
                rel="noreferrer"
              >
                <p className="mb-3">Menjadi Agen</p>
              </a>
              <Link href="/konten-video">
                <p className="mb-3">Edukasi Properti</p>
              </Link>
              <div
                onClick={() => {
                  setOpened(true);
                }}
                style={{ cursor: 'pointer' }}
              >
                <p style={{ color: '#fff' }}>Karir</p>
              </div>
            </div>
            <div
              className={`col mx-auto text-start ${style.column2}`}
              style={{ fontWeight: 700 }}
            >
              <a
                href="https://docs.google.com/forms/d/e/1FAIpQLSfBkuEsfLr4PWUHX8CrFS-zxX0s2Iby_cTccA5NN6umFtgqIg/viewform?usp=sf_link"
                target="_blank"
                rel="noreferrer"
              >
                <p className="mb-3">Kritik & Saran</p>
              </a>
              <a
                href="https://api.whatsapp.com/send/?phone=6281335258080&text=Halo,+saya+mengalami+kendala+dengan+aplikasi+sepakat.+Apakah+bisa+dibantu?&type=phone_number&app_absent=0"
                target="_blank"
                rel="noreferrer"
              >
                <p className="mb-3">Bantuan Pengguna</p>
              </a>

              <Link href="/kebijakan-privasi">
                <p className="mb-3">Kebijakan dan Privasi</p>
              </Link>
              <div
                onClick={() => {
                  setOpened(true);
                }}
                style={{ cursor: 'pointer' }}
              >
                <p style={{ color: '#fff' }}>Hak Cipta</p>
              </div>
            </div>
            <div
              className={`col mx-auto text-start ${style.column3}`}
              style={{ fontWeight: 700 }}
            >
              <div
                onClick={() => {
                  setOpenedComingSoon(true);
                }}
                style={{ cursor: 'pointer' }}
              >
                <p style={{ color: '#fff' }} className={'mb-3'}>
                  Kalkulator KPR
                </p>
              </div>
              <a
                href="https://api.whatsapp.com/send/?phone=6281335258080&text=Halo,+saya+ingin+mengetahui+lebih+lanjut+perihal+sepakat,+apakah+bisa+dibantu?&type=phone_number&app_absent=0"
                target="_blank"
                rel="noreferrer"
              >
                <p className="mb-3">Customer Service</p>
              </a>
            </div>
            <div className={`${style.contact} col mx-auto text-start`}>
              <h1>Informasi lebih lanjut</h1>
              <div
                className={`${style.container} ${style['kolom-1']}  d-flex flex-row justify-content-space-between pe-3`}
              >
                <div
                  className={`${style['klm-1']} col d-flex mt-2 copy-text align-items-center mb-2`}
                  data-clipboard-target="#foo"
                >
                  <Image
                    className="mt-1 ms-3 logoEmail"
                    src={emailIcon}
                    alt=""
                    width="15px !important"
                    height="15px !important"
                    style={{ maxWidth: 'inherit' }}
                  />
                  <p
                    className={`ms-2 me-3 email ${style.email}`}
                    id="foo"
                    style={{ fontWeight: 700 }}
                  >
                    ptsepakatmenjalaberkat@gmail.com
                  </p>
                </div>
                <CopyToClipboard text="ptsepakatmenjalaberkat@gmail.com">
                  <button className={`${style['button-copy']}`}>
                    <MdContentCopy color="#000" />
                  </button>
                </CopyToClipboard>
              </div>
              <div
                className={`${style.container} ${style['kolom-2']}  d-flex flex-row mb-3 justify-content-space-between pe-3`}
              >
                <Link
                  href="https://api.whatsapp.com/send/?phone=6281335258080&text&type=phone_number&app_absent=0"
                  target="_blank"
                >
                  <div
                    className={`${style['klm-2']}col d-flex mt-2 copy-text align-items-center`}
                  >
                    <Image
                      className="ms-3"
                      src={whatsappIcon}
                      alt=""
                      width="15px"
                      height="15px"
                    />
                    <p
                      className={`ms-2 me-3 ${style.number}`}
                      style={{ color: '#4D4D4D', fontWeight: 700 }}
                      id="myInput"
                    >
                      0813 3525 8080
                    </p>
                  </div>
                </Link>
              </div>
            </div>
          </div>
          <div className={`${style.barBottom} row`}>
            <div
              className={`${style.bottomBar} col mx-auto text-start bg-success`}
            >
              <h1 className={`text-start ${style.copyRightText}`}>
                2022 Copyright Sepakat
              </h1>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
};

export default FooterCompo;

import { useEffect, useRef } from 'react';

// ** Import Library
import { Tooltip } from '@mantine/core';
import Aos from 'aos';
import 'aos/dist/aos.css';
import Image from 'next/image';

// ** Import Style
import style from './jumbotron2CompoStyle.module.css';

// ** Import Image
import playstore from '../../pages/assets/images/Section-Footer-Button-Download-Playstore-Large.svg';
import appstore from '../../pages/assets/images/Section-Footer-Button-Download-Appstore-Large.svg';
import phone from '../../pages/assets/images/phone.svg';
import { motion } from 'framer-motion';

const JumboTron2 = () => {
  useEffect(() => {
    Aos.init({
      duration: 300,
      easing: 'ease-in-sine',
      offset: 200,
      delay: 100,
    });
  }, []);
  const ref = useRef(null);
  return (
    <div
      className={`${style['jumbotron-2']} jumbotron-fluid mb-0`}
      style={{ padding: '10% 0' }}
    >
      <div className={`row ${style.rows}`}>
        <motion.div
          className={`${style.heading} col-lg-6 col-md-6 `}
          initial={{ x: -100 }}
          whileInView={{ x: 0 }}
          viewport={{ root: ref }}
          transition={{ duration: '0.2' }}
        >
          <h1 className="my-2">
            Download Aplikasi <br />
            Sepakat
          </h1>
          <p className="lead-1 my-2">#MudahCariProperti</p>
          <div className={`${style.logoArea} d-flex flex-row`}>
            <a
              href="https://play.google.com/store/apps/details?id=id.asc.sepakat"
              target="_blank"
              rel="noreferrer"
            >
              <Image
                src={playstore}
                alt="logo play store"
                className={style.playstore}
              />
            </a>
            <div>
              <Tooltip
                label="Segera Hadir"
                color="green.9"
                position="right"
                withArrow
              >
                <Image
                  src={appstore}
                  alt="logo app store"
                  className={style.appstore}
                />
              </Tooltip>
            </div>
          </div>
        </motion.div>
        <motion.div
          className={`${style.logoHp} col-lg col-md-6 kolom2 d-flex flex-row`}
          initial={{ x: 100 }}
          whileInView={{ x: 0 }}
          viewport={{ root: ref }}
          transition={{ duration: '0.2' }}
        >
          <span className={`${style.hp1} d-flex justify-content-center`}>
            <Image src={phone} alt="" className="img-fluid w-lg-100 w-sm-50" />
          </span>
        </motion.div>
      </div>
    </div>
  );
};

export default JumboTron2;

import Image from 'next/image';
import React from 'react';
import Logo from '../../pages/assets/images/icon-logo.svg';
import style from './loadingAnimation.css';

export default function loadingAnimation() {
  return (
    <div className={`${style.containerPreloader}`}>
      <Image
        src={Logo}
        className={`${style.loadingPreloader}`}
        style={{ width: '10%' }}
        alt=""
      />
    </div>
  );
}

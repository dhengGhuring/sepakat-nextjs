import { Modal, Text, Box, Tooltip } from "@mantine/core";
import playstore from "../../pages/assets/images/Button-Download-Playstore-Large.svg";
import appstore from "../../pages/assets/images/Button-Download-Appstore-Large.svg";
import Image from "next/image";

export default function Alert(props) {
    return (
        <Modal
            opened={props.opened}
            onClose={() => props.setOpened(false)}
            withCloseButton={false}
            centered
        >
            <Box
                sx={(theme) => ({
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                })}
            >
                <Box
                    sx={() => ({
                        width: props.heading == "Sepakat" ? "50%" : "20%",
                    })}
                >
                    <Image
                        src={props.image}
                        alt="Logo"
                        width={props.heading == "Sepakat" ? "60" : "80"}
                        style={{
                            display: "block",
                            marginLeft: "auto",
                            marginRight: "auto",
                        }}
                    />
                </Box>
                <Text
                    weight={700}
                    size={"2rem"}
                    sx={() => ({
                        marginTop: 30,
                    })}
                >
                    {props.heading}
                </Text>
                <Text>{props.description}</Text>
                <Box
                    sx={() => ({
                        display: "flex",
                        gap: 30,
                        maxWidth: "70%",
                        marginTop: 30,
                        marginBottom: 30,
                    })}
                >
                    {props.heading == "Sepakat" ? (
                        <a
                            href="https://play.google.com/store/apps/details?id=id.asc.sepakat"
                            target="_blank"
                        >
                            <Image
                                src={playstore}
                                alt={"logo playstore"}
                                width={120}
                            />
                        </a>
                    ) : (
                        <Tooltip
                            label="Coming Soon"
                            position="bottom"
                            color={"green.9"}
                            arrowStyle={{ backgroundColor: "#000" }}
                        >
                            <Image
                                src={playstore}
                                alt={"logo playstore"}
                                width={120}
                            />
                        </Tooltip>
                    )}
                    <Tooltip
                        label="Coming Soon"
                        position="bottom"
                        color={"green.9"}
                        arrowStyle={{ backgroundColor: "#000" }}
                    >
                        <Image
                            src={appstore}
                            alt={"logo playstore"}
                            width={120}
                        />
                    </Tooltip>
                </Box>
            </Box>
        </Modal>
    );
}

import React, { useRef } from "react";
import Image from "next/image";

// ** Import Style
import style from "./gallerysepakatCompoStyle.module.css";

import gmb1 from "../../pages/assets/images/aktivitas sepakat/gmb1Resize.webp";
import gmb2 from "../../pages/assets/images/aktivitas sepakat/gmb2Resize.webp";
import gmb3 from "../../pages/assets/images/aktivitas sepakat/gmb3Resize.webp";
import gmb4 from "../../pages/assets/images/aktivitas sepakat/gmb4Resize.webp";

import "aos/dist/aos.css";

const GallerySepakat = () => {
    const ref = useRef(null);
    return (
        <>
            <div className={style.dekstopView}>
                <div className={`row px-5 ${style.row}`}>
                    <div className="col-xs-12 col-lg-12">
                        <div
                            className={`${style["content-wrapper"]} d-flex align-items-center`}
                        >
                            <div
                                className={style["img-wrapper-left"]}
                                data-aos="zoom-in"
                                data-aos-duration="200"
                            >
                                <Image
                                    src={gmb1}
                                    alt=""
                                    className={`img-fluid rounded-5 ${style.img}`}
                                    width="40%"
                                />
                            </div>
                            <div
                                className={`${style["text-wrapper"]} d-flex flex-column justify-content-center ms-3`}
                                data-aos="fade-right"
                                data-aos-duration="450"
                            >
                                <p className={` ${style.title}`}>
                                    Pelatihan Agen Profesional Berkelanjutan
                                </p>
                                <p className={` ${style.description}`}>
                                    Pelatihan ini diadakan untuk membangun
                                    karakter, kredibilitas dan kemampuan agen di
                                    bidang properti sehingga mereka menjadi agen
                                    properti professional. Pembekalan kepada
                                    agen mulai dari melakukan listing,
                                    mengutamakan pelayanan kepada klien,
                                    aktivitas pemasaran, melakukan transaksi,
                                    hingga pelayanan purna jual.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12 col-lg-12">
                        <div
                            className={`${style["content-wrapper"]} d-flex align-items-center float-end`}
                        >
                            <div
                                className={`${style["text-wrapper-left"]} d-flex flex-column justify-content-center me-3`}
                                data-aos="fade-left"
                                data-aos-duration="450"
                            >
                                <p className={` ${style.title}`}>
                                    Edukasi Seputar Properti
                                </p>
                                <p className={` ${style.description}`}>
                                    Komitmen kami untuk berbagi pengetahuan
                                    tentang properti dengan konten kekinian
                                    berupa artikel, tips, foto dan video edukasi
                                    yang berkaitan dengan dunia properti dan
                                    sejenisnya.
                                </p>
                            </div>
                            <div
                                className={style["img-wrapper"]}
                                data-aos="zoom-in"
                                data-aos-duration="200"
                            >
                                <Image
                                    src={gmb2}
                                    alt=""
                                    className={`img-fluid rounded-5 ${style.img}`}
                                    width="40%"
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12 col-lg-12">
                        <div
                            className={`${style["content-wrapper"]} d-flex align-items-center`}
                        >
                            <div
                                className={style["img-wrapper-left"]}
                                data-aos="zoom-in"
                                data-aos-duration="200"
                            >
                                <Image
                                    src={gmb3}
                                    alt=""
                                    className={`img-fluid rounded-5 ${style.img}`}
                                    width="40%"
                                />
                            </div>
                            <div
                                className={`${style["text-wrapper"]} d-flex flex-column justify-content-center ms-3`}
                                data-aos="fade-right"
                                data-aos-duration="450"
                            >
                                <p className={` ${style.title}`}>
                                    Kegiatan Pemasaran
                                </p>
                                <p className={` ${style.description}`}>
                                    Menyebarkan brosur perumahan di berbagai
                                    instansi, kegiatan event, pusat kegiatan
                                    masyarakat serta melakukan pendekatan dan
                                    edukasi kepada calon pembeli.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12 col-lg-12">
                        <div className="content-wrapper d-flex align-items-center float-end">
                            <div
                                className={`${style["text-wrapper-left"]} d-flex flex-column justify-content-center me-3`}
                                data-aos="fade-left"
                                data-aos-duration="450"
                            >
                                <p className={` ${style.title}`}>
                                    Komunikasi dan Berbagi Pengalaman Para Agen
                                </p>
                                <p className={` ${style.description}`}>
                                    Sharing dan kolaborasi antara agen untuk
                                    berbagi pengetahuan dan pengalaman di
                                    lapangan, juga berbagi cara mengenalkan
                                    Sepakat kepada masyarakat baik tentang
                                    identitas perusahaan, produk, serta
                                    pelayanan yang diberikan.
                                </p>
                            </div>
                            <div
                                className={style["img-wrapper"]}
                                data-aos="zoom-in"
                                data-aos-duration="200"
                            >
                                <Image
                                    src={gmb4}
                                    alt=""
                                    className={`img-fluid rounded-5 ${style.img}`}
                                    width="40%"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* Mobile VIew */}
            <div className={style.mobileView}>
                <div className={`row px-5 ${style.row}`}>
                    <div className="col-xs-12 col-lg-12 mb-3">
                        <div
                            className={`${style["content-wrapper"]} d-flex align-items-center`}
                            data-aos="fade-right"
                            data-aos-duration="400"
                        >
                            <div
                                className={`${style["text-wrapper"]} d-flex flex-column justify-content-center ms-3`}
                            >
                                <p className={` ${style.title}`}>
                                    Pelatihan Agen Profesional Berkelanjutan
                                </p>
                                <p className={` ${style.description}`}>
                                    Pelatihan ini diadakan untuk membangun
                                    karakter, kredibilitas dan kemampuan agen di
                                    bidang properti sehingga mereka menjadi agen
                                    properti professional. Pembekalan kepada
                                    agen mulai dari melakukan listing,
                                    mengutamakan pelayanan kepada klien,
                                    aktivitas pemasaran, melakukan transaksi,
                                    hingga pelayanan purna jual.
                                </p>
                            </div>
                            <div className={style["img-wrapper"]}>
                                <Image
                                    src={gmb1}
                                    alt=""
                                    className={`img-fluid rounded-5 ${style.img}`}
                                    width="40%"
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12 col-lg-12 mb-3">
                        <div
                            className={`${style["content-wrapper"]} d-flex align-items-center float-start`}
                            data-aos="fade-left"
                            data-aos-duration="400"
                        >
                            <div
                                className={`${style["text-wrapper"]} d-flex flex-column justify-content-center me-3`}
                            >
                                <p className={` ${style.title}`}>
                                    Edukasi Seputar Properti
                                </p>
                                <p className={` ${style.description}`}>
                                    Komitmen kami untuk berbagi pengetahuan
                                    tentang properti dengan konten kekinian
                                    berupa artikel, tips, foto dan video edukasi
                                    yang berkaitan dengan dunia properti dan
                                    sejenisnya.
                                </p>
                            </div>
                            <div className={style["img-wrapper"]}>
                                <Image
                                    src={gmb2}
                                    alt=""
                                    className={`img-fluid rounded-5 ${style.img}`}
                                    width="40%"
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12 col-lg-12 mb-3">
                        <div
                            className={`${style["content-wrapper"]} d-flex align-items-center`}
                            data-aos="fade-right"
                            data-aos-duration="400"
                        >
                            <div
                                className={`${style["text-wrapper"]} d-flex flex-column justify-content-center ms-3`}
                            >
                                <p className={` ${style.title}`}>
                                    Kegiatan Pemasaran
                                </p>
                                <p className={` ${style.description}`}>
                                    Menyebarkan brosur perumahan di berbagai
                                    instansi, kegiatan event, pusat kegiatan
                                    masyarakat serta melakukan pendekatan dan
                                    edukasi kepada calon pembeli.
                                </p>
                            </div>
                            <div className={style["img-wrapper"]}>
                                <Image
                                    src={gmb3}
                                    alt=""
                                    className={`img-fluid rounded-5 ${style.img}`}
                                    width="40%"
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12 col-lg-12 mb-3">
                        <div
                            className={`${style["content-wrapper"]} d-flex align-items-center`}
                            data-aos="fade-left"
                            data-aos-duration="400"
                        >
                            <div
                                className={`${style["text-wrapper"]} d-flex flex-column justify-content-center ms-3 `}
                            >
                                <p className={` ${style.title}`}>
                                    Komunikasi dan Berbagi Pengalaman Para Agen
                                </p>
                                <p className={` ${style.description}`}>
                                    Sharing dan kolaborasi antara agen untuk
                                    berbagi pengetahuan dan pengalaman di
                                    lapangan, juga berbagi cara mengenalkan
                                    Sepakat kepada masyarakat baik tentang
                                    identitas perusahaan, produk, serta
                                    pelayanan yang diberikan.
                                </p>
                            </div>
                            <div className={style["img-wrapper"]}>
                                <Image
                                    src={gmb4}
                                    alt=""
                                    className={`img-fluid rounded-5 ${style.img}`}
                                    width="40%"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* End Mobile View */}
        </>
    );
};

export default GallerySepakat;

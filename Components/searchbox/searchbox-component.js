import { createStyles } from "@mantine/core";
import Link from "next/link";
import style from "./searchboxCompoStyle.module.css";
import { useState } from "react";
import MASTERCITIES from "../../data/sample_cities.json";
import Image from "next/image";
const useStyle = createStyles((theme) => ({
    wrapperSearchBox: {
        "@media (min-width: 600px)": {
            maxWidth: "320px",
        },
        "@media (min-width: 700px)": {
            maxWidth: "500px",
        },
        "@media (max-height: 500px)": {
            maxWidth: "345px",
        },
        "@media (min-height: 500px)": {
            maxWidth: "345px",
        },
        "@media (min-height: 600px)": {
            maxWidth: "385px",
        },
        "@media (min-height: 700px)": {
            maxWidth: "450px",
        },
    },
    form: {
        width: "100%!important",
        maxWidth: "100%",
        height: 40,
        borderRadius: 15,
        margin: "0!important",
        background: "rgba(245, 245, 245, 0.4)",
        backdropFilter: "blur(10px)",
        border: "2px solid rgba(245, 245, 245, 0.2) !important",
        "@media (min-width: 992px)": {
            height: 50,
        },
    },
}));
const SearchBoxCompo = () => {
    const { classes } = useStyle();
    const CITIESDATA = MASTERCITIES.sampleCitiesData;
    const [searchTerm, setSearchTerm] = useState("");
    return (
        <div className={classes.wrapperSearchBox}>
            <div className={style["input-container"]}>
                <Link href="/pencarian">
                    <form
                        autoComplete="off"
                        className={`${style.form} ${classes.form}`}
                    >
                        <div className="mb-3 d-flex flex-row">
                            <Image
                                src={require("../../pages/assets/icons/lensIcon.png")}
                                alt=""
                            />
                            <input
                                type="search"
                                className={`form-control ${style.boxPencarian}`}
                                placeholder="Cari Properti"
                                onChange={(event) => {
                                    setSearchTerm(event.target.value);
                                }}
                            />
                        </div>
                        {searchTerm.length !== 0 && (
                            <div>
                                {CITIESDATA.map((val, key) => {
                                    return (
                                        <div className="city" key={key}>
                                            <a
                                                className={style.dataItem}
                                                href="https://www.youtube.com/watch?v=tB8k-X-_yBE"
                                                target="_blank"
                                                rel="noreferrer"
                                            >
                                                <p>{val["name"]}</p>
                                            </a>
                                        </div>
                                    );
                                })}
                            </div>
                        )}
                    </form>
                </Link>
            </div>
        </div>
    );
};
export default SearchBoxCompo;

import React from 'react';
import { Button, createStyles, Title } from '@mantine/core';
const useStyle = createStyles((theme) => ({
  button: {
    background: '#f56600',
    color: '#fff',
    width: 'max-content',
  },
  title: {
    fontFamily: 'Nunito Sans, sans-serif',
    fontSize: 20,
  },
}));
function ResultSearchComponent(props) {
  const { classes } = useStyle();
  return (
    <>
      <div className="container">
        <Button className={classes.button} radius="xl">
          <Title className={classes.title}>{props.title}</Title>
        </Button>
      </div>
    </>
  );
}

export default ResultSearchComponent;

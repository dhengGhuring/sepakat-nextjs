import { Autocomplete, Center } from "@mantine/core";
import { AiOutlineSearch } from "react-icons/ai";
import React, { useState } from "react";
import samplesCities from "../../data/sample_cities.json";

const SearchBoxV2Component = (props) => {
    const { setSelectedCity } = props;
    const constanta = samplesCities.sampleCitiesData.map((e) => e.name);
    const sampleData = [
        "jember",
        "mojokerto",
        "malang",
        "probolinggo",
        "banyuwangi",
    ];
    const [data, setData] = useState(sampleData);

    function handleChnageCities(text) {
        const newData =
            text.trim().length <= 0
                ? sampleData
                : constanta.filter((e) => e.includes(text));
        setData(newData);
        setSelectedCity(text.trim().length <= 0 ? null : text);
    }

    return (
        <>
            <Center>
                <Autocomplete
                    pl={27}
                    mt={45}
                    icon={<AiOutlineSearch color="#000" />}
                    placeholder="Cari Kota"
                    data={data}
                    size="sm"
                    sx={{
                        borderRadius: 20,
                        background: "white",
                        width: "60%",
                        padding: 5,
                        "@media (max-width: 600px)": {
                            width: "100%",
                            borderRadius: 20,
                        },
                        "@media (min-width: 600px)": {
                            width: "100%",
                        },
                        "@media (min-width: 1600px)": {
                            width: "60%",
                        },
                    }}
                    onChange={(text) => handleChnageCities(text)}
                    filter={(value, item) => {
                        return (
                            item.value
                                .toLowerCase()
                                .includes(value.toLowerCase().trim()) ||
                            item.description
                                .toLowerCase()
                                .includes(value.toLowerCase().trim())
                        );
                    }}
                    transition="pop-top-left"
                    transitionDuration={150}
                    transitionTimingFunction="ease"
                />
            </Center>
        </>
    );
};

export default SearchBoxV2Component;

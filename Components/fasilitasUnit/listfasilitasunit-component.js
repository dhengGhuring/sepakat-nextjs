import { Container, Group, Text, createStyles } from "@mantine/core";
import Image from "next/image";

const useStyle = createStyles((theme) => ({
  container: {
    backgroundColor: "#ffff",
    borderRadius: 50,
    maxWidth: 1000,
    width: "max-content",
    padding: "8px 16px 8px 16px",
    boxShadow: "0px 0px 5px rgba(77, 77, 77, 0.15)",
    fontFamily: "Nunito Sans, sans-serif",
    margin: 5,
  },
  title: {
    fontSize: 12,
    "@media (min-width: 600px)": {
      fontSize: 16,
    },
  },
}));

const ListFasilitasUnit = (props) => {
  const { classes } = useStyle();
  return (
    <div>
      <Container className={classes.container}>
        <Group align="center">
          <Image
            // src={require(`../../pages/assets/icons/sepakat icon/Tag-Facility-icon-${props.icon}.svg`)}
            src={require(`../../pages/assets/icons/sepakat icon/Tag-Facility-icon-${props.icon}.svg`)}
            width={15}
            height={15}
            alt="icon fasilitas"
            pt={8}
            pb={8}
          />
          <Text weight={600} className={classes.title}>
            {props.title}
          </Text>
        </Group>
      </Container>
    </div>
  );
};
export default ListFasilitasUnit;

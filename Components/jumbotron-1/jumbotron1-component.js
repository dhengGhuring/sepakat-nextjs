import { Autocomplete, Center } from "@mantine/core";
import { AiOutlineSearch } from "react-icons/ai";
// import { useNavigate } from "react-router-dom";
import { useRouter } from "next/router";
import capitalizeWord from "../../utility/capitalizeWord";
import style from "./jumbotron1Style.module.css";

const JumboTron1 = (props) => {
    const { city } = props;
    // const navigate = useNavigate();
    const router = useRouter();
    return (
        <div className={`${style.jumbotron} jumbotron-fluid`}>
            <div className={`${style["input-container"]} input-container`}>
                <Center>
                    <Autocomplete
                        pl={27}
                        mt={45}
                        icon={<AiOutlineSearch color="#000" />}
                        placeholder={capitalizeWord(city)}
                        data={[]}
                        size="xs"
                        sx={{
                            padding: 10,
                            borderRadius: 20,
                            background: "white",
                            "@media (max-width: 600px)": {
                                width: "90%",
                                borderRadius: 10,
                            },
                            "@media (min-width: 600px)": {
                                width: "70%",
                                borderRadius: 10,
                            },
                            "@media (min-width: 1200px)": {
                                width: "60%",
                                borderRadius: 10,
                            },
                        }}
                        transition="pop-top-left"
                        transitionDuration={150}
                        transitionTimingFunction="ease"
                        onClick={() => {
                            router.push("/pencarian");
                        }}
                    />
                </Center>
            </div>
        </div>
    );
};

export default JumboTron1;

import { Container, Title, Text, Group, createStyles } from '@mantine/core';
import Image from 'next/image';

import { bathroom, bedroom, carport, kitchen } from '../../pages/assets/';
const useStyle = createStyles((theme) => ({}));

const NameHouseCompo = (props) => {
  const { classes } = useStyle();
  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'IDR',
  });
  const { unitFacilities } = props;

  function priceHouse(price) {
    let format = formatter.format(price);
    format = format.substring(0, format.indexOf('.'));

    if (`${price}`.slice(-6) === '000000') {
      format = format.replaceAll(',', '');
      format = format.slice(0, -6);
      format += ` ${price >= 1e9 ? 'M' : 'JT'}`;
    }

    return format.replaceAll('IDR', 'Rp');
  }

  return (
    <Container className={classes.container} mt={40} mb={40}>
      <Title
        size={24}
        mb={10}
        style={{ fontFamily: 'Nunito Sans, sans-serif', fontWeight: 700 }}
      >
        Tipe {props.type}
      </Title>
      <Text
        size={20}
        mb={10}
        style={{ fontFamily: 'Nunito Sans, sans-serif', fontWeight: 400 }}
      >
        {props.name}
      </Text>
      <Group align="flex-end">
        <Text color="#F56600" mr={40} size={18} weight={400}>
          {priceHouse(props.price)}
        </Text>
        <Group spacing="xs" pb={3}>
          {unitFacilities.bathroom && (
            <Group align="flex-start" spacing="xs">
              <Image src={bathroom} width={15} alt="" />
              <Text size="xs">{unitFacilities.bathroom}</Text>
            </Group>
          )}
          {unitFacilities.bedroom && (
            <Group align="center" spacing="xs">
              <Image src={bedroom} width={15} alt="" />
              <Text size="xs">{unitFacilities.bedroom}</Text>
            </Group>
          )}
          {unitFacilities?.kitchen && (
            <Group align="center" spacing="xs">
              <Image src={kitchen} width={15} alt="" />
              <Text size="xs">Dapur</Text>
            </Group>
          )}
          {unitFacilities?.carport && (
            <Group align="center" spacing="xs">
              <Image src={carport} width={15} alt="" />
              <Text size="xs">{unitFacilities.carport}</Text>
            </Group>
          )}
        </Group>
      </Group>
    </Container>
  );
};
export default NameHouseCompo;

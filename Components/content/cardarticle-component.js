import {
    Card,
    Image,
    Text,
    Title,
    createStyles,
    BackgroundImage,
    Button,
    Tooltip,
} from "@mantine/core";
// import { Link } from "react-router-dom";
import Link from "next/link";
import style from "./cardarticleCompoStyle.module.css";
const useStyle = createStyles((theme) => ({
    btn_more: {
        position: "absolute",
        bottom: "30px",
    },
    card: {
        height: 450,
        width: "100%",
        overflow: "hidden",
        "@media (max-width: 600px)": {
            width: "100%",
            justifyContent: "center",
            height: 480,
        },
        "@media (min-width: 600px)": {
            height: 450,
        },
        "@media (min-width: 768px)": {
            height: 450,
            overflow: "hidden",
        },
    },
    backgroundImage: {
        "@media (max-width: 600px)": {
            height: 200,
        },
        "@media (min-width: 600px)": {
            height: 200,
        },
    },
    cardArticleWrapper: {
        marginBottom: 50,
        "@media (max-width: 600px)": {
            marginBottom: 30,
        },
    },
    title: {
        fontFamily: "Nunito Sans, sans-serif",
        fontSize: 18,
        fontWeight: 700,
        color: "#4D4D4D",
        paddingTop: 16,
        paddingLeft: 24,
        paddingRight: 24,
        paddingBottom: 10,
        overflow: "hidden",
        textOverflow: "ellipsis",
        maxLines: "2!important",
        whiteSpace: "nowrap",
        height: 60,
        "@media (max-width: 600px)": {},
    },
    text: {
        fontSize: 14,
        fontWeight: 400,
        color: "#4D4D4D",
        paddingLeft: 24,
        paddingRight: 24,
        paddingBottom: 24,
        overflow: "hidden",
        textOverflow: "ellipsis",
        maxLines: "3!important",
        height: 80,
        webkitBox: {
            webkitLineClamp: 2 /* number of lines to show */,
            lineClamp: 2,
            webkitBoxOrient: "vertical",
        },
        "@media (max-width: 600px)": {
            textOverflow: "ellipsis!important",
        },
    },
    stack: {
        gap: 6,
        paddingTop: 230,
        "@media (max-width: 600px)": {
            paddingTop: 120,
        },
        "@media (min-width: 600px)": {
            paddingTop: 120,
        },
    },
    containerIconFav: {
        height: 314,
    },
    detailFav: {
        color: "#FEFEFD",
        fontFamily: "Nunito Sans, sans-serif",
        fontSize: 20,
        fontWeight: 700,
    },
}));
const CardArticleCompo = (props) => {
    const { classes } = useStyle();
    return (
        <div className={classes.cardArticleWrapper}>
            <Card
                radius={20}
                shadow="lg"
                className={classes.card}
                sx={{ overflow: "hidden", textOverflow: "ellipsis" }}
            >
                <Card.Section>
                    <BackgroundImage
                        src={`"${props.image} "`}
                        radius={0}
                        className={classes.backgroundImage}
                    ></BackgroundImage>
                </Card.Section>
                <Tooltip
                    className={classes.tooltip}
                    label={props.title}
                    color="orange.6"
                >
                    <Title className={classes.title}>{props.title}</Title>
                </Tooltip>
                <Text className={`${classes.text} ${style.text}`}>
                    {props.article}
                </Text>
                <Link
                    href={{
                        pathname: "/detail-artikel",
                        query: { id: props.id },
                    }}
                >
                    <Button
                        ml={20}
                        color="orange.9"
                        radius="xl"
                        className={classes.btn_more}
                    >
                        Selengkapnya
                    </Button>
                </Link>
            </Card>
        </div>
    );
};

export default CardArticleCompo;

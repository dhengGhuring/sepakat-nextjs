import style from './video2CompoStyle.module.css';
import { useEffect, useRef, useState } from 'react';
import { createStyles } from '@mantine/core';
import Link from 'next/link';
import Image from 'next/image';
import Aos from 'aos';
import 'aos/dist/aos.css';
import { motion } from 'framer-motion';
const useStyle = createStyles((theme) => ({
  cards: {
    backgroundColor: 'transparent!important',
    border: 'none!important',
    width: 350,
    maxHeight: 625,
    overflow: 'hidden',
    borderRadius: '30px!important',
    boxShadow: '10px 10px 5px 0px rgb(0 0 0 / 10%)',
    paddingLeft: 0,
    '@media (max-width: 600px)': {
      padding: '0!important',
      maxHeight: 625,
    },
    '&:before': {
      padding: 0,
      margin: 0,
      content: '""',
      position: 'absolute',
      width: '100%',
      height: '100%',
      border: 'none',
      background:
        'linear-gradient(to bottom, rgba(255, 255, 255, 0.20), rgba(10, 10, 10, 0.50))',
      zIndex: 0,
      borderRadius: '30px',
      '@media (min-width: 1200px) and (max-height: 500px)': {
        width: '100%',
      },
    },
    '@media (min-width: 600px)': {
      width: 350,
    },
    '@media (min-width: 768px)': {
      width: 300,
    },
    '@media (min-width: 992px)': {
      width: 270,
    },
    '@media (min-width: 1200px)': {
      width: 230,
    },
    '@media (min-width: 1400px)': {
      width: 275,
    },
  },
  '@media (min-width: 600px)': {
    width: 350,
  },
  // '@media (min-width: 600px) and (min-height: 500px)': {
  //   width: 230,
  // },
  '@media (min-width: 768px)': {
    width: 300,
  },
  '@media (min-width: 992px)': {
    width: 270,
  },
  '@media (min-width: 1200px)': {
    width: 230,
  },
  '@media (min-width: 1400px)': {
    width: 275,
  },
  cardsDummy: {
    background: '#4FA483',
    height: 300,
    width: 500,
  },
  cio: {
    height: '100%',
    padding: '0!important',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'end',
  },
  textWrapper: {
    fontFamily: 'Nunito-Sans, sans-serif',
    padding: 16,
    '@media (max-width: 600px)': {},
    '@media (min-width: 600px)': {
      padding: '35px 16px',
    },
    '@media (min-width: 1200px)': {
      padding: '40px 16px',
    },
  },
  video: {
    borderRadius: 30,
    height: '100%',
    width: 'inherit !important',
    objectFit: 'cover',
    '@media (max-width: 600px)': {
      borderRadius: 30,
    },
  },
  cardTitle: {
    paddingLeft: '5%',
    fontWeight: 700,
    fontSize: 18,
  },
  iconTextWrapper: {
    display: 'flex',
    flexDirection: 'row',
    gap: '20%',
    alignItems: 'center',
    padding: '5%',
  },
  cardText: {
    width: 300,
    fontWeight: 400,
    fontSize: 16,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    maxLines: '3!important',
    height: 80,
    webkitBox: {
      webkitLineClamp: 2 /* number of lines to show */,
      lineClamp: 2,
      webkitBoxOrient: 'vertical',
    },
    '@media (max-width: 600px)': {
      textOverflow: 'ellipsis!important',
    },
  },
  iconText: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  iconTextImg: {
    width: 40,
    height: 38,
  },
  result: {
    fontFamily: 'Nunito Sans, sans-serif',
    fontSize: 20,
    fontWeight: 700,
    color: '#fefefd',
  },
  gridCol: {
    '@media (max-width: 600px)': {
      padding: 50.5,
      paddingBottom: 0,
    },
  },
  btnExpand: {
    '@media (max-width: 600px)': {
      marginTop: 100,
    },
  },
}));
const VideoCompo = (props) => {
  const { classes } = useStyle();
  const [activeElement, setActiveElement] = useState('');
  function play(video) {
    const vid = document.getElementById(video);
    if (vid.paused) {
      setActiveElement(style.active);
      vid.play();
    } else {
      setActiveElement(style['not-active']);
      vid.pause();
    }
  }
  const listElement = useRef(null);
  const ref = useRef(null);
  function apper() {
    listElement.current.classList.add(style.actived);
    listElement.current.classList.remove(style['not-actived']);
  }
  function dissaper() {
    listElement.current.classList.add(style['not-actived']);
    listElement.current.classList.remove(style.actived);
  }
  return (
    <>
      <motion.div
        className={`card bg-transparent text-bg-dark ${classes.cards} ${style.cards}`}
        initial={{ y: 50 }}
        whileInView={{ y: 0 }}
        whileHover={{ y: -20 }}
        viewport={{ root: ref }}
        key={props.myId}
      >
        <video
          id={props.myId}
          className={classes.video}
          src={props.video}
          poster={props.poster}
          style={{ maxWidth: 'fit-content' }}
        ></video>
        <div className={`card-img-overlay ${classes.cio}`}>
          <div className={classes.textWrapper}>
            <h1 className={`card-title ${classes.cardTitle}`}>{props.title}</h1>
            <div className={`iconText-wrapper ${classes.iconTextWrapper}`}>
              <p className={`card-text ${classes.cardText} ${classes.p}`}>
                {props.desc}
              </p>
            </div>
          </div>
          <span
            className={style['tmb-selengkapnya-wrapper']}
            onMouseOver={() => apper(this)}
            onMouseOut={() => dissaper(this)}
            ref={listElement}
          >
            <Link
              href={`/detail-artikel?id=${props.myId}`}
              className={`btn btn-success ${style.btnInfo}`}
              onClick={() => {
                props.setFetching(false);
              }}
            >
              Baca Selengkapnya
            </Link>
          </span>
        </div>
        <span className={`${style['tmb-play-wrapper']} `}>
          <button
            className={`${style.playbtn} ${activeElement}`}
            onClick={() => play(props.myId)}
          >
            <Image
              src={require('../../pages/assets/icons/play.png')}
              alt=""
              style={{ display: 'unset' }}
            />
          </button>
        </span>
      </motion.div>
    </>
  );
};

VideoCompo.defaultProps = {
  video: 'No Video',
  title: 'Title',
  desc: 'Description...',
};
export default VideoCompo;

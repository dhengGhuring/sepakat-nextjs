import { Image, Title, Text, Group, createStyles, Button } from '@mantine/core';
// import { Link } from 'react-router-dom';
const useStyle = createStyles((theme) => ({
  title: {
    fontFamily: 'Nunito Sans, sans-serif',
    fontWeight: 700,
    fontSize: 14,
    color: '#000000',
    '@media (min-width: 600px)': {
      fontSize: 17,
    },
  },
  text: {
    fontSize: 12,
    fontWeight: 400,
    color: '#4D4D4D',
    textAlign: 'justify',
    marginTop: 10,
    marginBottom: 10,
    '@media (min-width: 600px)': {
      fontSize: 14,
    },
  },
  date: {
    fontSize: 12,
    fontWeight: 700,
    color: '#000000',
    '@media (min-width: 600px)': {
      fontSize: 13,
    },
  },
  writter: {
    fontSize: 12,
    fontWeight: 400,
    color: '#000000',
    '@media (min-width: 600px)': {
      fontSize: 13,
    },
  },
}));
const ArticleMobileCompo = (props) => {
  const { classes } = useStyle();
  return (
    <div className="container">
      <hr />
      <div className="row align-items-center">
        <div className="col-6">
          <Image src={props.image} className={classes.image} alt="" />
        </div>
        <div className="col">
          <Title className={classes.title}>{props.title}</Title>
          <Text className={classes.text}>{props.article}</Text>
          {/* <Link to={`/detail-artikel?id=${props.myId}`}> */}
          <Button
            ml={20}
            color="orange.9"
            radius="xl"
            className={classes.btn_more}
          >
            Selengkapnya
          </Button>
          {/* </Link> */}
          {/* <Group>
            <Text className={classes.date}>18 Mei 2022</Text>
            <Text className={classes.writter}>Admin Sepakat</Text>
          </Group> */}
        </div>
      </div>
      <hr />
    </div>
  );
};

export default ArticleMobileCompo;

import { Grid, Container, createStyles, Title, Text } from '@mantine/core';
import Image from 'next/image';

import Aos from 'aos';
import 'aos/dist/aos.css';
import { useEffect } from 'react';
import hand from '../../pages/assets/icons/Hand.png';
import man from '../../pages/assets/icons/Man.png';
import book from '../../pages/assets/icons/Book.png';

const useStyle = createStyles((theme) => ({
  grid: {
    justifyContent: 'space-between!important',
    textAlign: '-webkit-center',
  },
  container: {
    textAlign: 'left',
  },
  title: {
    height: 100,
    fontFamily: 'Poppins, sans-serif',
    fontWeight: 700,
    color: '#1C7849',
    marginTop: 20,
    '@media (max-width: 600px)': {
      height: 65,
    },
    '@media (min-width: 600px)': {
      height: 65,
    },
    '@media (min-width: 992px)': {
      fontSize: 20,
      height: 90,
    },
  },
  title1: {
    fontSize: 23,
    '@media (max-width: 600px)': {
      marginBottom: 20,
    },
    '@media (min-width: 992px)': {
      fontSize: 20,
    },
  },
  title2: {
    fontSize: 22,
    '@media (min-width: 992px)': {
      fontSize: 20,
    },
  },
  title3: {
    fontSize: 20,
    '@media (max-width: 600px)': {
      height: 90,
    },
    '@media (min-width: 992px)': {
      fontSize: 20,
    },
  },
  text1: {
    fontFamily: 'Nunito Sans, sans-serif',
    fontSize: 23,
    fontWeight: 400,
    color: '#000000',
    textAlign: 'justify',
  },
}));

const FeatureCompo = () => {
  const { classes } = useStyle();
  useEffect(() => {
    Aos.init({
      duration: 300,
      easing: 'ease-in-sine',
      offset: 200,
      delay: 100,
    });
  });
  return (
    <div>
      <div className="container">
        <Grid className={classes.grid} data-aos="fade-up">
          <Grid.Col md={4} lg={4}>
            <Image
              // src={require('../../assets/images/house2 1.png')}
              src={man}
              width={200}
              alt=""
            />
            <Container className={classes.container}>
              <Title className={`${classes.title} ${classes.title1}`}>
                Cari Rumah yang Sesuai dengan Anda
              </Title>
              <Text className={classes.text} style={{ textAlign: 'justify' }}>
                Sepakat telah bekerja sama dengan lebih dari 100 perumahan di 10
                kota di Jawa Timur. Hal ini membuat user memiliki banyak pilihan
                untuk jenis rumah yang diinginkan dengan mengetahui langsung
                harga hingga spesifikasi rumah tersebut.
              </Text>
            </Container>
          </Grid.Col>
          <Grid.Col md={4} lg={4}>
            <Image
              // src={require('../../assets/images/briefcase 2.png')}
              src={hand}
              width={200}
              alt=""
            />
            <Container className={classes.container}>
              <Title className={`${classes.title} ${classes.title2}`}>
                Memilih Agen Properti Sendiri
              </Title>
              <Text className={classes.text} style={{ textAlign: 'justify' }}>
                Dengan aplikasi sepakat, user dapat memilih sendiri agen yang
                akan membantu dia dalam proses pembelian rumah. Aplikasi ini
                akan menampilkan profil lengkap agen tersebut, sehingga user
                dapat mengetahui kualifikasi dari agen yang dipilihnya
              </Text>
            </Container>
          </Grid.Col>
          <Grid.Col md={4} lg={4}>
            <Image
              // src={require('../../assets/images/siteplan 1.png')}
              src={book}
              width={200}
              alt=""
            />
            <Container className={classes.container}>
              <Title className={`${classes.title} ${classes.title3}`}>
                Memilih Kavling Sendiri dengan Siteplan Aktif dan Realtime
              </Title>
              <Text className={classes.text} style={{ textAlign: 'justify' }}>
                Sepakat memiliki fitur siteplan aktif, yaitu dapat mengupdate
                kavling yang telah terjual dan belum terjual. Dengan fitur ini,
                user dapat memilih sendiri kavling yang dia inginkan, dan
                mengetahui langsung gambar rumah tersebut tanpa harus survey ke
                lokasi.
              </Text>
            </Container>
          </Grid.Col>
        </Grid>
      </div>
    </div>
  );
};

export default FeatureCompo;

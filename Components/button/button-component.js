import { Container, Group, Button, Text } from "@mantine/core";
import Image from "next/image";
import brosurIcon from "../../pages/assets/icons/Brosur.png";
import viewImage from "../../pages/assets/icons/360.png";
import redirectPlayStore from "../../utility/redirectPlayStore";
import Link from "next/link";

const ButtonCompo = (props) => {
  return (
    <div>
      <Container mb={40} p={0}>
        <Group>
          <Link href={`/brosur`} target={"_blank"}>
            <Button color="teal.9" radius="xl">
              <Image src={brosurIcon} width={20} alt={"icon brosur"} />{" "}
              <Text ml={10}>Brosur</Text>
            </Button>
          </Link>
          <Button color="teal.9" radius="xl" onClick={redirectPlayStore}>
            <Image src={viewImage} width={25} alt={"icon 360 view"} />
            <Text ml={10}>360 View</Text>
          </Button>
        </Group>
      </Container>
    </div>
  );
};

export default ButtonCompo;

import style from './infoCardStyle.module.css';
import playstore from '../../pages/assets/images/Section-Footer-Button-Download-Playstore-Large.svg';
import appstore from '../../pages/assets/images/Section-Footer-Button-Download-Appstore-Large.svg';
import { Tooltip } from '@mantine/core';
import Image from 'next/image';
import Link from 'next/link';
const InfoCardCompo = () => {
  return (
    <>
      <div className={`${style['rincian-5']} p-4 my-5 `}>
        <h1>Butuh info lebih lanjut ??</h1>
        <a href="https://wa.me/+6281335258080" target="_blank" rel="noreferrer">
          <button
            type="button"
            className={`${style.tombolRincian5} btn btn-success mb-4 rounded-3 d-flex flex-row`}
          >
            <p className="m-0">Cari info lewat Whatsapp</p>
            <Image
              src={require('../../pages/assets/icons/WA Solid.png')}
              alt=""
              className={style.icon}
            />
          </button>
        </a>

        <h1>Atau download aplikasi lewat</h1>
        <div className="d-flex">
          <Link
            href="https://play.google.com/store/apps/details?id=id.asc.sepakat"
            target="_blank"
          >
            <Image src={playstore} alt="" className={style.iconPS} />
          </Link>
          <Tooltip label="Segera Hadir" position="bottom" color="green.9">
            <a>
              <Image src={appstore} alt="" className={style.iconPS} />
            </a>
          </Tooltip>
        </div>
      </div>
    </>
  );
};

export default InfoCardCompo;

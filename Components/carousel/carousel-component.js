import { useState } from 'react';
import { Carousel } from '@mantine/carousel';
import { createStyles } from '@mantine/core';
import style from './carouselCompoStyle.module.css';
import PaginationDetailProperty from '../pagination/PaginationDetailProperty';

import { BsXCircle } from 'react-icons/bs';
import { GiNextButton, GiPreviousButton } from 'react-icons/gi';
import Image from 'next/image';

const useStyles = createStyles((theme) => ({
  control: {
    backgroundColor: '#F2EEC8',
    borderColor: '#F2EEC8',
    color: '#F56600',
    opacity: 0.5,
  },
  carouselSlide: {
    overflow: 'hidden',
    cursor: 'pointer',
  },
  image: {
    position: 'absolute',
    objectFit: 'cover',
    width: '100%',
    height: '100%',
    top: -9999,
    bottom: -9999,
    left: -9999,
    right: -9999,
    margin: 'auto',
  },
  containerGallery: {
    transitionTimingFunction: 'ease-out',
    transition: '1s!important',
  },
}));

const CarouselCompo = (props) => {
  const { property } = props;
  const { classes } = useStyles();

  // Ambil data gambar yang di klik
  const [data, setData] = useState({ img: '', i: 0 });

  // Mengupdate dan menampilkan hasil klik gambar
  const viewImage = (img, i) => {
    setData({ img, i });
    // console.log(data);
  };

  // Fungsi next slide gambar
  const actionImage = (action) => {
    let i = data.i;
    if (!action) {
      setData({ img: '', i: 0 });
    }
  };

  // Fungsi next slide gambar
  const nextImage = () => {
    let i = data.i;
    if (i < property?.images?.length - 1) {
      i++;
      setData({ img: property?.images[i], i });
    }
  };

  // Fungsi prev slide gambar
  const prevImage = () => {
    let i = data.i;
    if (i > 0) {
      i--;
      setData({ img: property?.images[i], i });
    }
  };

  return (
    <>
      {/* Image Gallery Section */}
      {/* {data.img ? (
                <div
                    className={classes.containerGallery}
                    style={{
                        width: "100%",
                        height: "100vh",
                        background: "black",
                        position: "fixed",
                        zIndex: 3,
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        overflow: "hidden",
                        transition: "0.5s",
                    }}
                >
                    <button
                        style={{
                            position: "absolute",
                            top: "50px",
                            right: "30px",
                            border: "none",
                            background: "none",
                        }}
                        onClick={() => {
                            actionImage();
                        }}
                    >
                        <BsXCircle color="white" size={30} />
                    </button>

                    <button
                        onClick={() => {
                            prevImage();
                        }}
                        style={{
                            position: "absolute",
                            top: "50%",
                            left: "20px",
                            border: "none",
                            background: "none",
                        }}
                    >
                        <GiPreviousButton color="white" size={30} />
                    </button>
                    <img
                        src={data.img}
                        alt=""
                        style={{
                            marginTop: "-100px",
                            width: "auto",
                            maxWidth: "90%",
                            maxHeight: "60%",
                            borderRadius: "10px",
                        }}
                    />
                    <button
                        onClick={() => {
                            nextImage();
                        }}
                        style={{
                            position: "absolute",
                            top: "50%",
                            right: "50px",
                            border: "none",
                            background: "none",
                        }}
                    >
                        <GiNextButton color="white" size={30} />
                    </button>
                </div>
            ) : null} */}
      {data.img ? (
        <div>
          <PaginationDetailProperty
            image={property?.images}
            actionImage={actionImage}
            index={data.i}
          />
        </div>
      ) : null}
      {/* End Image Gallery */}
      <Carousel
        draggable={false}
        controlsOffset="10%"
        controlSize={40}
        height={400}
        slideSize="33.333333%"
        slideGap="sm"
        breakpoints={[
          { maxWidth: 'md', slideSize: '50%' },
          { maxWidth: 'sm', slideSize: '100%', slideGap: 0 },
        ]}
        align="start"
        classNames={{
          control: classes.control,
        }}
        styles={{
          control: {
            '&[data-inactive]': {
              opacity: 0,
              cursor: 'default',
            },
          },
        }}
      >
        {property?.images?.map((item, key) => {
          return (
            <Carousel.Slide
              key={key}
              className={`${style['mantine-mkghez']} ${classes.carouselSlide}`}
            >
              {/* <Image src={item} withPlaceholder className={classes.image} /> */}
              <Image
                src={item}
                alt=""
                srcSet=""
                className={classes.image}
                width={1000}
                height={1000}
                onClick={() => {
                  viewImage(item, key);
                }}
              />
            </Carousel.Slide>
          );
        })}
        {/* ...other slides */}
      </Carousel>
    </>
  );
};

export default CarouselCompo;

import React from 'react';
import { Title, Text, createStyles } from '@mantine/core';
import capitalizeWord from '../../utility/capitalizeWord';
const useStyle = createStyles((theme) => ({
  title: {
    fontFamily: 'Nunito Sans, sans-serif',
    fontSize: 16,
    fontWeight: 400,
    paddingBottom: 10,
    color: '#4d4d4d',
    '@media (min-width: 600px)': {
      fontSize: 25,
    },
  },
  address: {
    fontFamily: 'Nunito, sans-serif',
    fontSize: 12,
    fontWeight: 400,
    paddingBottom: 10,
    color: '#4d4d4d',
    '@media (min-width: 600px)': {
      fontSize: 15,
    },
  },
  ul: {
    display: 'flex',
  },
  li: {
    fontSize: 10,
    fontWeight: 700,
    marginRight: 25,
    '@media (min-width: 600px)': {
      fontSize: 15,
    },
  },
}));
export default function DescriptionHousingComponent(props) {
  const { classes } = useStyle();
  return (
    <>
      <Title className={classes.title}>{props.title}</Title>
      <Text className={classes.address}>{props.address}</Text>
      <ul className={`ps-3 ${classes.ul}`}>
        {props.categories?.map((category, key) => (
          <li key={key} className={`text-success ${classes.li}`}>
            {capitalizeWord(category)}
          </li>
        ))}
        <li className={`text-danger ${classes.li}`}>
          {capitalizeWord(props.transaction)}
        </li>
      </ul>
    </>
  );
}

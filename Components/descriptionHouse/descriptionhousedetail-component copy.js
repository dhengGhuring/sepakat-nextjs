import { Title, Text, Container } from "@mantine/core";
import style from "./descriptionCompoStyle.module.css";
const DescHouseDetailCompo = (props) => {
  return (
    <div className={style.pelindung}>
      <Container>
        <Title
          mt={20}
          size={20}
          color="#F56600"
          style={{ fontFamily: "Nunito, sans-serif", fontWeight: 700 }}
        >
          Deskripsi
        </Title>
        <Text
          mt={20}
          align="justify"
          style={{ fontFamily: "Nunito Sans, sans-serif" }}
        >
          {props.description1}
        </Text>
      </Container>
    </div>
  );
};

export default DescHouseDetailCompo;

import { Container, Title, createStyles, Stack } from "@mantine/core";
// Import AOS
import Aos from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";
import HeroImage from "../../pages/assets/images/timSepakat.png";
import style from "./jumbotron3CompoStyle.module.css";

const useStyle = createStyles((theme) => ({
    backgroundimage: {
        height: "100vh",
        display: "flex!important",
        backgroundImage: `url(${HeroImage})`,
        backgroundSize: "cover",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
    },
    container: {
        display: "flex!important",
        alignItems: "center",
        justifyContent: "center",
        paddingLeft: 0,
    },
    title1: {
        "@media (min-width: 600px)": {
            display: "none",
        },
        fontFamily: "Poppins, sans-serif",
        color: "#F56600",
        fontSize: 48,
        fontWeight: 700,
    },
    title: {
        fontFamily: "Poppins, sans-serif",
        fontSize: 96,
        fontWeight: 700,
        color: "#FEFEFD",
        "@media (max-width: 600px)": {
            fontSize: 20,
        },
        "@media (min-width: 600px)": {
            fontSize: 40,
        },
        "@media (min-width: 992px)": {
            fontSize: 80,
        },
        "@media (min-width: 1200px)": {
            fontSize: 96,
        },
    },
}));
const JumboTron3Compo = () => {
    const { classes } = useStyle();
    useEffect(() => {
        Aos.init({
            duration: 300,
            easing: "ease-in-sine",
            offset: 200,
            delay: 100,
        });
    }, []);
    return (
        <div className={`${classes.backgroundimage} ${style.container}`}>
            <Container data-aos="fade-up" className={classes.container}>
                <Stack>
                    <Title className={classes.title1}>Sepakat</Title>
                    <Title className={classes.title}>#MudahCariProperti</Title>
                </Stack>
            </Container>
        </div>
    );
};
export default JumboTron3Compo;

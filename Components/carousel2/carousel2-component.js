import PropTypes from "prop-types";
import { Carousel } from "@mantine/carousel";
import {
  createStyles,
  Text,
  Title,
  BackgroundImage,
  Container,
} from "@mantine/core";
import { motion } from "framer-motion";
import style from "./carouselCompoStyle.module.css";

const useStyles = createStyles((theme) => ({
  container: {
    width: "100%",
    marginLeft: 50,
  },
  slideWrapper: {},
  backgroundImage: {
    width: "400px!important",
    height: "100%",
    // backgroundRepeat: "no-repeat",
    // backgroundPosition: "center",
    // backgroundSize: "cover",
    // borderRadius: 16,
    // position: "relative",
    // transition: "5s ease-in-out",
    // ":hover": {
    //     backgroundSize: "115%",
    //     transitionDuration: "1s",
    //     transitionDelay: "0.5s",
    //     transitionTimingFunction: "ease-in-out",
    //     transitionProperty: "background-size",
    //     transition: "5s ease-in-out",
    // },
    "&:before": {
      content: '""',
      position: "absolute",
      width: "95%",
      height: "100%",
      border: "none",
      background:
        "linear-gradient(to bottom, rgba(255, 255, 255, 0.20), rgba(10, 10, 10, 0.40))",
      zIndex: 0,
      borderRadius: 16,
    },
    "@media (max-width: 600px)": {
      width: "400px!important",
    },
    "@media (max-height: 500px)": {
      height: "37vh!important",
      transition: "0.5s",
    },
    "@media (min-height: 500px) and (max-height: 600px)": {
      height: "37vh!important",
      transition: "0.5s",
    },
    "@media (min-height: 600px) and (max-height: 750px)": {
      height: "30vh!important",
      transition: "0.5s",
    },
  },
  containerText: {
    paddingTop: "35%",
    paddingBottom: 24,
    paddingLeft: 24,
    position: "relative",
    "@media (max-height: 500px)": {
      paddingTop: "20vh!important",
      transition: "0.5s",
    },
    "@media (min-height: 500px) and (max-height: 600px)": {
      paddingTop: "25vh!important",
      transition: "0.5s",
    },
    "@media (min-height: 600px) and (max-height: 750px)": {
      paddingTop: "20vh!important",
      transition: "0.5s",
    },
  },
  control: {
    backgroundColor: "#F2EEC8",
    color: "#F56600",
    borderColor: "#F2EEC8",
    opacity: 1,
  },
  title: {
    fontFamily: "Nunito Sans, sans-serif",
    fontWeight: 800,
    fontSize: 18,
    textOverflow: "ellipsis",
    color: "#fff",
    // WebkitTextStrokeColor: '#4d4d4d',
    // WebkitTextStrokeWidth: '0.5px',
  },
  text: {
    fontFamily: "Nunito Sans, sans-serif",
    fontSize: 18,
    fontWeight: 400,
    textOverflow: "ellipsis",
    color: "#fff",
    // WebkitTextStrokeColor: '#4d4d4d',
    // WebkitTextStrokeWidth: '0.5px',
  },
  anchor: {
    "&:hover": {
      color: theme.white,
      textDecoration: "none",
    },
  },
}));
const CarouselCompo2 = (props) => {
  const { classes } = useStyles();
  return (
    <Carousel.Slide className={classes.slideWrapper}>
      <div
        className={`${classes.backgroundImage} ${style.image}`}
        style={{
          backgroundImage: `url(${props.image})`,
          borderRadius: 16,
        }}
      >
        <Container className={classes.containerText}>
          <Title order={5} className={classes.title}>
            {props.title}
          </Title>
          <Text className={classes.text}>{props.address}</Text>
        </Container>
      </div>
    </Carousel.Slide>
  );
};
// Default Value Props
CarouselCompo2.propTypes = {
  title: PropTypes.string,
};
// CarouselCompo2.defaultProps = {
//   image: 'Image not load',
//   title: 'No Title',
//   address: 'No Address',
// };
export default CarouselCompo2;

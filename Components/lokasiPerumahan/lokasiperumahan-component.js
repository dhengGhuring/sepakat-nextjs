import Image from "next/image";
import style from "./lokasiPerumCompoStyle.module.css";
const LokasiPerumCompo = (props) => {
  return (
    <div key={props.index}>
      <div className={`${style["detail-1"]} d-flex mb-3`}>
        <div className={`${style.iconKeterangan} d-flex `}>
          <Image src={props.icon} alt="" className={style.img} />
          <p>{props.title}</p>
        </div>
        <div className={style.waktu}>
          <p>{props.time}</p>
        </div>
      </div>
    </div>
  );
};
export default LokasiPerumCompo;

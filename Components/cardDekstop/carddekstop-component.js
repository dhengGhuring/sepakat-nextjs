import { Carousel } from "@mantine/carousel";
import {
  BackgroundImage,
  Container,
  Title,
  Text,
  createStyles,
} from "@mantine/core";
import style from "./cardDekstopStyle.module.css";

const useStyles = createStyles((theme) => ({
  backgroundImage: {
    height: "100%",
    borderRadius: "20px!important",
    "&:before": {
      content: '""',
      position: "absolute",
      width: "100%",
      height: "100%",
      border: "none",
      background:
        "linear-gradient(to bottom, rgba(255, 255, 255, 0.20), rgba(10, 10, 10, 0.40))",
      zIndex: 0,
      borderRadius: 16,
    },
  },
  containerText: {
    position: "relative",
    width: 200,
    height: "auto",
    paddingTop: "100%",
    paddingBottom: "10%",
  },
  control: {
    backgroundColor: "#F2EEC8",
    color: "#F56600",
    borderColor: "#F2EEC8",
    opacity: 1,
  },
  anchor: {
    "&:hover": {
      color: theme.white,
      textDecoration: "none",
    },
  },
  title: {
    fontFamily: "Nunito Sans, sans-serif",
    fontWeight: 700,
    fontSize: 18,
  },
}));

const CardDekstopCompo = (props) => {
  const { classes } = useStyles();
  return (
    <>
      <Carousel.Slide mr={20}>
        <BackgroundImage
          src={`"${props.image} "`}
          className={classes.backgroundImage}
        >
          <Container
            className={`${classes.containerText} ${style.textContainer}`}
          >
            <Title className={`${style.title} ${classes.title}`}>
              {props.title}
            </Title>
            <Text>{props.address}</Text>
          </Container>
        </BackgroundImage>
      </Carousel.Slide>
    </>
  );
};
export default CardDekstopCompo;

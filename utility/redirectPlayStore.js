import Swal from "sweetalert2";
import { PLAYSTORE_ID } from "./constants";

async function generateDynamicLinks(type, id) {
  try {
    const url = "https://firebasedynamiclinks.googleapis.com/v1/shortLinks";
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        dynamicLinkInfo: {
          domainUriPrefix: process.env.REACT_APP_DYNAMIC_LINK,
          link: `${process.env.REACT_APP_DYNAMIC_LINK}/${type}?id=${id}`,
          androidInfo: {
            androidPackageName: PLAYSTORE_ID,
            androidMinPackageVersionCode: "0",
          },
          socialMetaTagInfo: {
            socialTitle: "Agent Sepakat",
            socialDescription: "#mudahCariProperti",
            socialImageLink:
              "https://scontent-cgk1-1.xx.fbcdn.net/v/t39.30808-6/279579683_124028403590826_8780324267848155288_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=uHrFup1HpuEAX8ufL_i&_nc_ht=scontent-cgk1-1.xx&oh=00_AT-gQje6vUtD9FLLjlPjsDJjmK2OoL7xt_xdXJ6fBTN-wA&oe=62C0D1ED",
          },
        },
        suffix: { option: "SHORT" },
      }),
    };
    const res = await fetch(
      `${url}?key=AIzaSyAM6JDt7DsfzC2GXMow6p8e6IUMGxc6Ae4`,
      requestOptions
    )
      .then((res) => {
        return res.json();
      })
      .then((res) => {
        return res["shortLink"];
      });
    return res;
  } catch (e) {
    console.log("error generate dynamic links: " + e);
  }
}

async function mobileCheck() {
  const toMatch = [
    /Android/i,
    // /webOS/i,
    /iPhone/i,
    /iPad/i,
    /iPod/i,
    /BlackBerry/i,
    /Windows Phone/i,
  ];

  return toMatch.some((toMatchItem) => {
    return navigator.userAgent.match(toMatchItem);
  });
}

export default async function redirectPlayStore(type, id) {
  let uri = `https://play.google.com/store/apps/details?id=${PLAYSTORE_ID}`;
  if (type && id) {
    let temp;
    let mCheck;
    try {
      mCheck = await mobileCheck();
      temp = await generateDynamicLinks(type, id);
    } catch (error) {
      console.log("error generate: " + error);
    }
    console.log("redirectPlayStore: ", mCheck);
    if (temp && mCheck) {
      uri = temp;
    }
  }

  return await Swal.fire({
    title: "Info!",
    text: "Fitur ini tidak tersedia untuk versi website. Silahkan unduh Aplikasi Sepakat untuk menikmati fitur ini!",
    icon: "info",
    confirmButtonText: "Ok",
  }).then(function () {
    window.open(uri);
  });
}

export const suggestion = [
  { name: "Jember" },
  { name: "Bondowoso" },
  { name: "Situbondo" },
  { name: "Banyuwangi" },
];

export const logoMitra = [
  { logo: "../assets/images/logo/2560px-INDOSIAR_Logo.png" },
];

export const iconFasilitas = [
  {
    bathroom:
      "../assets/images/Icons/Sepakat-Icon/Tag-Facility-icon-bathroom.svg",
  },
  {
    bedroom:
      "../assets/images/Icons/Sepakat-Icon/Tag-Facility-icon-bedroom.svg",
  },
  {
    building:
      "../assets/images/Icons/Sepakat-Icon/Tag-Facility-icon-building.svg",
  },
  {
    certificate:
      "../assets/images/Icons/Sepakat-Icon/Tag-Facility-icon-certificate.svg",
  },
  {
    family: "../assets/images/Icons/Sepakat-Icon/Tag-Facility-icon-family.svg",
  },
  { flash: "../assets/images/Icons/Sepakat-Icon/Tag-Facility-icon-flash.svg" },
  { frame: "../assets/images/Icons/Sepakat-Icon/Tag-Facility-icon-frame.svg" },
  {
    garage: "../assets/images/Icons/Sepakat-Icon/Tag-Facility-icon-garage.svg",
  },
  {
    garden: "../assets/images/Icons/Sepakat-Icon/Tag-Facility-icon-garden.svg",
  },
  { plate: "../assets/images/Icons/Sepakat-Icon/Tag-Facility-icon-plate.svg" },
  { sofa: "../assets/images/Icons/Sepakat-Icon/Tag-Facility-icon-sofa.svg" },
  { water: "../assets/images/Icons/Sepakat-Icon/Tag-Facility-icon-water.svg" },
];

export const AgenIcon = {
  chat: "../assets/images/Icons/chat.png",
  briefcase: "../assets/images/Icons/briefcase.png",
  clipboard: "../assets/images/Icons/clipboard.png",
};

export const APP_URL = process.env.REACT_APP_BASE_URL;
export const PLAYSTORE_ID = "id.asc.sepakat";
